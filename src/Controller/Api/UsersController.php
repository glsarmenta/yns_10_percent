<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\I18n\Time;
use Cake\Http\Exception\BadRequestException;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('Users');
        $this->loadModel('Members');
        $this->Auth->allow(['login']);
        $this->autoRender = false;
    }

    /**
     * login method
     *
     * @return \Cake\Http\Response|void
     */
    public function login()
    {
        $message = [
            'status' => true,
            'message' => __('success'),
            'data' => []
        ];
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $message['data'] = $this->Auth->user();
            } else {
                $message['status'] = false;
                $message['message'] = __('Incorrect Login');
            }
        }
        $this->response->body(json_encode($message));
    }
    /**
     * getUser method
     *
     * @return \Cake\Http\Response|void
     */
    public function getUser()
    {
        $this->request->allowMethod(['get']);

        $data = $this->request->query();
        if (!isset($data['id'])) {
            throw new BadRequestException(__('Invalid Parameter', 400));
        }
        $user = $this->Users->find()
                ->where([
                    'id' => $data['id']
                ])
                ->first();

        // Prior to 3.4.0 - Set the body
        $this->response->body($user);
        $this->set('_serialize', ['user']);
    }
}
