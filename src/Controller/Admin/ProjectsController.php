<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\Core\Configure;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 */
class ProjectsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('AdminUser');
    }

    /**
     * index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $projects = $this->Projects->find(
            'all', [
                        'fields' => [
                            'id',
                            'name',
                            'project_code'
                        ]
                ]
        )->contain([
            'Tasks' => [
                    'fields' => [
                        'id',
                        'project_id',
                        'estimate_times',
                        'total_actual_times'
                 ]
            ]
        ])->toArray();

        foreach ($projects as $key => $value) {
            $collection = new Collection($value['tasks']);
            $projects[$key]['total_actual_times'] = $collection->sumOf(
                function ($v) {
                    if (isset($v['total_actual_times'])) {
                        return $v['total_actual_times'];
                    }
                }
            );

            $projects[$key]['total_estimate_times'] = $collection->sumOf(
                function ($v) {
                    if (isset($v['estimate_times'])) {
                        return $v['estimate_times'];
                    }
                }
            );
        }

        $this->set(compact('projects'));
        $this->set('_serialize', ['projects']);
    }

    /**
     * detail method
     *
     * @param  string|null $id Project id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function detail($id = null)
    {
        // Get Tasks table
        $this->Tasks = TableRegistry::get('Tasks');
        $this->Milestones = TableRegistry::get('Milestones');

        //Get all milestones
        $milestone = $this->Milestones->find('all')
            ->contain([
                'Tasks'
            ])
            ->where(['Milestones.project_id' => $id])
            ->toArray();
        foreach ($milestone as $key => $value) {
            $collection = new Collection($value['tasks']);
            $milestone[$key]['actual_time'] = $collection->sumOf(
                function($v) {
                    if(isset($v['total_actual_times'])){
                        return $v['total_actual_times'];
                    }
                }
            );
        }
        // Get Projects
        $project = $this->Projects->find(
            'all', [
                    'fields' => [
                        'id',
                        'name',
                        'project_code'
                    ]
                ]
        )->contain(
            [
                'ProjectUsers' => [
                'Users' => function ($q) {
                      return $q
                          ->select(['id', 'name'])
                          ->where([
                                    'is_outsource' => 0,
                                    'is_enable'    => 1
                                  ]);
                  }
            ], 'Tasks' => [
                    'fields' => [
                        'id',
                        'project_id',
                        'estimate_times',
                        'total_actual_times',
                        'grade'
                    ]
                ]
            ]
        )->where(['id' => $id]
        )->toArray();

        // Compute for total_actual_times and total_estimate_times of Project
        foreach ($project as $key => $value) {
            $collection = new Collection($value['tasks']);
            $project[$key]['total_actual_times'] = $collection->sumOf(
                function ($v) {
                    if (isset($v['total_actual_times'])) {
                        return $v['total_actual_times'];
                    }
                }
            );
            $project[$key]['total_estimate_times'] = $collection->sumOf(
                function ($v) {
                    if (isset($v['estimate_times'])) {
                        return $v['estimate_times'];
                    }
                }
            );
        }

        // For Task list: Get daily_work_results by Task id
        $daily_work_results = $this->Tasks->find('all')
            ->contain(
              ['DailyWorkResults' => [
                    'sort' => ['DailyWorkResults.modified' => 'DESC'],
                    'Users' => function ($q) {
                          return $q
                              ->select(['id', 'name'])
                              ->where([
                                        'is_outsource' => 0,
                                        'is_enable'    => 1
                                      ]);
                     }
                ],
                'Projects' => [
                    'fields' => [
                        'name'
                    ]
                ]
            ]
        )
        ->innerJoinWith(
            'DailyWorkResults', function ($q) {
                return $q->where(['DailyWorkResults.task_id = Tasks.id']);
            }
        )
        ->where(['project_id' => $id])
        ->group(['Tasks.id'])
        ->toArray();

        $task_codes    = [];
        $actual_time   = [];
        $estimate_time = [];

        foreach ($daily_work_results as $key => $value) {
            $task_codes[]    = $value['task_code'];
            $actual_time[]   = $value['total_actual_times'];
            $estimate_time[] = $value['estimate_times'];
        }

        $max = 0;
        $max_actual    = (!empty($actual_time)) ? max($actual_time) : 0;
        $max_estimate  = (!empty($estimate_time)) ? max($estimate_time) : 0;

        $task_codes    = json_encode($task_codes);
        $actual_time   = json_encode($actual_time);
        $estimate_time = json_encode($estimate_time);

        // ChartJS ticks: max for xAxes
        $max = ($max_actual > $max_estimate) ? $max_actual : $max_estimate;
        $max = ceil($max) + 2;

        $all_grades = Configure::read('Grades');

        $this->set(compact('id', 'project', 'daily_work_results', 'all_grades', 'task_codes', 'actual_time', 'estimate_time', 'max','milestone'));
        $this->set('_serialize', ['id', 'project', 'daily_work_results', 'all_grades', 'task_codes', 'actual_time', 'estimate_time', 'max']);
    }

    /**
     * add method
     *
     * responsible for saving new project
     *
     * @return \Cake\Http\Response|json
     */
    public function add()
    {
        if ($this->request->is('post')) {
            // Get ProjectUsers table
            $this->ProjectUsers = TableRegistry::get('ProjectUsers');

            $entity = $this->Projects->newEntity();

            $this->request->data['in_backlog'] = 0;

            if ($this->request->data['is_common'] == 'true') {
                $this->request->data['is_common']  = 1;
            } else {
                $this->request->data['is_common']  = 0;
            }

            $patch_entity = $this->Projects->patchEntity($entity, $this->request->data);

            // Saving Projects
            if ($result = $this->Projects->save($patch_entity)) {
                $user_ids = $this->request->data('user_id');

                // Saving ProjectUsers
                foreach ($user_ids as $user_id) {
                    $data = [
                        'user_id'    => $user_id,
                        'project_id' => $result->id
                    ];
                    $entityProjectUsers = $this->ProjectUsers->newEntity();
                    $patchEntityProjectUsers = $this->ProjectUsers->patchEntity($entityProjectUsers, $data);
                    $this->ProjectUsers->save($patchEntityProjectUsers);
                }

                $response = json_encode(
                    [
                        'status'    => true,
                        'message'   => __('Record Added')
                    ]
                );
            } else {
                $error = array_map('current', $patch_entity->getErrors());
                $response = json_encode(['status' => false, 'message' => $error]);
            }

            $this->response->body($response);
            return $this->response;
        }

        // Get Users table
        $this->Users = TableRegistry::get('Users');
        $users = $this->Users->find(
            'list', [
                'keyField'   => 'id',
                'valueField' => 'name'
            ]
        )
            ->where(
                [
                'is_enable' => 1
                ]
            )
            ->order('name ASC')
            ->toArray();

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * validateAddEdit method
     *
     * responsible for validating inputs
     *
     * @return \Cake\Http\Response|json
     */
    public function validateAddEdit()
    {
        if ($this->request->is('post')) {
            // Get ProjectUsers table
            $this->ProjectUsers = TableRegistry::get('ProjectUsers');

            $entity = $this->Projects->newEntity();

            $request_user_ids = $this->request->data('user_id');

            // Check if no member is selected
            if (!isset($request_user_ids)) {
                $error = array_map(
                    'current', [
                        'is_allmembers' => [
                            '_empty' => __('Please select a member')
                        ]
                    ]
                );
                $response = json_encode(['status' => false, 'message' => $error]);
                $this->response->body($response);
                return $this->response;
            }

            // Remove validation if projects.in_backlog = 1
            if (!isset($this->request->data['name']) || !isset($this->request->data['project_code'])) {
                $patch_entity = $this->Projects->patchEntity($entity, $this->request->data, ['validate' => false]);

            }
            // Remove isUnique validation if projects.in_backlog = 0 and project_code is not changed
            else if ( $this->request->data['action'] == 'edit'
                && ($this->request->data['old_project_code'] == $this->request->data['project_code'])
                && $this->request->data['project_code'] != '') {

                $patch_entity = $this->Projects->patchEntity($entity, $this->request->data, ['validate' => 'Custom']);

            } else {
                $patch_entity = $this->Projects->patchEntity($entity, $this->request->data);
            }

            // Validate input
            if (empty($patch_entity->getErrors())) {
                $response = json_encode(['status' => true]);
            } else {
                $error = array_map('current', $patch_entity->getErrors());
                $response = json_encode(['status' => false, 'message' => $error]);
            }

            $this->response->body($response);
            return $this->response;
        }
    }

    /**
     * edit method
     *
     * responsible for updating project
     *
     * @param  string|null $id Project id.
     * @return \Cake\Http\Response|json
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Throw Exception if project not found
        $projects = $this->Projects->get($id);

        // Saving
        if ($this->request->is('post')) {
            // Get ProjectUsers table
            $this->ProjectUsers = TableRegistry::get('ProjectUsers');

            $this->request->data['project_id'] = $id;

            $request_user_ids = $this->request->data('user_id');

            // Update Projects and ProjectUsers if project is not in backlog
            if ($projects['in_backlog'] == 0) {
                $entity = $this->Projects->newEntity();

                $dataProjects = [
                    'id'            => $id,
                    'name'          => $this->request->data['name'],
                    'project_code'  => $this->request->data['project_code'],
                    'is_allmembers' => $this->request->data['is_allmembers'],
                    'in_backlog'    => 0
                ];

                // Remove isUnique validation if project_code is not changed
                if ($this->request->data['old_project_code'] == $this->request->data['project_code'] ) {
                    $patch_entity = $this->Projects->patchEntity(
                        $entity,
                        $dataProjects,
                        [
                            'accessibleFields' => ['id' => true],
                            'validate' => 'Custom'
                        ]
                    );
                } else {
                    $patch_entity = $this->Projects->patchEntity(
                        $entity,
                        $dataProjects,
                        [
                            'accessibleFields' => ['id' => true]
                        ]
                    );
                }

                // Updating Projects
                if ($this->Projects->save($patch_entity)) {

                    // Updating ProjectUsers
                    $this->__saveProjectUsers($id, $request_user_ids);

                    $response = json_encode(
                        [
                            'status'       => true,
                            'message'      => __('Updated Success')
                        ]
                    );
                } else {
                    $error = array_map('current', $patch_entity->errors());
                    $response = json_encode(['status' => false, 'message' => $error]);
                    $this->response->body($response);
                    return $this->response;
                }

            } else if($projects['in_backlog'] == 1) {
                // Updating ProjectUsers
                $this->__saveProjectUsers($id, $request_user_ids);

                $response = json_encode(
                    [
                        'status'    => true,
                        'message'   => __('Updated Success')
                    ]
                );
            } else {
                $error = array_map('current', $patch_entity->getErrors());
                $response = json_encode(['status' => false, 'message' => $error]);
            }

            $this->response->body($response);
            return $this->response;
        }

        // Get ProjectUsers table
        $this->ProjectUsers = TableRegistry::get('ProjectUsers');
        $user_ids = $this->ProjectUsers->find(
            'list', [
                'keyField'   => 'id',
                'valueField' => 'user_id'
            ]
        )
            ->where(
                [
                'ProjectUsers.project_id' => $id,
                ]
            )
            ->toArray();

        // Get Users table
        $this->Users = TableRegistry::get('Users');
        $users = $this->Users->find(
            'list', [
                'keyField'   => 'id',
                'valueField' => 'name'
            ]
        )
            ->where(
                [
                'is_enable' => 1
                ]
            )
            ->order('name ASC')
            ->toArray();

        $this->set(compact('projects', 'users', 'user_ids'));
        $this->set('_serialize', ['projects', 'users', 'user_ids']);
    }

    public function __saveProjectUsers($id = null, $request_user_ids = [])
    {
        // Delete all records on ProjectUsers with same project_id
        $this->ProjectUsers->deleteAll(['project_id' => $id]);

        // Saving ProjectUsers
        foreach ($request_user_ids as $user_id) {
            $dataProjectUsers = [
                'user_id'    => $user_id,
                'project_id' => $id
            ];
            $entityProjectUsers      = $this->ProjectUsers->newEntity();
            $patchEntityProjectUsers = $this->ProjectUsers->patchEntity($entityProjectUsers, $dataProjectUsers);
            $this->ProjectUsers->save($patchEntityProjectUsers);
        }
    }
}
