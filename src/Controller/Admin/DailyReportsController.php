<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * DailyReports Controller
 *
 * @property \App\Model\Table\DailyReportsTable $DailyReports
 *
 * @method \App\Model\Entity\DailyReport[] paginate($object = null, array $settings = [])
 */
class DailyReportsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('AdminUser');
        $this->loadComponent('SearchItem');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->DailyReports = TableRegistry::get('DailyReports');
        $data = $this->DailyReports->find('all')
                ->contain(['Users'])
                ->order(['DailyReports.business_date' => 'DESC']);

        $settings = [
            'maxLimit' => count($data->toArray()),
            'limit' => count($data->toArray())
        ];

        $dailyReports = $this->paginate($data, $settings);
        $this->set(compact('dailyReports'));
        $this->set('_serialize', ['dailyReports']);
    }

    public function SummaryList()
    {
        $this->Projects = TableRegistry::get('Projects');
        $this->Users    = TableRegistry::get('Users');
        $users   = $this->Users->find('all')
                    ->where([
                        'is_outsource'  => 0
                    ]);
        $projects = $this->Projects->find('list');

        $this->set(compact('projects', 'users'));
        $this->set('_serialize', ['projects', 'users']);
    }

    public function getMemberAndMilestone()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);
        $this->Projects = TableRegistry::get('Projects');
        $response = json_encode($this->SearchItem->searchMemberAndMilestone($this->request->data));
        $this->response->body($response);
        return $this->response;
    }

    public function suggestMember()
    {
        //Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);
        $project_id = "";
        if (isset($this->request->query['project_id'])) {
            $project_id = $this->request->query['project_id'];
        }

        $response = json_encode($this->SearchItem->getMemberSuggestion($this->request->query['name'],$project_id));
        $this->response->body($response);

        return $this->response;
    }

    public function noPlannedHour() {
        $this->Projects = TableRegistry::get('Projects');
        $this->Users    = TableRegistry::get('Users');
        $this->Tasks    = TableRegistry::get('Tasks');
        $users   = $this->Users->find('all')
                    ->where([
                        'is_outsource'  => 0
                    ]);

        $projects = $this->Projects->find('list');

        $tasks = $this->Tasks->find('all')
                    ->contain([
                        'DailyWorkResults' => function ($q) {
                            return $q
                            ->select(['duration' => $this->Tasks->find('all')->func()->sum('CAST(DailyWorkResults.actual_times as DECIMAL (4,2))')
                            , 'DailyWorkResults.task_id'])
                            ->contain(['Users' => function($q) {
                                return $q->autoFields(true);
                            }])
                            ->group(['DailyWorkResults.user_id', 'DailyWorkResults.task_id']);
                        },
                        'Projects',
                        'Milestones'
                    ])
                    ->where([
                        'Tasks.estimate_times' => 0,
                        'Tasks.is_ignored' => 0,
                    ])
                    ->toArray();

        $this->set(compact('projects', 'users', 'tasks'));
        $this->set('_serialize', ['projects', 'users', 'tasks']);
    }

    public function ignore() {
        $this->autoRender = false;
        $task_id = $this->request->getData('task_id');

        $tasks = TableRegistry::get("Tasks");
        $query = $tasks->query();
        $result = $query->update()
                ->set(['is_ignored' => 1])
                ->where(['id' => $task_id])
                ->execute();
    }

    public function getMemberAndMilestoneWithoutDuration()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);
        $this->Projects = TableRegistry::get('Projects');
        $response = json_encode($this->SearchItem->searchMemberAndMilestoneWithoutDuration($this->request->data));
        $this->response->body($response);
        return $this->response;
    }
}
