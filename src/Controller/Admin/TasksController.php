<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Tasks Controller
 *
 * @property \App\Model\Table\TasksTable $Tasks
 *
 * @method \App\Model\Entity\Task[] paginate($object = null, array $settings = [])
 */
class TasksController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('AdminUser');
        $this->loadComponent('Backlog');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Projects', 'Milestones']
        ];
        $tasks = $this->paginate($this->Tasks);
        $this->DailyWorkResults = TableRegistry::get('DailyWorkResults');
        // get daily_work_results by User id and $date_selected
        $daily_work_results = $this->Tasks->find('all')
            ->contain(
                [
                    'DailyWorkResults' => [
                        'sort' => ['DailyWorkResults.modified' => 'DESC'],
                        'Users' =>
                            function ($q) {
                                  return $q
                                      ->select(['id', 'name'])
                                      ->where([
                                                'is_outsource' => 0,
                                                'is_enable'    => 1
                                              ]);
                              }
                    ],
                    'Projects' => [
                        'fields' => [
                            'name',
                            'in_backlog'
                        ]
                    ]
                ]
            )->innerJoinWith(
                'DailyWorkResults', function ($q) {
                    return $q->where([
                                        'DailyWorkResults.work_date' => date('Y-m-d'),
                                        'DailyWorkResults.task_id = Tasks.id'
                                    ]);
                }
            )
            ->group(['Tasks.id'])
            ->order(['DailyWorkResults.modified' => 'DESC'])
            ->toArray();

        $all_grades = Configure::read('Grades');
        $this->set(compact('daily_work_results', 'all_grades'));
        $this->set('_serialize', ['tasks']);
    }

    /**
     * detail method
     *
     * @param  string|null $id Task id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function detail($id = null)
    {
        $task = $this->Tasks->get(
            $id, [
            'contain' => [
                    'Projects',
                    'DailyWorkResults' => [
                        'sort' => ['DailyWorkResults.modified' => 'DESC'],
                        'Users' => function ($q) {
                              return $q
                                  ->select(['id', 'name'])
                                  ->where([
                                            'is_outsource' => 0,
                                            'is_enable'    => 1
                                          ]);
                          }
                    ],
            ]
            ]
        )->toArray();
        $daily_works = [];
        foreach ($task['daily_work_results'] as $key => $value) {
            $daily_works[] = ['title' => $value['user']['name']. ' '.$value['actual_times'].'hrs', 'start' =>$value['work_date']->i18nFormat()];
        }
        $all_grades = Configure::read('Grades');
        $this->set(compact('task', 'daily_works', 'all_grades'));
        $this->set('task', $task);
        $this->set('_serialize', ['task']);
    }

    /**
     * GradeTask Method
     * responsible on saving grade on daily_work_results
     *
     * @return \Cake\Http\Response|void json
     */
    public function GradeTask()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);

        $data  = $this->request->getData();
        $entity = $this->Tasks->newEntity(
            $data, [
                    'accessibleFields' => ['id' => true],
                    'validate' => false
            ]
        );
        $grade = $this->Tasks->patchEntity($entity, $data, ['validate' => false]);

        if ($grade->getErrors()) {
            $error = array_map('current', $grade->getErrors());
            $response = json_encode(['status' => false,  'message' => $error]);
        } else {
            $response = json_encode(['status' => true,  'message' => __('Updated successfuly')]);
            if (!$this->Tasks->save($grade)) {
                $response = json_encode(['status' => false,  'message' => __('Update failed')]);
            }
        }

        $this->response->body($response);
        return $this->response;
    }

    /**
     * add method
     *
     * responsible for saving new task
     *
     * @return \Cake\Http\Response|json
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $entity = $this->Tasks->newEntity();

            $this->request->data['grade']              = 0;
            $this->request->data['is_close']           = 0;
            $this->request->data['milestone_id']       = null;
            $this->request->data['total_actual_times'] = 0;

            $patch_entity = $this->Tasks->patchEntity($entity, $this->request->data);

            // Saving Task
            if ($result = $this->Tasks->save($patch_entity)) {
                $response = json_encode(
                    [
                        'status'         => true,
                        'message'        => __('Record Added')
                    ]
                );
            } else {
                $error = array_map('current', $patch_entity->getErrors());
                $response = json_encode(['status' => false, 'message' => $error]);
            }

            $this->response->body($response);
            return $this->response;
        }

        // Get Projects table
        $this->Projects = TableRegistry::get('Projects');
        $projects = $this->Projects->find(
            'list', [
                'keyField'   => 'id',
                'valueField' => 'name'
            ]
        )
            ->where(
                [
                'in_backlog' => 0
                ]
            )
            ->order('name ASC')
            ->toArray();

        $this->set(compact('projects'));
        $this->set('_serialize', ['projects']);
    }

    /**
     * validateAddEdit method
     *
     * responsible for validating inputs
     *
     * @return \Cake\Http\Response|json
     */
    public function validateAddEdit()
    {
        if ($this->request->is('post')) {
            $entity = $this->Tasks->newEntity();

            if ($this->request->data['action'] == 'add') {
                $this->request->data['grade']    = 0;
                $this->request->data['is_close'] = 0;
            }

            $this->request->data['milestone_id']       = null;
            $this->request->data['total_actual_times'] = 0;

            // Remove isUnique validation if task_code is not changed
            if ( $this->request->data['action'] == 'edit'
                && ($this->request->data['old_task_code'] == $this->request->data['task_code'])
                && $this->request->data['task_code'] != '' ) {

                $patch_entity = $this->Tasks->patchEntity($entity, $this->request->data, ['validate' => 'Custom']);

            } else {
                $patch_entity = $this->Tasks->patchEntity($entity, $this->request->data);
            }

            // Validate input
            if (empty($patch_entity->getErrors())) {
                $response = json_encode(['status' => true]);
            } else {
                $error = array_map('current', $patch_entity->getErrors());
                $response = json_encode(['status' => false, 'message' => $error]);
            }

            $this->response->body($response);
            return $this->response;
        }
    }

    /**
     * edit method
     *
     * responsible for updating task
     *
     * @param  string|null $id Project id.
     * @return \Cake\Http\Response|json
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->is('post')) {
            $entity = $this->Tasks->newEntity();

            $this->request->data['milestone_id']       = null;
            $this->request->data['total_actual_times'] = 0;
            $this->request->data['id']                 = $id;

            // Remove isUnique validation if task_code is not changed
            if ($this->request->data['old_task_code'] == $this->request->data['task_code'] ) {
                $patch_entity = $this->Tasks->patchEntity(
                    $entity,
                    $this->request->data,
                    [
                        'validate' => 'Custom',
                        'accessibleFields' => ['id' => true]
                    ]
                );
            } else {
                $patch_entity = $this->Tasks->patchEntity(
                    $entity,
                    $this->request->data,
                    [
                        'accessibleFields' => ['id' => true]
                    ]
                );
            }

            // Saving Tasks
            if ($this->Tasks->save($patch_entity)) {
                $response = json_encode(
                    [
                        'status'    => true,
                        'message'   => __('Update Success')
                    ]
                );
            } else {
                $error = array_map('current', $patch_entity->getErrors());
                $response = json_encode(['status' => false, 'message' => $error]);
            }

            $this->response->body($response);
            return $this->response;
        }

        // Get Projects table
        $this->Projects = TableRegistry::get('Projects');

        // Throw Exception if project not found
        $tasks = $this->Tasks->find('all')
            ->contain(
                [
                    'Projects' => [
                        'fields' => [
                            'in_backlog'
                        ]
                    ]
                ]
            )
            ->where(
                [
                    'Projects.in_backlog' => 0,
                    'Tasks.id' => $id
                ]
            )
            ->first();

        $projects = $this->Projects->find(
            'list', [
                'keyField'   => 'id',
                'valueField' => 'name'
            ]
        )
            ->where(
                [
                'in_backlog' => 0
                ]
            )
            ->order('name ASC')
            ->toArray();

        if (empty($tasks)) {
            $this->Flash->error(__('Access denied'));
            return $this->redirect(['action' => 'index']);
        }

        $this->set(compact('tasks', 'projects'));
        $this->set('_serialize', ['tasks', 'projects']);

    }

    public function addBacklogIssue() {
        $this->Projects = TableRegistry::get('Projects');
        $this->Task     = TableRegistry::get('tasks');
        $projects       = $this->Projects->find('list')->toArray();

        if (!empty($this->request->query('project_id')) || !empty($this->request->query('keyword'))) {
            $data = $this->request->query;
            if ($data['project_id'] == 0) {
                $data['project_id'] = null;
            }
            $issues = $this->Backlog->getIssueByProjectOrKeyword($data['project_id'], $data['keyword']);
            $this->set('status', Configure::read('issue_status'));
            $this->set(compact('issues'));
        }

        $this->set(compact('projects'));
        $this->set('_serialize', ['projects']);
    }

    public function addIssue() {
        if ($this->request->is('Ajax')) {
            $data = $this->request->getData();
            $this->Task = TableRegistry::get('tasks');
            parse_str($data['data'], $data);
            //validaton is being applied there
            $entity = $this->Task->newEntity($data, [
                'accessibleFields' => ['id' => true],
                'validate'         => false
            ]);
            // If not save transaction rollback
            if ($this->Task->save($entity)) {
                $milestones = $this->Backlog->getAllMilestoneVersion($data['project_id']);
                if (!empty($milestones)) {
                    $this->data = [];
                    // filter milestone_version array for saving in milestones
                    foreach ($milestones as $key => $value) {
                        if ($value['id'] == $data['milestone_id']) {
                            if ($value['releaseDueDate'] != '') {
                                $datetime1 = strtotime($value['startDate']);
                                $datetime2 = strtotime($value['releaseDueDate']);
                                $total_estimate_time = ($datetime2 - $datetime1) / 3600;
                            } else {
                                $total_estimate_time = 0;
                            }

                            $this->data[] = [
                                'id'                  => $value['id'],
                                'project_id'          => $value['projectId'],
                                'name'                => $value['name'],
                                'total_estimate_time' => $total_estimate_time
                            ];
                        }
                    }
                    // Initiating Milestones Table
                    $this->milestones = TableRegistry::get('Milestones');
                    foreach ($this->data as $set) {
                        //validaton is being applied there
                        $entity = $this->milestones->newEntity(
                            $set, [
                            'validate' => false,
                            'accessibleFields' => ['id' => true]
                            ]
                        );
                        // If not save transaction rollback
                        if (!$this->milestones->save($entity, ['checkRules' => false])) {
                            $response = json_encode(['status' => false,  'message' => __('Has been failed to saved.')]);
                        }
                    }
                }
                $response = json_encode(['status' => true,  'message' => __('Has been successfully added.')]);
            } else {
                $response = json_encode(['status' => false,  'message' => __('Has been failed to saved.')]);
            }

            $this->response->body($response);
            return $this->response;
        }
    }
}
