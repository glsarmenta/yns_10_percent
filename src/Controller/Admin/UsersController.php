<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use App\Form\LoginForm;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\I18n\Time;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('AdminUser');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users   = $this->Users->find('all')
                    ->contain('MissRegistrationTasks',function($q) {
                            return $q->where(['MissRegistrationTasks.is_ignored' => 0]);
                    })
                    ->where([
                        'is_outsource'  => 0,
                    ]);
        $user_id = $this->Auth->user('id');
        $this->set(compact('users', 'user_id'));
        $this->set('_serialize', ['users', 'user_id']);
    }

    /**
     * Login method
     *
     * @return \Cake\Http\Response|void
     */
    public function login()
    {
        $this->request->session()->destroy();
        // Modelless intantiate for form validation
        $loginForm = new LoginForm();
        if ($this->request->is('post')) {
            if ($loginForm->execute($this->request->getData())) {
                // Auth Component identify function check if the given data is exist
                $user = $this->Auth->identify();
                if ($user) {
                    $redirectUrl = '/admin/users/login';
                    if ($user['role'] === 2) {
                        $this->Flash->error(__('Incorrect Login'));
                    } else {
                        $this->Auth->setUser($user);
                        $redirectUrl = '/admin/users';
                        return $this->redirect($redirectUrl);
                    }
                } else {
                    $this->Flash->error(__('Incorrect Login'));
                }
            } else {
                $this->Flash->error(__('Incorrect Login'));
            }
        }
        $this->set('loginForm', $loginForm);
    }

    /**
     * logout method
     * logout current user
     *
     * @return \Cake\Http\Response|void
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Task method
     *
     * @param string $id            = User id.
     * @param date   $date_selected = null|date.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function task($id, $date_selected = null)
    {
        // Throw Exception if user not found
        $this->Users = TableRegistry::get('Users');
        $user = $this->Users->get($id);

        // Get DailyReports table
        $this->DailyReports = TableRegistry::get('DailyReports');

        // Get DailyWorkResults table
        $this->DailyWorkResults = TableRegistry::get('DailyWorkResults');

        // set current date if $date_selected is null
        if ($date_selected == null) {
            $date_selected = date('Y-m-d');
        }

        // validate date
        $date = date_parse($date_selected);
        if ($date["error_count"] > 0
            || $date["warning_count"] > 0
            || isset($date['tz_abbr'])
            || !checkdate($date["month"], $date["day"], $date["year"])
            || strtotime($date_selected) == ''
        ) {
            $this->Flash->error(__('Invalid Date'));
            return $this->redirect(array('controller' => "users", 'action' => "task", $id));
        }

        // get daily_work_results by User id and $date_selected
        $daily_work_results = $this->DailyWorkResults->find('all')
            ->contain(
                [
                    'Tasks' => [
                        'Projects' => [
                            'fields' => [
                                'Projects.id',
                                'Projects.name'
                            ]
                        ]
                    ]
                ]
            )
            ->where(
                [
                   'work_date' => $date_selected,
                   'DailyWorkResults.user_id' => $id
                ]
            )
            ->toArray();

        $collection = new Collection($daily_work_results);

        // Create a new collection containing elements
        // with a isset value
        // Collection will be helpful to minimize database math functions
        $total_actual_time = $collection->sumOf(
            function ($value) {
                if (isset($value['actual_times'])) {
                    return $value['actual_times'];
                }
            }
        );

        // get daily report by $date_selected if there is
        $daily_report = $this->DailyReports
            ->findByUserIdAndBusinessDate($id, $date_selected)
            ->first();

        // default values
        $start_time      = 0;
        $end_time        = 0;
        $total_work_time = 0;

        // Compute for Total Work Time
        // total_work_time = end_time - start_time
        if (isset($daily_report)) {
            $start_time      = $daily_report->start_time->i18nFormat();
            $end_time        = $daily_report->end_time->i18nFormat();

            $total_work_time = ( (strtotime($end_time) - strtotime($start_time)) / 3600 ) - 1; // convert to hours; -1hr for lunch break
        }

        $this->set(compact('id', 'user', 'date_selected', 'start_time', 'end_time', 'total_work_time', 'total_actual_time', 'daily_work_results', 'daily_report'));
        $this->set('_serialize', ['id', 'user', 'date_selected', 'start_time', 'end_time', 'total_work_time', 'total_actual_time', 'daily_work_results', 'daily_report']);
    }

    /**
     * ChangeStatus Method
     *
     * @return \Cake\Http\Response|void json
     */
    public function ChangeStatus()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);

        // User id
        $user_id = $this->request->data['user_id'];

        // Error in changing status of currently logged in user
        if ($user_id == $this->Auth->user('id')) {
            $response = json_encode(['status' => false, 'message' => __('Invalid Action')]);
            $this->response->body($response);
            return $this->response;
        }

        // Get user based on $user_id
        $user = $this->Users->get($user_id);

        // Change value of is_enable
        $old_status = ($user['is_enable'] == 1 ) ? __('Enabled') : __('Disabled');
        $this->request->data['is_enable'] = ($user['is_enable'] == 1) ? 0 : 1;
        $new_status = ($this->request->data['is_enable'] == 1 ) ? __('Enabled') : __('Disabled');

        $user = $this->Users->patchEntity($user, $this->request->data);

        // Define role type
        $role = ($user['role'] == 1) ? __('Admin') : __('Member');

        if ($this->Users->save($user)) {
            $response = json_encode(
                [
                    'status'     => true,
                    'message'    => __('Update Success'),
                    'old_status' => $old_status,
                    'new_status' => $new_status,
                    'name'       => $user['name'],
                    'role'       => $role,
                    'email'      => $user['email']
                ]
            );

        } else {
            $error    = array_map('current', $user->getErrors());
            $response = json_encode(['status' => false, 'message' => $error]);
        }

        $this->response->body($response);
        return $this->response;
    }


    /**
     * getMissCount Method
     *
     * @return \Cake\Http\Response|void json
     */
    public function getMissCount(){
        $this->MissRegistrationTasks = TableRegistry::get('MissRegistrationTasks');
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);
        $user_id = $this->request->data['id'];

        $conditions['user_id']    = $user_id;
        $conditions['is_ignored'] = 0;

        if (isset($this->request->data['date'])) {
            if ($this->request->data['date'] != 'all') {
                $conditions['YEAR(miss_date)']  = date('Y', strtotime($this->request->data['date']));
                $conditions['MONTH(miss_date)'] = date('m', strtotime($this->request->data['date']));
                if ($this->request->data['date'] == date('Y-m-d',strtotime("-1 days"))) {
                    $conditions['DAY(miss_date)'] = date('d',strtotime("-1 days"));
                }
            }
        }

        $data   = $this->MissRegistrationTasks->find('all')
            ->where([$conditions])
            ->order('miss_date DESC')
            ->toArray();
        foreach ($data as $key => $value) {
            $data[$key]['miss_date'] = $data[$key]['miss_date']->i18nFormat('yyyy-MM-dd');
        }
        $response  = json_encode($data);
        $this->response->body($response);
        return $this->response;
    }

    public function ignoreMissCount() {
        $this->MissRegistrationTasks = TableRegistry::get('MissRegistrationTasks');
        $this->request->allowMethod(['ajax', 'post']);
        $id = $this->request->data['id'];

        $data = $this->MissRegistrationTasks->get($id);
        $data->is_ignored = 1;
        $this->MissRegistrationTasks->save($data);

        $response = json_encode(['status' => true, 'message'=> __('Data has been updated!')]);
        $this->response->body($response);
        return $this->response;
    }

    public function missRegistration($date = NULL) {
        $this->Users = TableRegistry::get('Users');

        $conditions['is_ignored'] = 0;
        if (!empty($date)) {
            $conditions['YEAR(miss_date)']  = date('Y', strtotime($date));
            $conditions['MONTH(miss_date)'] = date('m', strtotime($date));

            if ($date == date('Y-m-d',strtotime("-1 days"))) {
                $conditions['DAY(miss_date)'] = date('d',strtotime("-1 days"));
            }

            $this->set(compact('date'));
        }

        $users = $this->Users->find('all')
            ->contain('MissRegistrationTasks',function($q) use ($conditions) {
                return $q->where([$conditions]);
            })
            ->where([
                'is_outsource'  => 0,
            ]);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
}
