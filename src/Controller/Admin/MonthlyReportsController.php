<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
/**
 * DailyReports Controller
 *
 * @property \App\Model\Table\DailyReportsTable $DailyReports
 *
 * @method \App\Model\Entity\DailyReport[] paginate($object = null, array $settings = [])
 */
class MonthlyReportsController extends AppController
{
    public $uses = ['DailyWorkResult'];
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('AdminUser');
        $this->loadComponent('SearchItem');
    }

    /**
     * SummaryList Landing Page
     *
     * @return \Cake\Http\Response|void
     */
    public function SummaryList()
    {
        $currentYear    = date('Y');
        $currentMonth   = date('n'); //current month without leading zero

        $today = $currentYear . ' / ' . $currentMonth;

        // this function is for the range of year / month in the select tag
        $years = [];
        for($i =0; $i <= 5 ;$i++)
        {
            $year = date('Y') - 4 + $i;
            array_push($years, $year);
        }

        $this->set(compact('years', 'today'));
    }

    /**
     * Method for Ajax during select type onchange and onload
     *
     * @return \Cake\Http\Response|void
     */
    public function getMilestonePerMonth() {
        $this->request->allowMethod(['ajax', 'post']);
        $this->autoRender = false;

        $start_date = $this->request->getData('start_date');
        $end_date = $this->request->getData('end_date');
        $project_id = $this->request->getData('project_id');
        $condition = [
            'DailyWorkResults.work_date >' => $this->request->getData('start_date'),
            'DailyWorkResults.work_date <' => $this->request->getData('end_date')
        ];

        $conn = ConnectionManager::get('default');

        $stmt = $conn->execute("SELECT SUM(DailyWorkResults.actual_times) AS actual_time,
                                Milestones.name AS milestone,
                                Projects.name AS project_name,
                                Users.name AS user_name
                                FROM daily_work_results DailyWorkResults
                                INNER JOIN tasks Tasks
                                    ON DailyWorkResults.task_id = Tasks.id
                                INNER JOIN milestones Milestones
                                    ON Tasks.milestone_id = Milestones.id
                                INNER JOIN projects Projects
                                    ON Tasks.project_id = Projects.id
                                INNER JOIN project_users ProjectUsers
                                    ON Projects.id = ProjectUsers.project_id
                                INNER JOIN users Users
                                    ON ProjectUsers.user_id = Users.id
                                    AND DailyWorkResults.user_id = Users.id
                                WHERE DailyWorkResults.work_date > '". $start_date ."' AND DailyWorkResults.work_date < '" . $end_date ."' AND Users.is_outsource = 0
                                GROUP BY Users.id, Projects.id
                                ORDER BY Projects.id DESC");

        $rows = $stmt->fetchAll('assoc');


        $response = json_encode($rows);
        $this->response->body($response);
    }
}
