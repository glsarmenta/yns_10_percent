<?php
namespace App\Controller\Component;

use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\Controller\Component;

class SearchItemComponent extends Component
{

    /**
    * Display Milestone depending on the Project selected
    *
    * @return json
    */
    public function searchMatchingProject($data)
    {
        $this->Projects = TableRegistry::get('Projects');
        $this->Milestones = TableRegistry::get('Milestones');
        // Get project_id if user selected a project
        if ($data != null) {
            $project_data = $this->Projects->findById(key($data))->first();
            $conditions['conditions'] = ['Milestones.project_id' => $project_data['id']];
        }

        $milestones = $this->Milestones->find('list', $conditions);

        return $milestones;
    }


    /**
    * Suggest task code depending on selected milestone
    *
    * @return json
    */
    public function getTaskCodeSuggestion($user_id, $project_id, $milestone_id, $searchWord, $work_date, $limit = 5)
    {
        $conditions['task_code LIKE'] = '%'.$searchWord.'%';
        $conditions['is_close']       = 0;
        $conditions['project_id']     = $project_id;

        if ($milestone_id != '') {
            $conditions['milestone_id'] = $milestone_id;
        } else {
            $conditions['milestone_id IS '] = null;
        }

        $result = TableRegistry::get('Tasks')->find('list', [
                'keyField'   => 'id',
                'valueField' => 'task_code'
            ])
            ->notMatching('DailyWorkResults', function ($q) use ($user_id, $work_date) {
                return $q
                    ->where([
                        'DailyWorkResults.user_id'   => $user_id,
                        'DailyWorkResults.work_date' => $work_date
                    ]);
            })
            ->where([$conditions])
            ->order(['Tasks.modified' => 'DESC'])
            ->limit($limit);

        return $result;
    }

    public function searchMemberAndMilestone($data)
    {
        $this->ProjectUsers     = TableRegistry::get('ProjectUsers');
        $this->Milestones       = TableRegistry::get('Milestones');
        $this->DailyWorkResults = TableRegistry::get('DailyWorkResults');
        $this->Users            = TableRegistry::get('Users');

        $weekBefore = strtotime('-6 day', strtotime(date('Y-m-d')));

        $dailyWorkConditions   = [];
        $taskConditions        = [];
        $milestonesConditions  = [];
        $projectUsersCondition = [];
        $date_from             = "";
        $date_to               = "";

        //Check if member_id is not empty then set condition
        if (!empty($data['member_id'])) {
            $dailyWorkConditions['user_id IN']   = $data['member_id'];
            $projectUsersCondition['user_id IN'] = $data['member_id'];
        }

        //Check if project_id is not empty then set condition
        if (isset($data['project_id']) && $data['project_id'] != 0 ) {
            $taskConditions['project_id']        = $data['project_id'];
            $projectUsersCondition['project_id'] = $data['project_id'];
            $milestonesConditions['project_id']  = $data['project_id'];
        }

        if (isset($data['milestone_id']) && $data['milestone_id'] != 0) {
            $taskConditions['milestone_id'] = $data['milestone_id'];
        }

        //Check if estimated_hour is exists
        if (isset($data['estimate_times']) && !empty($data['estimate_times'])) {
            $taskConditions['estimate_times'] = 0;
        }

        //Check if date_from is not empty
        if (isset($data['date_from']) && !empty($data['date_from'])) {
            $date_from = $data['date_from'];
        } else {
            $date_from = date('Y-m-d', $weekBefore);
        }

        //Check if date_to is not empty
        if (isset($data['date_to']) && !empty($data['date_to'])) {
            $date_to = $data['date_to'];
        } else {
            $date_to = date('Y-m-d');
        }

        //Set condition for the work_date
        $dailyWorkConditions['work_date >='] = $date_from;
        $dailyWorkConditions['work_date <='] = $date_to;

        //Set condition for all not outsource user 0 = not_outsource, 1 = outsource
        $projectUsersCondition['is_outsource'] = 0;

        if (!empty($data)) {
            $result['Milestones'] = $this->Milestones->find('list')->where([$milestonesConditions]);
            $result['Members']    = $this->ProjectUsers->find('list', [
                    'keyField'   => 'user_id',
                    'valueField' => 'user.name'
                ])
                ->contain([
                    'Users'
                ])
                ->where([$projectUsersCondition])
                ->toArray();

            $result['Summary']['result'] =  $this->DailyWorkResults->find('all')
                ->where([$dailyWorkConditions])
                ->contain([
                    'Users' => [
                        'fields' => ['id','name']
                    ]
                ])
                ->contain('Tasks', function($res) use ($taskConditions) {
                    return $res
                    ->contain([
                        'Projects' => [
                            'fields' => ['name'],
                            'Milestones' => ['fields' => ['name']]
                        ]
                    ])
                    ->where([$taskConditions]);
                })
                ->order([
                    'Users.name' => 'DESC'
                ])
                ->toArray();
        }
        if (isset($data['project_id'])) {
            if (!empty($result['Summary']['result'])) {
                $totalHour = 0;
                $id        = "";

                foreach($result['Summary']['result'] as $key => $value) {
                    if (empty($id)) {
                        $id = $value['user']['id'];
                    }

                    if ($id == $value['user']['id']) {
                        $totalHour+=$value['actual_times'];
                    } else {
                        $id = $value['user']['id'];
                        $totalHour = $value['actual_times'];
                    }
                    $result['Summary']['total_hour'][$id] = $totalHour;
                }
            }
        }


        //Miss registration
        $conditions                                       = [];
        $conditions['MissRegistrationTasks.miss_date >='] = $date_from;
        $conditions['MissRegistrationTasks.miss_date <='] = $date_to;


        if (!empty($result['Members'])) {
            $customer_ids = [];
            foreach ($result['Members'] as $key => $value) {
                array_push($customer_ids, $key);
            }
        }
        $users = $this->Users->find('all')
            ->contain('MissRegistrationTasks', function($q) use ($conditions) {
                return $q->where([$conditions]);
            })
            ->where([
                'Users.is_outsource' => 0,
                'Users.id IN'        => $customer_ids
            ])->toArray();
        $result['no_planned_hour'] = $users;
        return $result;
    }

    public function getMemberSuggestion($name, $project_id)
    {
        $this->ProjectUsers = TableRegistry::get('ProjectUsers');

        $projectConditions = [];

        if (!empty($project_id)) {
            $projectConditions['project_id'] = $project_id;
        }

        $userConditions['name LIKE']    = $name.'%';
        $userConditions['is_outsource'] = 0;

        $result = $this->ProjectUsers->find('list', [
            'keyField' => 'user_id',
            'valueField' => 'user.name'
        ])
        ->where([$projectConditions])
        ->contain('Users', function($response) use ($userConditions) {
            return $response
            ->where([$userConditions]);
        });

        return $result;
    }

    /**
     * Get all Tasks without Estimation That is related to project_id and milestone_id
     *
     * @author Kevin
     * DB     : db_mngtool
     * Param  : $data ['member_id', 'project_id', 'milestone_id']
     */
    public function searchMemberAndMilestoneWithoutDuration($data)
    {
        $this->ProjectUsers     = TableRegistry::get('ProjectUsers');
        $this->Milestones       = TableRegistry::get('Milestones');
        $this->DailyWorkResults = TableRegistry::get('DailyWorkResults');

        $dailyWorkConditions   = [];
        $taskConditions        = [];
        $milestonesConditions  = [];
        $projectUsersCondition = [];
        $date_from             = "";
        $date_to               = "";

        $taskConditions['is_ignored'] = 0;

        //Check if member_id is not empty then set condition
        if (!empty($data['member_id'])) {
            $dailyWorkConditions['user_id IN']   = $data['member_id'];
            $projectUsersCondition['user_id IN'] = $data['member_id'];
        }

        //Check if project_id is not empty then set condition
        if (isset($data['project_id']) && $data['project_id'] != 0 ) {
            $taskConditions['project_id']        = $data['project_id'];
            $projectUsersCondition['project_id'] = $data['project_id'];
            $milestonesConditions['project_id']  = $data['project_id'];
        }

        if (isset($data['milestone_id']) && $data['milestone_id'] != 0) {
            $taskConditions['milestone_id'] = $data['milestone_id'];
        }

        //Check if estimated_hour is exists
        if (isset($data['estimate_times']) && !empty($data['estimate_times'])) {
            $taskConditions['estimate_times'] = 0;
        }

        //Set condition for all not outsource user 0 = not_outsource, 1 = outsource
        $projectUsersCondition['is_outsource'] = 0;

        if (!empty($data)) {
            $result['Milestones'] = $this->Milestones->find('list')
                    ->where([$milestonesConditions]);

            $result['Members'] = $this->ProjectUsers->find('list', [
                    'keyField'   => 'user_id',
                    'valueField' => 'user.name'
                ])
                ->contain([
                    'Users'
                ])
                ->where([$projectUsersCondition])
                ->toArray();

            $result['Summary']['result'] =  $this->DailyWorkResults->find('all')
                ->where([$dailyWorkConditions])
                ->select(['duration' => $this->DailyWorkResults->find('all')->func()->sum('CAST(DailyWorkResults.actual_times as DECIMAL (4,2))'), 'DailyWorkResults.task_id'])
                ->autoFields(true)
                ->contain([
                    'Users' => [
                        'fields' => ['id','name']
                    ]
                ])
                ->group(['DailyWorkResults.task_id', 'DailyWorkResults.user_id'])
                ->contain('Tasks', function($res) use ($taskConditions) {
                    return $res
                    ->contain([
                        'Projects' => [
                            'fields' => ['name'],
                            'Milestones' => ['fields' => ['name']]
                        ]
                    ])
                    ->where([$taskConditions]);
                })
                ->order([
                    'Users.name' => 'DESC'
                ])
                ->toArray();
        }
        if (isset($data['project_id'])) {
            if (!empty($result['Summary']['result'])) {
                $totalHour = 0;
                $id        = "";

                foreach($result['Summary']['result'] as $key => $value) {
                    if (empty($id)) {
                        $id = $value['user']['id'];
                    }

                    if ($id == $value['user']['id']) {
                        $totalHour+=$value['actual_times'];
                    } else {
                        $id = $value['user']['id'];
                        $totalHour = $value['actual_times'];
                    }
                    $result['Summary']['total_hour'][$id] = $totalHour;
                }
            }
        }
        return $result;
    }

}
