<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use atomita\Backlog;
use atomita\BacklogException;

class BacklogComponent extends Component
{
    protected $backlog = null;
    public function __construct()
    {
        $this->backlog = new Backlog('yns', 'L0jHKwsbLBqipKoWO922KJKkAI5mwizXIpoZnNfAyUkgKFTlg2UJQTQFoCQcISUj');
        try {
            // $space = $this->backlog->space->get();
        } catch (BacklogException $e) {
            // Halt Exception to continue the application
            // throw new BacklogException($e->getMessage(), 1);
        }
    }
    /**
    * getIssueByProjectAndUser method
    * Accepts null values in parameter
    * if value of parameter is null
    * no condition for that parameter
    * @param $project_id int Project Id unique id
    * @param $user_id int User id
    * @param [int] $count limit of data
    * @param [date] $created_since date start created (yyyy-MM-dd)
    * @param [date] $created_until date end date (yyyy-MM-dd)
    * @param [int] $status [1 => Open, 2 => In Progress, 3 => Resolved, 4 => Closed]
    * @return arrray
    */
    public function getIssueByProjectAndUser($project_id = null, $user_id = null, $count = 20, $created_since = null, $created_until = null, $status = null)
    {
        // null if no result
        $issues = $this->backlog->issues->get([
            'projectId[]'   => $project_id,
            'assigneeId[]'  => $user_id,
            'count'         => $count,
            'createdSince'  => $created_since,
            'createdUntil'  => $created_until,
            'statusId[]'    => $status,
            'sort'          => 'updated',
        ]);
        return $issues;
    }
    /**
    * getAllMilestoneVersion method
    * @param $project_id int Project Id unique id
    * @return null or arrray
    */
    public function getAllMilestoneVersion($project_id)
    {
        return $this->backlog->projects->$project_id->versions->get();
    }
    /**
    * getAllCurrentUsers method
    * @return null or arrray
    */
    public function getAllCurrentUsers()
    {
        return $this->backlog->users->get();
    }
    /**
    * getAllProjects method
    * @return null or arrray
    */
    public function getAllProjects()
    {
        // archived = true Get all deleted project ; false = get all active project
        // all = true = get all project ; false = get all project that api key holder belongs
        return $this->backlog->projects->get(['archived' => false, 'all' => true]);
    }
    /**
    * getAllMilestoneVersion method
    * @param $project_id int Project Id unique id
    * @return null or arrray
    */
    public function getAllProjectUsers($project_id)
    {
        return $this->backlog->projects->$project_id->users->get();
    }
    /**
    * getIssueByProjectOrKeyword method
    * @param $project_id int Project Id unique id
    * @param $keyword int Task code
    * @return rrray
    */
    public function getIssueByProjectOrKeyword($project_id = null, $keyword = null)
    {
        // null if no result
        $issues = $this->backlog->issues->get([
            'projectId[]' => $project_id,
            'keyword'     => $keyword,
            'count'       => 100,
            'sort'        => 'updated'
        ]);
        return $issues;
    }
}
