<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DailyWorkResults Controller
 *
 * @property \App\Model\Table\DailyWorkResultsTable $DailyWorkResults
 *
 * @method \App\Model\Entity\DailyWorkResult[] paginate($object = null, array $settings = [])
 */
class DailyWorkResultsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Tasks']
        ];
        $dailyWorkResults = $this->paginate($this->DailyWorkResults);

        $this->set(compact('dailyWorkResults'));
        $this->set('_serialize', ['dailyWorkResults']);
    }

    /**
     * View method
     *
     * @param string|null $id Daily Work Result id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dailyWorkResult = $this->DailyWorkResults->get($id, [
            'contain' => ['Users', 'Tasks']
        ]);

        $this->set('dailyWorkResult', $dailyWorkResult);
        $this->set('_serialize', ['dailyWorkResult']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dailyWorkResult = $this->DailyWorkResults->newEntity();
        if ($this->request->is('post')) {
            $dailyWorkResult = $this->DailyWorkResults->patchEntity($dailyWorkResult, $this->request->getData());
            if ($this->DailyWorkResults->save($dailyWorkResult)) {
                $this->Flash->success(__('The daily work result has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The daily work result could not be saved. Please, try again.'));
        }
        $users = $this->DailyWorkResults->Users->find('list', ['limit' => 200]);
        $tasks = $this->DailyWorkResults->Tasks->find('list', ['limit' => 200]);
        $this->set(compact('dailyWorkResult', 'users', 'tasks'));
        $this->set('_serialize', ['dailyWorkResult']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Daily Work Result id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dailyWorkResult = $this->DailyWorkResults->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dailyWorkResult = $this->DailyWorkResults->patchEntity($dailyWorkResult, $this->request->getData());
            if ($this->DailyWorkResults->save($dailyWorkResult)) {
                $this->Flash->success(__('The daily work result has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The daily work result could not be saved. Please, try again.'));
        }
        $users = $this->DailyWorkResults->Users->find('list', ['limit' => 200]);
        $tasks = $this->DailyWorkResults->Tasks->find('list', ['limit' => 200]);
        $this->set(compact('dailyWorkResult', 'users', 'tasks'));
        $this->set('_serialize', ['dailyWorkResult']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Daily Work Result id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dailyWorkResult = $this->DailyWorkResults->get($id);
        if ($this->DailyWorkResults->delete($dailyWorkResult)) {
            $this->Flash->success(__('The daily work result has been deleted.'));
        } else {
            $this->Flash->error(__('The daily work result could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
