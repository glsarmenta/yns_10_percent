<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Form\LoginForm;
use App\Form\EditPasswordForm;
use App\Form\ForgotPasswordForm;
use App\Form\ForgotPasswordCodeForm;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('User');
        $this->loadModel('Projects');
        $this->loadModel('Milestones');
        $this->loadModel('DailyWorkResults');
        $this->loadModel('DailyReports');
        $this->loadModel('ProjectUsers');
        $this->loadModel('CodeVerifications');
        $this->loadComponent('SearchItem');
    }

    public function beforeFilter(Event $event)
    {
        // allow only login, ForgotPassword, ForgotPasswordCode, ForgotPasswordInput, ForgotPasswordChangePassword
         $this->Auth->allow(['login', 'ForgotPassword', 'ForgotPasswordCode', 'ForgotPasswordInput', 'ForgotPasswordChangePassword']);
    }

    /**
     * Login method
     *
     * @return \Cake\Http\Response|void
     */
    public function login()
    {
        $this->request->session()->destroy();
        // Modelless intantiate for form validation
        $loginForm = new LoginForm();
        if ($this->request->is('post')) {
            if ($loginForm->execute($this->request->getData())) {
                // Auth Component identify function check if the given data is exist
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    $redirectUrl = '/users/productivity';
                    if ($user['password_updated'] === 0) {
                        $redirectUrl = '/users/edit_password';
                    }
                    return $this->redirect($redirectUrl);
                } else {
                    $this->Flash->error(__('Incorrect Login'));
                }
            } else {
                $this->Flash->error(__('Incorrect Login'));
            }
        }
        $this->set('loginForm', $loginForm);
    }
    /**
     * logout method
     * logout current user
     *
     * @return \Cake\Http\Response|void
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    /**
     * editPassword method
     *
     * @return \Cake\Http\Response|void
     */
    public function editPassword()
    {
        // Modelless intantiate for form validation
        $editForm = new EditPasswordForm();
        if ($this->request->is('post')) {
            $user = $this->Users->get($this->Auth->user('id'));
            if ($editForm->execute($this->request->getData())) {
                $user = $this->Users->patchEntity($user, $this->request->getData());
                if ($this->Users->save($user)) {
                    return $this->redirect(['action' => 'productivity']);
                }
                $this->Flash->error(__('Success'));
            } else {
                $this->Flash->error(__('Invalid Input'));
            }
        }
        $this->set('editForm', $editForm);
    }
    /**
     * Productivity method
     *
     * @param date $date_selected = null|date
     * @return \Cake\Http\Response|void
     */
    public function productivity($date_selected = null)
    {
        $id = $this->Auth->user('id');

        // set current date if $date_selected is null
        if ($date_selected == null) {
            $date_selected = date('Y-m-d');
        }

        // validate date
        $date = date_parse($date_selected);

        if ($date["error_count"] > 0
            || $date["warning_count"] > 0
            || isset($date['tz_abbr'])
            || !checkdate($date["month"], $date["day"], $date["year"])
            || strtotime($date_selected) == ''
        ) {
            $this->Flash->error(__('Invalid Date'));
            return $this->redirect(array('controller' => "users", 'action' => "productivity"));
        }

        $all_projects = $this->ProjectUsers->find('all',
            [
                'keyField'   => 'project_id',
                'valueField' => 'project.name'
            ])
            ->contain([
                'Projects'
            ])
            ->where([
                'user_id' =>$id
            ])
            ->toArray();
        $project_lists = [];
        $project_common = [];
        foreach ($all_projects as $key => $value) {
            $project_lists[$value->project->id] = $value->project->name;
            // if project is not in backlog and not common
            if ($value->project->in_backlog == 0 && $value->project->is_common == 0) {
                $project_common[] = $value->project->id;
            }
        }
        $all_projects = $project_lists;

        // Get all daily_work_results
        // convert Object to Array
        $daily_work_result = $this->DailyWorkResults->find('all')
            ->where([
                'Users.id ' => $id,
                'DailyWorkResults.work_date ' => $date_selected
            ])
            ->contain([
                    'Users' => [
                        'fields' => [
                            'Users.id',
                        ],
                    ],
                    'Tasks' => [
                        'fields' => [
                            'Tasks.id',
                            'Tasks.project_id',
                            'Tasks.milestone_id',
                            'Tasks.task_code',
                            'Tasks.name',
                            'Tasks.estimate_times',
                            'Tasks.total_actual_times',
                        ],
                        'Projects' => [
                            'fields' => [
                                'Projects.id',
                                'Projects.name',
                            ]
                        ],
                        'Milestones' => [
                            'fields' => [
                                'Milestones.id',
                                'Milestones.project_id',
                                'Milestones.name',
                            ]
                        ]
                    ]
                ])->toArray();

        $collection = new Collection($daily_work_result);

        // Create a new collection containing elements
        // with a isset value
        // Collection will be helpful to minimize database math functions
        $total_actual_time = $collection->sumOf(function ($value) {
            if (isset($value['actual_times'])) {
                return $value['actual_times'];
            }
        });

        // Get Todays daily report if there is
        $daily_report = $this->DailyReports
            ->findByUserIdAndBusinessDate($this->Auth->user('id'), $date_selected)
            ->first();
        $total_work_time = 0;
        $end_time = '';

        // Compute for Total Work Time
        // total_work_time = end_time - start_time
        if (isset($daily_report)) {
            $start_time      = $daily_report->start_time->i18nFormat();
            $end_time        = $daily_report->end_time->i18nFormat();
            $total_work_time = (strtotime($end_time) - strtotime($start_time)) / 3600; // convert to hours
        }

        $this->set(compact('id', 'date_selected', 'daily_work_result', 'all_projects',
         'all_milestones', 'daily_report', 'total_work_time', 'total_actual_time',
         'end_time', 'project_common'));
    }

    /**
     * validateProductivityAdd method
     * responsible for validating daily_work_results
     * @return \Cake\Http\Response|json
     */
    public function validateProductivityAdd()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);

        // Get DailyWorkResults table
        $this->DailyWorkResults = TableRegistry::get('DailyWorkResults');

        // Get Tasks table
        $this->Tasks = TableRegistry::get('Tasks');
        // Get Projects tables
        $this->Projects = TableRegistry::get('Projects');
        // newEntity find DailyWorkResults
        $entity = $this->DailyWorkResults->newEntity();

        // Get Tasks depending on task_code
        // Check if task_code is present in tasks table
        $milestone_id = $this->request->data['milestone_id'] !== ''
                        ? ['milestone_id' => $this->request->data['milestone_id'] ]
                        : ['milestone_id IS NULL'];

        $tasks = $this->Tasks->findByProjectIdAndTaskCode(
                    $this->request->data['project_id'],
                    $this->request->data['task_code'],
                    $milestone_id
                )
                ->where([$milestone_id])
                ->contain([
                        'Projects',
                        'Milestones'
                    ])
                ->first();

        if ($tasks != '') {
            $tasks = $tasks->toArray();
        } else {
            $task_is_saved = false;
            $project_data = $this->Projects->findById($this->request->data['project_id'])
                            ->first();
            // if the Project is not from backlog, Task can be added to Database
            if ($project_data !== '' && $project_data->in_backlog == 0
                && $project_data->is_common == 0) {

                $task_data = [
                    'milestone_id'      => NULL,
                    'project_id'        => $this->request->data['project_id'],
                    'task_code'         => $this->request->data['task_code'],
                    'name'              => $this->request->data['task_code'],
                    'estimate_times'    => $this->request->data['estimate_times'],
                    'is_close'          => 0,
                    'actual_times'      => $this->request->data['actual_times'],
                    'total_actual_times'=> 0
                ];
                // Creating New Entity for Tasks and validation for task_data
                $task_entity = $this->Tasks->newEntity($task_data);

                if ($task_entity->getErrors()) {
                    $error = array_map('current', $task_entity->getErrors());
                    $response = json_encode(['status' => false, 'message' => $error]);
                    $this->response->body($response);
                    return $this->response;
                } else {
                    $result = $this->Tasks->save($task_entity);
                    $tasks['id'] = $task_entity->id;
                    $task_is_saved = true;
                }
            }
            if (!$task_is_saved) {
                $error = array_map('current', [
                        'task_code' => [
                            '_empty' => __('Record Not Found')
                        ]
                    ]);
                $response = json_encode(['status' => false, 'message' => $error]);
                $this->response->body($response);
                return $this->response;
            }
        }

        // Check if same user_id and task_id is present in daily_work_results table
        $exist_dwr = $this->DailyWorkResults->find('list')
                ->where([
                        'DailyWorkResults.user_id'   => $this->Auth->user('id'),
                        'DailyWorkResults.task_id'   => $tasks['id'],
                        'DailyWorkResults.work_date' => $this->request->data['work_date']
                    ])
                ->limit(1)
                ->toArray();
        if (!empty($exist_dwr)) {
            $error = array_map('current', [
                    'task_code' => [
                        '_duplicate' => __("User can't have same task")
                    ]
                ]);
            $response = json_encode(['status' => false, 'message' => $error]);
            $this->response->body($response);
            return $this->response;
        }

        $this->request->data['user_id']   = $this->Auth->user('id');
        $this->request->data['task_id']   = $tasks['id'];
        unset($this->request->data['task_code']);

        // patchEntity is merging of given data in a find entity
        $entity = $this->DailyWorkResults->patchEntity($entity, $this->request->data);

        // Used for updating tasks.total_actual_times
        $update_task       = $this->Tasks->get($entity['task_id']);
        $task_tot_act_time = $update_task['total_actual_times'] + $entity['actual_times']; // tasks.total_actual_times + daily_work_results.actual_times
        $new_tot_act_time  = ['total_actual_times' => $task_tot_act_time];
        $update_task       = $this->Tasks->patchEntity($update_task, $new_tot_act_time);

        // if Save is failed throw json with status false
        if ($entity->getErrors()) {
            $error = array_map('current', $entity->getErrors());
            $response = json_encode(['status' => false, 'message' => $error]);
        } else {
            $response = json_encode(['status' => true]);
        }

        $this->response->body($response);
        return $this->response;
    }

    /**
     * productivityAdd method
     * responsible for saving daily_work_results
     * @return \Cake\Http\Response|json close modal then update tasks table.
     */
    public function productivityAdd()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);

        // Get DailyWorkResults table
        $this->DailyWorkResults = TableRegistry::get('DailyWorkResults');

        // Get Tasks table
        $this->Tasks = TableRegistry::get('Tasks');

        // newEntity find DailyWorkResults
        $entity = $this->DailyWorkResults->newEntity();

        $milestone_id = $this->request->data['milestone_id'] !== ''
                        ? ['milestone_id' => $this->request->data['milestone_id'] ]
                        : ['milestone_id IS NULL'];

        // Get Tasks depending on task_code
        $tasks = $this->Tasks->findByProjectIdAndTaskCode(
                    $this->request->data['project_id'],
                    $this->request->data['task_code'],
                    $milestone_id
                )
                ->where([$milestone_id])
                ->contain([
                        'Projects',
                        'Milestones'
                    ])
                ->first();

        $this->request->data['user_id']   = $this->Auth->user('id');
        $this->request->data['task_id']   = $tasks['id'];
        unset($this->request->data['task_code']);

        // patchEntity is merging of given data in a find entity
        $entity = $this->DailyWorkResults->patchEntity($entity, $this->request->data);

        // Used for updating tasks.total_actual_times
        $update_task       = $this->Tasks->get($entity['task_id']);
        $task_tot_act_time = $update_task['total_actual_times'] + $entity['actual_times']; // tasks.total_actual_times + daily_work_results.actual_times
        $new_tot_act_time  = ['total_actual_times' => $task_tot_act_time];
        $update_task       = $this->Tasks->patchEntity($update_task, $new_tot_act_time);

        // if Save is failed throw json with status false
        if ($result = $this->DailyWorkResults->save($entity)) {
            // Update tasks.total_actual_times table
            if ($this->Tasks->save($update_task)) {
                $response = json_encode([
                        'status'    => true,
                        'message'   => __('Record Added'),
                        'task_name' => h($tasks['name']),
                        'proj_id'   => $tasks['project']['id'],
                        'proj_name' => h($tasks['project']['name']),
                        'mile_id'   => $tasks['milestone']['id'],
                        'mile_name' => h($tasks['milestone']['name']),
                        'act_time'  => $this->request->data['actual_times'],
                        'dwr_id'    => $result->id // daily_work_results.id
                    ]);
            }
        } else {
            $error = array_map('current', $entity->getErrors());
            $response = json_encode(['status' => false, 'message' => $error]);
        }

        $this->response->body($response);
        return $this->response;
    }

    /**
     * searchMatchingProject method
     *
     * @return json
     */
    public function searchMatchingProject()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);
        // default response
        $response = json_encode($this->SearchItem->searchMatchingProject($this->request->data));
        $this->response->body($response);
        return $this->response;
    }

    /**
     * suggestTaskCode method
     *
     * @return json
     */
    public function suggestTaskCode()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);

        // User id
        $user_id = $this->Auth->user('id');

        // default response
        $response = json_encode([]);

        if (isset($this->request->query['text']) && $this->request->query['text'] != '') {
            $response = json_encode($this->SearchItem->getTaskCodeSuggestion($user_id, $this->request->query['project_id'], $this->request->query['milestone_id'], $this->request->query['text'], $this->request->query['work_date'] ));
        }
        $this->response->body($response);
        return $this->response;
    }

    /**
     * searchMatchingTask method
     *
     * @return json
     */
    public function searchMatchingTask()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);

        // Get Tasks table
        $this->Tasks = TableRegistry::get('Tasks');

        // Get Tasks depending on task_code
        // Check if task_code is present in tasks table
        $milestone_id = $this->request->data['mile_id'] !== ''
                        ? ['milestone_id' => $this->request->data['mile_id'] ]
                        : ['milestone_id IS NULL'];

        $tasks = $this->Tasks->findByProjectIdAndTaskCode(
                    $this->request->data['proj_id'],
                    $this->request->data['task_code'],
                    $milestone_id
                )
                ->where([$milestone_id])
                ->contain([
                        'Projects',
                        'Milestones'
                    ])
                ->first();

        // default response
        $response = json_encode(['status' => false, 'estimate_times' => 0]);

        if (isset($tasks) || $tasks != '') {
            $set_estimate_time = ($tasks['estimate_times'] == '' || $tasks['estimate_times'] == null) ? 0 : $tasks['estimate_times'];
            $response = json_encode(['status' => true, 'estimate_times' => $set_estimate_time]);
        }

        $this->response->body($response);
        return $this->response;
    }

    /**
     * ProductivityDelete method
     * responsible for deleting daily work results
     * @return \Cake\Http\Response|void json
     */
    public function ProductivityDelete()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);
        // Get Tasks table
        $this->Tasks = TableRegistry::get('Tasks');
        //Get DailyWorkResults Table
        $this->DailyWorkResults = TableRegistry::get('DailyWorkResults');
        // Get first task results by id
        $entity = $this->DailyWorkResults->get($this->request->data['deleted_id']);
        // default response
        $response = json_encode(['status' => true, 'message' => __('Record Deleted')]);
        // if the record not found
        if (empty($entity)) {
            $response = json_encode(['status' => false, 'message' => __('Record Not Found')]);
        } else {
            // Used for updating tasks.total_actual_times
            $update_task       = $this->Tasks->get($entity['task_id']);
            // Get Project Data
            $this->Projects    = TableRegistry::get('Projects');
            $project_data      = $this->Projects->findById($update_task->project_id)->first();

            $task_tot_act_time = $update_task['total_actual_times'] - $entity['actual_times']; // tasks.total_actual_times - daily_work_results.actual_times
            $new_tot_act_time  = ['total_actual_times' => $task_tot_act_time];
            $update_task       = $this->Tasks->patchEntity($update_task, $new_tot_act_time);

            if ($project_data->in_backlog == 0 && $project_data->is_common == 0) {
                $new_tot_act_time  = [
                                        'total_actual_times' => $task_tot_act_time,
                                        'deleted_date'       => date('Y-m-d h:i:s')
                                    ];
            }

            $update_task       = $this->Tasks->patchEntity($update_task, $new_tot_act_time);
            $this->Tasks->save($update_task);
            $this->DailyWorkResults->delete($entity);
        }
        $this->response->body($response);
        return $this->response;
    }

    /**
     * validateProductivityEdit method
     * responsible for validating daily_work_results
     * @return \Cake\Http\Response|json
     */
    public function validateProductivityEdit()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);
        // Get DailyWorkResults table
        $this->DailyWorkResults = TableRegistry::get('DailyWorkResults');

        // Get Tasks table
        $this->Tasks = TableRegistry::get('Tasks');

        // Get Tasks depending on task_code
        // Check if task_code is present in tasks table
        $milestone_id = $this->request->data['milestone_id'] !== ''
                        ? ['milestone_id' => $this->request->data['milestone_id'] ]
                        : ['milestone_id IS NULL'];
        $tasks = $this->Tasks->findByProjectIdAndTaskCode(
                    $this->request->data['project_id'],
                    $this->request->data['task_code'],
                    $milestone_id
                )
                ->contain([
                        'Projects',
                        'Milestones'
                    ])
                ->first();

        if ($tasks != '') {
            $tasks = $tasks->toArray();
        } else {
            $error = array_map('current', [
                    'task_code' => [
                        '_empty' => __('Record Not Found')
                    ]
                ]);
            $response = json_encode(['status' => false, 'message' => $error]);
            $this->response->body($response);
            return $this->response;
        }

        // Check if same user_id and task_id is present in daily_work_results table
        if ($this->request->data['original_task_code'] != $this->request->data['task_code']) {
            $exist_dwr = $this->DailyWorkResults->find('list')
                    ->where([
                            'DailyWorkResults.user_id'   => $this->Auth->user('id'),
                            'DailyWorkResults.task_id'   => $tasks['id'],
                            'DailyWorkResults.work_date' => $this->request->data['work_date']
                        ])
                    ->limit(1)
                    ->toArray();
            if (!empty($exist_dwr)) {
                $error = array_map('current', [
                        'task_code' => [
                            '_duplicate' => __("User can't have same task")
                        ]
                    ]);
                $response = json_encode(['status' => false, 'message' => $error]);
                $this->response->body($response);
                return $this->response;
            }
        }
        $this->request->data['user_id'] = $this->Auth->user('id');
        $this->request->data['task_id'] = $tasks['id'];
        $milestone_name = ($this->request->data['milestone_id'] !== '') ? $tasks['milestone']['name'] : '';

        // Get first task results by id
        $entity       = $this->DailyWorkResults->get($this->request->data['id']);
        $old_act_time = $entity['actual_times'];

        // if the record not found
        if (empty($entity)) {
            $error = array_map('current', $entity->getErrors());
            $response = json_encode(['status' => false, 'message' => $error]);
        } else {
            // patchEntity is merging of given data in a find entity
            $daily_works = $this->DailyWorkResults->patchEntity($entity, $this->request->getData());

            // if Update is failed throw json with status false
            if (!$daily_works->getErrors()) {
                $response = json_encode(['status' => true]);
            } else {
                $error = array_map('current', $entity->getErrors());
                $response = json_encode(['status' => false, 'message' => $error]);
            }

        }
        $this->response->body($response);
        return $this->response;
    }

    /**
     * productivityEdit method
     * responsible for editing daily work results
     * @return \Cake\Http\Response|void json
     */
    public function productivityEdit()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);
        // Get DailyWorkResults table
        $this->DailyWorkResults = TableRegistry::get('DailyWorkResults');

        // Get Tasks table
        $this->Tasks = TableRegistry::get('Tasks');

        // Get Tasks depending on task_code
        // Check if task_code is present in tasks table
        $milestone_id = $this->request->data['milestone_id'] !== ''
                        ? ['milestone_id' => $this->request->data['milestone_id'] ]
                        : ['milestone_id IS NULL'];

        $tasks = $this->Tasks->findByProjectIdAndTaskCode(
                    $this->request->data['project_id'],
                    $this->request->data['task_code'],
                    $milestone_id
                )
                ->contain([
                        'Projects',
                        'Milestones'
                    ])
                ->first();

        $this->request->data['user_id'] = $this->Auth->user('id');
        $this->request->data['task_id'] = $tasks['id'];
        $milestone_name = ($this->request->data['milestone_id'] !== '') ? $tasks['milestone']['name'] : '';

        // Get first task results by id
        $entity       = $this->DailyWorkResults->get($this->request->data['id']);
        $old_act_time = $entity['actual_times'];

        // patchEntity is merging of given data in a find entity
        $daily_works = $this->DailyWorkResults->patchEntity($entity, $this->request->getData());

        // Used for updating tasks.total_actual_times
        $new_actual_time   = $daily_works['actual_times'] - $old_act_time;
        $update_task       = $this->Tasks->get($entity['task_id']);
        $task_tot_act_time = $update_task['total_actual_times'] + $new_actual_time; // tasks.total_actual_times - daily_work_results.actual_times
        $new_tot_act_time  = [
                                'total_actual_times' => $task_tot_act_time,
                                'estimate_times' => $this->request->data['estimate_times'],
                                'estimate_times' => $this->request->data['estimate_times'],
                             ];
        $update_task       = $this->Tasks->patchEntity($update_task, $new_tot_act_time);

        // if Update is failed throw json with status false
        if ($result = $this->DailyWorkResults->save($daily_works)) {
            if ($this->Tasks->save($update_task)) {
                $response = json_encode([
                        'status'     => true,
                        'message'    => __('Update Success'),
                        'task_id'    => $tasks['id'],
                        'task_name'  => h($tasks['name']),
                        'task_code'  => h($this->request->data['task_code']),
                        'proj_id'    => $tasks['project']['id'],
                        'proj_name'  => h($tasks['project']['name']),
                        'mile_id'    => $milestone_id,
                        'mile_name'  => h($milestone_name),
                        'act_times'  => $this->request->data['actual_times'],
                        'est_times'  => $this->request->data['estimate_times'],
                        'dwr_id'     => $result->id // daily_work_results.id
                    ]);
            }
        } else {
            $error = array_map('current', $entity->getErrors());
            $response = json_encode(['status' => false, 'message' => $error]);
        }

        $this->response->body($response);
        return $this->response;
    }

    /*
     * Responsible for validating daily_reports form
     *
     */
    public function validateAddDailyWork()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);

        // Get DailyReports table
        $this->DailyReports = TableRegistry::get('DailyReports');

        $daily_report       = $this->DailyReports->newEntity(
                                $this->request->getData(),
                                [
                                    'accessibleFields' => ['id' => true]
                                ]
        );

        if ($daily_report->getErrors()) {
            $error = array_map('current', $daily_report->getErrors());
            $response = json_encode(['status' => false,  'message' => $error]);

        } else {
            $response = json_encode(['status' => true]);
        }

        $this->response->body($response);
        return $this->response;
    }

    /*
     * Responsible for saving daily_reports
     *
     */
    public function AddDailyWork()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);

        // Get DailyReports table
        $this->DailyReports = TableRegistry::get('DailyReports');

        $data  = $this->request->getData();

        $daily_report = $this->DailyReports->newEntity(
            $data,
            [
                'accessibleFields' => ['id' => true]
            ]
        );

        $daily_report->start_time = date('H:i', strtotime($data['start_time']));
        $daily_report->end_time   = date('H:i', strtotime($data['end_time']));

        $response = json_encode(['status' => true,  'message' => __('Added successfuly')]);

        if (!$this->DailyReports->save($daily_report)) {
            $response = json_encode(['status' => false,  'message' => __('Added failed')]);
        }

        $this->response->body($response);
        return $this->response;
    }

    /**
     * ForgotPassword method
     * responsible for sending confirmation via email
     * @return \Cake\Http\Response|void
     */
    public function ForgotPassword()
    {
        // Clear Sessions
        $this->request->session()->destroy();

        // Modelless intantiate for form validation
        $forgotPasswordForm = new ForgotPasswordForm();

        // Get CodeVerifications table
        $this->CodeVerifications = TableRegistry::get('CodeVerifications');

        if ($this->request->is('post')) {

            if ($forgotPasswordForm->execute($this->request->getData())) {
                $get_email = $this->request->data['email'];

                $user = $this->Users->find('list')
                                    ->where(['email' => $get_email])
                                    ->toArray();

                // If email don't exist in users table
                if (empty($user)) {
                    $this->set('forgotPasswordForm', $forgotPasswordForm);
                    return $this->Flash->error(__('Email do not exist'));
                }

                $email = new Email('default');

                // Generate random string for code
                $code = $this->__GenerateCode();

                $code_verification = ['user_id' => key($user),
                                      'code'    => $code];
                $code_verification = $this->CodeVerifications->newEntity($code_verification);

                $send_mail = $email->transport('gmail')
                                   ->to($get_email)
                                   ->from('yns.management.system@gmail.com')
                                   ->emailFormat('html')
                                   ->template('change_password_mail')
                                   ->viewVars([
                                        'user_name' => $user[key($user)],
                                        'user_id'   => key($user),
                                        'email'     => $get_email,
                                        'code'      => $code
                                    ])
                                   ->subject(__('YNS Management System Email Confirmation'))
                                   ->send();

                if ($send_mail == true) {
                    if ($this->CodeVerifications->save($code_verification)) {
                        $this->Flash->success(__('An email with a verification code was just sent to ') . $get_email);
                        return $this->redirect(['action' => 'ForgotPasswordCode']);
                    }
                    $this->Flash->error(__('Error sending mail'));
                } else {
                    $this->Flash->error(__('Error sending mail'));
                }
            }
        }

        $this->set('forgotPasswordForm', $forgotPasswordForm);
    }

    /**
     * GenerateCode method
     * responsible for generating unique code
     * @return String
     */
    private function __GenerateCode()
    {
        $code     = "";
        $alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for ($i = 0; $i < 8; $i++) {
            $code .= $alphabet[rand(0, strlen($alphabet) - 1)];
        }

        // Check if code already exists in code_verifications table
        $code_exists = $this->CodeVerifications->find('list')
                                                ->where([
                                                    'code' => $code
                                                ])
                                                ->first();
        if ($code_exists != null) {
            $this->__GenerateCode();
        }

        return $code;
    }

    /**
     * ForgotPasswordCode method
     * responsible for validating code
     * @return \Cake\Http\Response|null
     */
    public function ForgotPasswordCode()
    {
        $session_id = $this->Auth->user('id');

        if ($session_id != '') {
            return $this->redirect(['action' => 'productivity']);
        }

        // Modelless intantiate for form validation
        $forgot_password_codeForm = new ForgotPasswordCodeForm();

        // Get CodeVerifications table
        $this->CodeVerifications = TableRegistry::get('CodeVerifications');

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if ($forgot_password_codeForm->execute($data)) {

                $get_code = $this->CodeVerifications->find('all')
                                                    ->where([
                                                        'code' => $data['code']
                                                    ])
                                                    ->first();
                // Check if code exist
                if (empty($get_code)) {
                    $this->Flash->error(__('Code not found'));

                } else {
                    $created_datetime = $get_code->created->i18nFormat('yyyy-MM-dd HH:mm:ss');
                    $current_datetime = date('Y-m-d H:i:s');

                    $diff = (strtotime($current_datetime) - strtotime($created_datetime));
                    $time = date('i', $diff);

                    // Check if code is expired
                    if ($time < 30) {
                        // Verify latest code by user_id and code
                        $latest_code = $this->CodeVerifications->find('all')
                            ->where([
                                'user_id' => $get_code['user_id']
                            ])
                            ->order([
                                'created' => 'DESC'
                            ])
                            ->first();

                        // Check if entered code is the latest available code in code_verifications table
                        if ($data['code'] == $latest_code['code']) {
                            $this->request->session()->write(['code' => $latest_code['code']]);
                            return $this->redirect([
                                'controller' => 'Users',
                                'action' => 'ForgotPasswordInput'
                            ]);

                        } else {
                            $this->Flash->error(__('Code not found'));
                        }
                    } else {
                        $this->Flash->error(__('Code not found'));
                    }
                }
            }
        }
        $this->set('forgot_password_codeForm', $forgot_password_codeForm);
    }

    /**
     * ForgotPasswordInput method
     * responsible for validating user's password
     * @return \Cake\Http\Response|json
     */
    public function ForgotPasswordInput()
    {
        // Get CodeVerifications table
        $this->CodeVerifications = TableRegistry::get('CodeVerifications');

        // Modelless intantiate for form validation
        $editForm = new EditPasswordForm();

        $code = $this->request->session()->read('code');

        if ($code == '') {
            return $this->redirect(['action' => 'login']);
        }

        if ($this->request->is('post', 'ajax')) {

            // Get user_id by $code on code_verifications table
            $user_id = $this->CodeVerifications->find('list', [
                    'keyField'   => 'id',
                    'valueField' => 'user_id'
                ])
                ->where([
                    'code' => $code
                ])
                ->first();

            $user = $this->Users->get($user_id);

            $data = $this->request->getData();

            if ($editForm->execute($data)) {
                $response = json_encode([
                    'status'   => true,
                    'user_id'  => $user['id'],
                    'password' => $data['password']
                ]);
                $this->response->body($response);
                return $this->response;
            } else {
                $errors = $editForm->errors();
                $response = json_encode([
                    'status'  => false,
                    'message' => $errors
                ]);
                $this->response->body($response);
                return $this->response;

            }

        }

        $this->set(compact('editForm'));
        $this->set('_serialize', ['editForm']);
    }


    /**
     * ForgotPasswordChangePassword method
     * responsible for changing user's password
     * @return \Cake\Http\Response|json
     */
    public function ForgotPasswordChangePassword()
    {
        // Only accept Ajax and Post requests
        $this->request->allowMethod(['ajax', 'post']);

        // Get Users table
        $this->Users = TableRegistry::get('Users');

        $data = $this->request->getData();

        $user = $this->Users->get($data['id']);

        $user = $this->Users->patchEntity($user, $data);

        if ($result = $this->Users->save($user)) {
            // Retrieve user from DB
            $authUser = $this->Users->get($result->id)->toArray();

            // Log user in using Auth
            $this->Auth->setUser($authUser);

            $this->Flash->success(__('Password has been changed'));

            $this->request->session()->delete('code');

            $response = json_encode(['status'  => true]);
            $this->response->body($response);
            return $this->response;
        } else {
            $this->Flash->error(__('Error in changing password'));
        }
    }
}
