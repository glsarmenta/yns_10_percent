<!--modal for deleteModal-->
<div class="modal fade" tabindex="-1" role="dialog" id="errorModal">
  <div class="modal-dialog" role="document">
      <div class="modal-content tms__modal">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo __('Error'); ?></h4>
        </div>
        <div class="modal-body">
          <h5><span id="errorMsg"></span></h5>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" id="successBtn" data-dismiss="modal"><?php echo __('Ok'); ?></button>
        </div>
      </div>
    </div>
</div><!-- /.modal-dialog -->