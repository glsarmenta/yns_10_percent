<div class="modal fade" tabindex="-1" role="dialog" id="confirmation_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content tms__modal">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= __('Confirmation'); ?></h4>
        </div>

        <div class="modal-body">
          <h5 class="modal-message"></h5>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= __('Close'); ?></button>
          <button type="button" class="btn btn-success submitForm"><?= __('Yes'); ?></button>
        </div>
      </div>
    </div>
</div>