<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
  .ui-autocomplete { position: absolute; cursor: default; z-index: 99999}
</style>

<div class="container tms">
  <div class="col-md-6 col-md-offset-3 tms__wrapper-margin">
    <?= $this->Flash->render() ?>
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title"><?= __('YNS Productivity Time') ?></h3>
      </div>
      <!-- /.box-header -->

      <?= $this->Form->create($daily_report, ['class'=>'form-horizontal', 'id' => 'formDailyWork']); ?>

      <?= $this->Form->hidden('user_id', [
            'required' => false,
            'value' => $id
            ]);
      ?>
      <?= $this->Form->hidden('id', [
          'required' => false,
          ]);
      ?>
      <div class="form-horizontal">
        <div class="box-body">
          <!-- text input -->
          <div class="form-group">
            <label class="col-sm-3 control-label"><?= __('Date') ?></label>
            <div class="col-sm-9">
                <div class="input-group date">
                    <?= $this->Form->control('business_date', [
                              'class'       =>'form-control datepicker',
                              'id'          => 'work_date',
                              'required'    => false,
                              'placeholder' => __('YYYY-MM-DD'),
                              'type'        => 'text',
                              'label'       => false,
                            ]);
                        ?>
                    <div class="input-group-addon calendar_icon">
                      <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
          </div>

          <!-- input states -->
          <div class="bootstrap-timepicker" id="error_start_time_div">
            <div class="form-group">
              <label class="col-sm-3 control-label"><?= __('Start Work Time:') ?></label>
              <div class="col-sm-9">
                <div class="input-group">
                    <?= $this->Form->control('start_time', [
                        'class'=>'form-control timepicker',
                        'id' => 'start_work_time',
                        'required' => false,
                        'label' => false,
                        'type' => 'text'
                        ]);
                    ?>
                    <span id="error_start_time_span" class="help-block text-red"></span>
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                </div>
                <!-- /.input group -->
              </div>
            </div>
          </div>

          <!-- input states -->
          <div class="bootstrap-timepicker" id="error_end_time_div">
            <div class="form-group">
              <label class="col-sm-3 control-label"><?= __('End Work Time:') ?></label>
              <div class="col-sm-9">
                <div class="input-group">
                    <?= $this->Form->control('end_time', [
                        'class'=>'form-control timepicker',
                        'id' => 'end_work_time',
                        'required' => false,
                        'label' => false,
                        'type' => 'text'
                        ]);
                    ?>
                    <?= $this->Form->hidden('end_time_hidden', [
                        'id' => 'end_work_time_hidden',
                        'val'  => $end_time
                        ]);
                    ?>
                    <span id="error_end_time_span" class="help-block text-red"></span>
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                </div>
                <!-- /.input group -->
              </div>
            </div>
          </div>


          <!-- text input -->
          <div class="form-group">
            <label class="col-sm-3 control-label"><?= __('Total Work Time') ?>:</label>
            <div class="col-sm-3">
              <span class="control-label tms__text" id="total_work_time_span"><?= $total_work_time; ?> hrs</span>
                <?= $this->Form->hidden('total_work_time', [
                        'class'=> 'form-control',
                        'id'   => 'total_work_time',
                        'val'  => $total_work_time
                        ]);
                    ?>

                <?php
                if (!empty($daily_report)) {
                    echo $this->Form->hidden('id', [
                        'class' => 'form-control',
                        'val'   => $daily_report['id']
                    ]);
                }
                ?>
            </div>
          </div>

          <!-- tables -->
          <div class="box-body no-padding">
            <table class="table table-striped tms__table">
              <tbody>
                <tr>
                  <th><?= __('Task Name'); ?></th>
                  <th><?= __('Project'); ?></th>
                  <th><?= __('Milestone'); ?></th>
                  <th><?= __('Task Code'); ?></th>
                  <th><?= __('Act Time'); ?></th>
                  <th><?= __('Est Time/Total Act Time'); ?></th>
                  <th><?= __('Edit'); ?></th>
                  <th><?= __('Delete'); ?></th>
                </tr>
                <?php
                  foreach ($daily_work_result as $key => $value) {
                      $actual_times = (h($value['actual_times']) == null || h($value['actual_times']) == '') ? 0 : h($value['actual_times']);
                      $milestone = (h($value['task']['milestone']) == null || h($value['task']['milestone']) =='') ? '' : h($value['task']['milestone']['name']);

                      echo "<tr data-id='tms_".$value['id']."' >";
                      echo "<td>". h($value['task']['name']) ."</td>";
                      echo "<td>". h($value['task']['project']['name']) ."</td>";
                      echo "<td>". $milestone ."</td>";
                      echo "<td>". h($value['task']['task_code']) ."</td>";
                      echo "<td>". $actual_times ."</td>";
                      echo "<td>". $value['task']['estimate_times'] ."</td>";
                      echo "<td><i class='icon fa fa-pencil-square-o edit' data-id='".$value['task']['id']."' data-toggle='modal'data-estimate_times='".$value['task']['estimate_times']."' data-target='#editModal' data-dwr_id='".$value['id']."' data-proj_id='".$value['task']['project']['id']."' data-milestone_id='".$value['task']['milestone']['id']."'></i></td>";
                      echo "<td class='deleterow' data-id='".$value['id']."' data-actual_time='".$actual_times."'><i class='icon fa fa-trash-o' ></i></td>";
                      echo "</tr>";
                  }
                ?>
              </tbody>
            </table>
            <button type="button" class="btn btn-success pull-right addtask" data-toggle="modal" data-target="#modal_add_task"><?= __('Add Task') ?></button>
          </div>

          <?= $this->Form->hidden('deletetaskId', [
              'required' => false,
              'id' => 'deletetaskId'
              ]);
          ?>
          <?= $this->Form->hidden('deductActualTime', [
              'required' => false,
              'id' => 'deductActualTime'
              ]);
          ?>

          <!-- text input -->
          <div class="form-group" id="error_total_actual_times_div">
            <label class="col-sm-3 control-label text-left"><?= __('Total Actual Time') ?>:</label>
            <div class="col-sm-3">
              <span class="control-label tms__text" id="total_actual_time"><?= $total_actual_time; ?> hrs</span>
                <?= $this->Form->hidden('total_actual_times', [
                        'class'=>'form-control',
                        'id'   => 'total_actual_times',
                        'val'  => $total_actual_time
                        ]);
                    ?>
            <span id="error_total_actual_times_span" class="help-block text-red"></span>
            </div>
          </div>

          <div class="form-group tms__comment-wrapper tms__comment-margin" id="error_comment_div">
            <label><?= __('Comment') ?></label>
            <?= $this->Form->textarea('comment', [
                    'class'=>'form-control',
                    'id' => 'comment',
                    'required' => false,
                    'placeholder' => __('Type your comment ...'),
                    'label' => false,
                    'rows' => 3
                    ]);
            ?>
            <span id="error_comment_span" class="help-block text-red"></span>
          </div>

          <div class="form-group tms__comment-wrapper" id="error_plan_for_tomorrow_div">
            <label><?= __('Plan for tommorow') ?></label>
            <?= $this->Form->textarea('plan_for_tomorrow', [
                    'class'=>'form-control',
                    'id' => 'plan_for_tomorrow',
                    'required' => false,
                    'placeholder' => __('Type your plan for tommorow ...'),
                    'label' => false,
                    'rows' => 3
                    ]);
            ?>
            <span id="error_plan_for_tomorrow_span" class="help-block text-red"></span>
          </div>

        </div>

        <div class="box-footer">
          <input type="button" class="btn btn-info pull-right save-btn" value="<?= __('Save'); ?>">
        </div>

      </div>
      <?= $this->Form->end(); ?>

        <!--modal for addtask-->
        <div class="modal fade" tabindex="-1" role="dialog" id="modal_add_task">
            <div class="modal-dialog" role="document">
              <div class="modal-content tms__modal">
                <div class="modal-body">
                  <div class="form-horizontal">

                  <div class="form-group">
                    <label class="control-label col-sm-4"><?= __('Project') ?></label>
                    <div class="col-sm-8">
                        <?= $this->Form->input('project_id', [
                            'class'   => 'form-control project project_option',
                            'options' =>  $all_projects,
                            'div'     => false,
                            'label'   => false,
                            'empty'   => '---',
                            'id'      => 'project_id_add'
                            ]);
                        ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4"><?= __('Milestone') ?></label>
                    <div class="col-sm-8">
                        <?= $this->Form->input('milestone_id', [
                            'class'   => 'form-control milestone',
                            'div'     => false,
                            'label'   => false,
                            'empty'   => '---',
                            'id'      => 'milestone_id_add'
                            ]);
                        ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="addTaskCode"><?= __('Task Code') ?></label>
                    <div class="col-sm-8 task_id_div" id="task_code_div">
                        <?= $this->Form->control('task_code', [
                            'class'=>'form-control taskCode',
                            'id' => 'addTaskCode',
                            'required' => false,
                            'label' => false,
                            'disabled' => true
                            ]);
                        ?>
                        <span id="error_task_code" class="help-block text-red error_task_id"></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="addActualTime"><?= __('Actual Time') ?></label>
                    <div class="col-sm-8" id="actual_times_div">
                        <?= $this->Form->control('actual_times', [
                            'class' =>'form-control acttime',
                            'id'    => 'addActualTime',
                            'required'    => false,
                            'label' => false,
                            'type'  => 'number',
                            'min'   => 0,
                            'max'   => 20
                            ]);
                        ?>
                        <span id="error_actual_times" class="help-block text-red"></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="addEstimateTime"><?= __('Estimate Time') ?></label>
                    <div class="col-sm-8" id="estimate_times_div">
                        <?= $this->Form->control('estimate_times', [
                            'class'    =>'form-control esttime',
                            'id'       => 'addEstimateTime',
                            'required' => false,
                            'label'    => false,
                            'type'     => 'number',
                            'min'      => 0,
                            'disabled' => true
                            ]);
                        ?>
                        <span id="error_estimate_times" class="help-block text-red"></span>
                    </div>
                  </div>

                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?= __('Cancel') ?></button>
                    <button type="button" class="btn btn-primary pull-right addnew" disabled="disabled"><?= __('Save') ?></button>
                  </div>
                </div>
              </div>
            </div>
        </div>

        <!--modal for editandupdate-->
        <div class="modal fade" tabindex="-1" role="dialog" id="editModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content tms__modal">
              <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                      <label class="control-label col-sm-4"><?= __('Project') ?></label>
                      <div class="col-sm-8">
                            <?= $this->Form->input('project_id', [
                                'class'   => 'form-control editProject project_option',
                                'options' => $all_projects,
                                'div'     => false,
                                'label'   => false,
                                'empty'   => '---',
                                'id'      => 'project_id'
                                ]);
                            ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-4"><?= __('Milestone') ?></label>
                      <div class="col-sm-8">
                            <?= $this->Form->input('milestone_id', [
                                'class'   => 'form-control editMilestone',
                                'div'     => false,
                                'label'   => false,
                                'empty'   => '---',
                                'id'      => 'milestone_id'
                                ]);
                            ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-4" for="taskCode"><?= __('Task Code') ?></label>
                      <div class="col-sm-8 task_id_div_edit" id="task_code_div_edit">
                        <?= $this->Form->control('task_code', [
                            'class'=>'form-control editTaskCode taskCode',
                            'id' => 'taskCode',
                            'required' => false,
                            'label' => false,
                            'disabled' => true
                            ]);
                        ?>
                        <?= $this->Form->hidden('original_task_code', [
                            'class'=>'form-control original_task_code',
                            'id' => 'original_task_code',
                            'required' => false,
                            ]);
                        ?>
                        <span id="error_task_code_edit" class="help-block text-red error_task_id_edit"></span>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-4" for="actualTime"><?= __('Actual Time') ?></label>
                      <div class="col-sm-8" id="actual_times_div_edit">
                        <?= $this->Form->control('actual_times', [
                            'class'    => 'form-control editActtime',
                            'id'       => 'actualTime',
                            'required' => false,
                            'label'    => false,
                            'type'     => 'number',
                            'min'      => 0,
                            'max'      => 20
                            ]);
                        ?>
                        <?= $this->Form->hidden('old_actual_times', [
                            'class'    => 'form-control oldActtime',
                            'id'       => 'oldActtime',
                            'required' => false,
                            ]);
                        ?>
                        <span id="error_actual_times_edit" class="help-block text-red"></span>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-4" for="estimateTime"><?= __('Estimate Time') ?></label>
                      <div class="col-sm-8" id="estimate_times_div_edit">
                        <?= $this->Form->control('estimate_times', [
                            'class'    => 'form-control editEstimate',
                            'id'       => 'estimateTime',
                            'required' => false,
                            'label'    => false,
                            'type'     => 'number',
                            'min'      => 0,
                            'disabled' => true
                            ]);
                        ?>
                        <span id="error_estimate_times_edit" class="help-block text-red"></span>
                      </div>
                    </div>
                    <?= $this->Form->hidden('id', [
                        'id'    => 'edittaskId'
                        ]);
                    ?>

                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?= __('Cancel') ?></button>
                    <button type="button" class="btn btn-primary pull-right update"><?= __('Update') ?></button>
                </div>
              </div>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

    </div>
  </div>
</div>
<input type="hidden" value="<?= h(isset($date_selected) ? $date_selected : date('Y-m-d')); ?>" id="defaultWorkDate">
<script>
  var strDate = '';

  $(document).ready(function(){
    // Hide error message span
    $('.help-block').css('display', 'none');

    // Change PM if time is in 12:00
    var get_end_time      = $('#end_work_time_hidden').val();
    var get_hour_only     = get_end_time.substring(0, 2);
    var remove_seconds    = get_end_time.slice(0, -3);
    var default_work_date = $('#defaultWorkDate').val();

    if (get_hour_only == 12) {
        $('#end_work_time').val(remove_seconds + ' PM');
    }

    // Set default daily report date
    $("#work_date").val(default_work_date);

    // Set value of Total Work Time
    computeTotalWorkTime();
  });

  // Daily report date
  $('.datepicker').datepicker({
    dateFormat : 'yy-mm-dd',
    weekStart  : 0
  });

  $("#work_date").on('changeDate change paste', function() {
    var new_date  = $(this).val();
    window.location = '/users/productivity/' + new_date;
  });

  // Compute Total Work Time
  $("#start_work_time, #end_work_time").on('change paste', function() {
    computeTotalWorkTime();
  });

  // Compute Total Work Time
  function computeTotalWorkTime(){
    var business_date   = $("#work_date").val();
    var getStartTime    = $('#start_work_time').val();
    var getEndTime      = $('#end_work_time').val();
    var total_work_time = differenceOfTime(business_date, getStartTime, getEndTime);
    total_work_time = (total_work_time < 0) ? 0 : total_work_time;

    $('#total_work_time').val(total_work_time);
    $('#total_work_time_span').text(total_work_time + ' hrs');
  }

  // Total_Work_Time = (Start_Work_Time - End_Work_Time)
  function differenceOfTime(business_date, startTime, endTime) {
    var timeStart = new Date(business_date + " " + startTime);
    var timeEnd   = new Date(business_date + " " + endTime);
    var diff      = (timeEnd - timeStart) / 60000; //dividing by seconds and milliseconds
    var minutes   = diff % 60;
    var hours     = "";
    if (timeEnd > new Date(business_date + " 12:00 PM")) {
        hours = ((diff - minutes) / 60) - 1; // Subtract by 1hr for lunch break
    } else {
        hours = ((diff - minutes) / 60);
    }
    var total_wt  = 0; //total_work_time

    if (hours > 0 || minutes > 0) {
      total_wt = timeToDecimal(hours + ':' + minutes);
      $('#error_end_time_span').css('display', 'none');
      $('#error_end_time_div').removeClass('has-error');
    }

    return total_wt;
  }

  // Convert 'HH:MM' to decimal
  function timeToDecimal(t) {
    t = t.split(':');
    var time = parseFloat(parseInt(t[0], 10) + parseInt(t[1], 10) / 60);
    return time.toFixed(2);
  }
  var project_common = [<?php echo implode(',',$project_common); ?>];

  // Add/Edit Modal: Search all matching milestones on selected project
  $(".project_option").on("change", function() {
    let id      = $(this).attr("id");
    let data    = $('#' + id).val();
    let url     = '/users/search_matching_project';
    let mile_id = (id === 'project_id_add') ? 'milestone_id_add' : 'milestone_id';
    if (data === '') {
      $('#' + mile_id).empty().prepend("<option value='' selected='selected'>---</option>");
    } else {
      searchMatchingProject(url, data, mile_id);
    }

    $('#addEstimateTime').prop('disabled',true);
    if (jQuery.inArray( parseInt(data), project_common ) === 0 ) {
        $('#addEstimateTime').prop('disabled',false);
    }
  });

  // Add: Enable/disable taskcode and 'Save' button if project and/or milestone is null
  $('#project_id_add, #milestone_id_add').change(function() {
    if ( $('#project_id_add').val() === ""
          || ($('#project_id_add').val() === "" && $('#milestone_id_add').val() != "") ){
        $('#addTaskCode').val('').prop('disabled', true);
        $('.addnew').prop('disabled', true);
        $('#addActualTime, #addEstimateTime').val('');

        // reset validation
        $('#error_task_code, #error_actual_times, #error_estimate_times').text('').css('display', 'none');
        $('#task_code_div, #actual_times_div, #estimate_times_div').removeClass('has-error');

    } else {
        $('#addEstimateTime').val('');
        $('#addTaskCode').val('').prop('disabled', false);
        $('.addnew').prop('disabled', false);

        // reset validation
        $('#error_task_code, #error_actual_times, #error_estimate_times').text('').css('display', 'none');
        $('#task_code_div, #actual_times_div, #estimate_times_div').removeClass('has-error');
    }
  });

  // EDIT: Enable/disable taskcode and 'Update' button if project and/or milestone is null
  $('.editProject, .editMilestone').change(function() {
    if ( $('.editProject').val() === ""
          || ($('.editProject').val() === "" && $('.editMilestone').val() != "") ){
        $('.editTaskCode').val('').prop('disabled', true);
        $('.update').prop('disabled', true);
        $('#actualTime, #estimateTime').val('');

        // reset validation
        $('#error_task_code_edit, #error_actual_times_edit, #error_estimate_times_edit').text('').css('display', 'none');
        $('#task_code_div_edit, #actual_times_div_edit, #estimate_times_div_edit').removeClass('has-error');

    } else {
        $('#estimateTime').val('');
        $('.editTaskCode').val('').prop('disabled', false);
        $('.update').prop('disabled', false);

        // reset validation
        $('#error_task_code_edit, #error_actual_times_edit, #error_estimate_times_edit').text('').css('display', 'none');
        $('#task_code_div_edit, #actual_times_div_edit, #estimate_times_div_edit').removeClass('has-error');
    }
  });

  // Add/Edit Modal: Suggest Task Code
  $('.taskCode').on('focus', function() {
    var task_id      = $(this).prop('id');
    var milestone_id = ( task_id === 'addTaskCode') ? $(".milestone").val() : $(".editMilestone").val();
    var project_id   = $('.project_option').val();
    var work_date    = $('#work_date').val();

    $(this).autocomplete({
        source: function( request, response ) {
          var url  = '/users/suggest_task_code';
          var data = { text : request.term, milestone_id : milestone_id, project_id : project_id, work_date : work_date };

          suggestFields(url, data, response);
        },
        select: function( event, ui ) {
          event.preventDefault();
          var task_code = ui.item;

          $('#'+task_id).val(task_code.value);

          var url2       = '/users/search_matching_task';
          var estimate  = '';
          var proj_id   = 0;
          var mile_id   = 0;

          if (task_id == 'addTaskCode') {
              proj_id  = $('#project_id_add').val();
              mile_id  = $('#milestone_id_add').val();
              estimate = 'addEstimateTime';
          } else {
              proj_id  = $('#project_id').val();
              mile_id  = $('#milestone_id').val();
              estimate = 'estimateTime';
          }

          var data2 = {
              proj_id   : proj_id,
              mile_id   : mile_id,
              task_code : task_code.value
            };

          setEstimateTime(url2, data2, estimate);
        }
    });
  });

  // Add/Edit Modal: display estimate time
  $(".taskCode").on('change paste', function() {
    var url       = '/users/search_matching_task';
    var task_code = $(this).val();
    var estimate  = '';
    var proj_id   = 0;
    var mile_id   = 0;

    if ($(this).prop('id') == 'addTaskCode') {
        proj_id  = $('#project_id_add').val();
        mile_id  = $('#milestone_id_add').val();
        estimate = 'addEstimateTime';
    } else {
        proj_id  = $('#project_id').val();
        mile_id  = $('#milestone_id').val();
        estimate = 'estimateTime';
    }

    var data = {
        proj_id   : proj_id,
        mile_id   : mile_id,
        task_code : task_code
      };

    setEstimateTime(url, data, estimate);
  });

  // Accept only decimal and non-negative inputs
  $('#addActualTime, #addEstimateTime, #actualTime, #estimateTime').keypress(function(event) {
    if ( (event.which != 46 || $(this).val().indexOf('.') != -1)
          && (event.which < 48 || event.which > 57) ) {
        event.preventDefault();
    }
    if ( ($(this).val().indexOf('.') != -1)
          && ($(this).val().substring($(this).val().indexOf('.'), $(this).val().indexOf('.').length).length > 2)) {
        event.preventDefault();
    }
  });

  // Timepicker
  $('#start_work_time').timepicker({
    defaultTime : '9:00 AM'
  });
  $('#end_work_time').timepicker({
    defaultValue : 'value'
  });

  var $delete;
  $(document).delegate(".deleterow","click", function(){
    $('#confirmation_modal').modal('show');
    $('.modal-message').text("<?= 'Are you sure you want to delete this task?'; ?>");
    $(".submitForm").removeClass('submit_daily_report');
    $('.submitForm').removeClass('submit_add_task');
    $('.submitForm').removeClass('submit_edit_task');
    $(".submitForm").addClass('submit_delete_task');

    $('#deletetaskId').val($(this).data('id'));
    $('#deductActualTime').val($(this).data('actual_time'));

    $delete = $(this).parent('tr');
  });

  $(document).delegate(".submit_delete_task","click", function(){
    $('#confirmation_modal').modal('hide');

    var deleted_id         = $("#deletetaskId").val();
    var actual_time        = $("#deductActualTime").val();
    var total_actual_time  = $("#total_actual_times").val();
    var final_total_actual = parseFloat(total_actual_time) - parseFloat(actual_time);

    var ajax_result = ajaxHandler('/users/productivity_delete',{deleted_id : deleted_id});

    ajax_result.then(function(response) {
      $(".submitForm").removeClass('submit_delete_task');

      $("#deletetaskId").val();
      $("#deductActualTime").val();

      if (response.status == true) {
         $("#successMsg").html(response.message);
         $("#successModal").modal('show');
         $delete.fadeOut(500, function(){
          $(this).remove();
          $('#total_actual_time').text(final_total_actual.toFixed(2) + ' hrs');
          $('#total_actual_times').val(final_total_actual.toFixed(2));
         });
      } else {
        $("#errorMsg").html(response.message);
        $("#errorModal").modal('show');
      }
    }).catch(function(fromReject) {
      $(".submitForm").removeClass('submit_delete_task');
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Clear/reset validation in Add Task modal
  $('.addtask').on("click", function(){
    $('#modal_add_task select, #modal_add_task input').val('');
    $('#addEstimateTime').prop('disabled',true);
    $('#addTaskCode').val('').prop('disabled', true);
    $('.addnew').prop('disabled', true);
    $('.submitForm').removeClass('submit_edit_task');
    $('.submitForm').removeClass('submit_delete_task');
    $('.submitForm').removeClass('submit_daily_report');

    var keys = ['task_id', 'task_code', 'actual_times', 'estimate_times'];
    keys.forEach(value => {
      $('#error_' + value).text('').css('display', 'none');
      $('#' + value + '_div').removeClass('has-error');
    });
    $('#milestone_id_add').empty().prepend("<option value='' selected='selected'>---</option>");
  });

  // Clear/reset validation in Edit Task modal
  $('.edit').on("click", function(){
    $('.submitForm').removeClass('submit_add_task');
    $('.submitForm').removeClass('submit_delete_task');
    $('.submitForm').removeClass('submit_daily_report');

    var keys = ['task_code', 'actual_times', 'estimate_times'];
    keys.forEach(value => {
      $('#error_' + value + '_edit').text('').css('display', 'none');
      $('#' + value + '_div_edit').removeClass('has-error');
    });
  });

  // Validate new daily_work_results
  var len= $('table tr').length;
  var table_row = '';
  var a = len;
  $("html").delegate(".addnew", "click", function(){
    var ajax_result = ajaxHandler('/users/validate_productivity_add',{
          work_date      : $('#work_date').val(),
          task_code      : $('#addTaskCode').val(),
          actual_times   : parseFloat($('#addActualTime').val()),
          estimate_times : $('#addEstimateTime').val(),
          milestone_id   : $('#milestone_id_add').val(),
          project_id     : $('#project_id_add').val()
        });

    ajax_result.then(function(response) {
        // Reset error message div and span
        $('.help-block').css('display', 'none');
        var keys = ['task_code', 'actual_times', 'estimate_times'];
        keys.forEach(value => {
          $('#error_' + value).text('').css('display', 'none');
          $('#' + value + '_div').removeClass('has-error');
          $('.error_' + value).text('').css('display', 'none');
          $('.' + value + '_div').removeClass('has-error');
        });

        if (response.status === true) {
          $('#modal_add_task').modal('hide');
          $('#confirmation_modal').modal('show');
          $('.modal-message').text("<?= __('Are you sure you want to add this task?'); ?>");
          $('.submitForm').addClass('submit_add_task');

        } else {
          jQuery.each(response.message, function(index, item) {
              $('#error_' + index).text(item).css('display', 'inline-block');
              $('#' + index + '_div').addClass('has-error');
              $('.error_' + index).text(item).css('display', 'inline-block');
              $('.' + index + '_div').addClass('has-error');
          });
        }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Saving new daily_work_results
  $(document).delegate(".submit_add_task", "click", function(){
    $('#confirmation_modal').modal('hide');
    $('.submitForm').removeClass('submit_add_task');

    var task_code      = $('#addTaskCode').val();
    var acttime        = parseFloat($('#addActualTime').val());
    var estimate_times = ($('#addEstimateTime').val() == '') ? 0 : $('#addEstimateTime').val();
    var total_acttime  = $("#total_actual_times").val();
    var final_tot_act  = parseFloat(total_acttime) + acttime;

    var ajax_result = ajaxHandler('/users/productivity_add',{
          work_date      : $('#work_date').val(),
          task_code      : task_code,
          actual_times   : acttime,
          estimate_times : estimate_times,
          milestone_id   : $('#milestone_id_add').val(),
          project_id     : $('#project_id_add').val()
        });

    ajax_result.then(function(response) {
        if (response.status === true) {
          $("#successModal").modal('show');
          $("#successMsg").html(response.message);
          if (response.mile_id == '' || response.mile_id == null) {
             response.mile_name = ''
          }
          $('table').append("<tr data-id=tms_"+ a +"><td>" + response.task_name + "</td><td>" + response.proj_name +"</td><td>"+ response.mile_name +"</td><td>"+ task_code +"</td><td>"+ acttime +"</td><td>"+ estimate_times +"</td><td><i class='icon fa fa-pencil-square-o edit' data-toggle='modal' data-target='#editModal' data-id='"+ response.task_id +"' data-estimate_times='"+ estimate_times +"' data-dwr_id='"+ response.dwr_id +"' data-proj_id='"+ response.proj_id +"' data-milestone_id='"+ response.mile_id +"' ></i></td><td class='deleterow' data-id='"+ response.dwr_id +"'  data-actual_time='" + response.act_time + "'><i class='icon fa fa-trash-o'></i></td></tr>");
          a++;

          $('#total_actual_time').text(final_tot_act.toFixed(2) + ' hrs');
          $('#total_actual_times').val(final_tot_act.toFixed(2));

        } else {
          $("#errorModal").modal('show');
          $("#errorMsg").html(response.message);
        }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Display edit daily_work_results modal
  $("html").delegate(".edit", "click", function(){
    table_row         = $(this).parent().parent().data('id');
    var dwr_id        = $(this).data('dwr_id'); // daily_work_result
    var proj_id       = $(this).data('proj_id');
    var milestone_id  = $(this).data('milestone_id');
    var TaskCode      = $(this).parent().parent().find('td:nth-child(4)').text();
    var Actualtime    = $(this).parent().parent().find('td:nth-child(5)').text();
    var EstimateTime  = $(this).parent().parent().find('td:nth-child(6)').text();

    $('#editModal').show('modal');
    $('#editModal select, #editModal input').val('');
    $(".editProject option[value='"+proj_id+"']").prop('selected', 'selected').change();
    $(".editMilestone option[value='"+milestone_id+"']").prop('selected', 'selected').change();
    $('.editTaskCode').val(TaskCode);
    $('.original_task_code').val(TaskCode);
    $('.editActtime').val(Actualtime);
    $('.oldActtime').val(Actualtime);
    $('.editEstimate').val(EstimateTime);
    $('#edittaskId').val(dwr_id);
    $('#estimateTime').prop('disabled',true);
    if (jQuery.inArray( parseInt(proj_id), project_common ) === 0 ) {
        $('#estimateTime').prop('disabled',false);
        $('#taskCode').prop('disabled', true);
    }
  });

  // Validate daily_work_results before updating
  $("html").delegate(".update","click", function(){
    var ajax_result = ajaxHandler('/users/validate_productivity_edit',{
                  work_date          : $('#work_date').val(),
                  id                 : $("#edittaskId").val(),
                  milestone_id       : $(".editMilestone").val(),
                  actual_times       : parseFloat($('.editActtime').val()),
                  task_code          : $('.editTaskCode').val(),
                  project_id         : $(".editProject").val(),
                  estimate_times     : $(".editEstimate").val(),
                  original_task_code : $('.original_task_code').val()
               });

    ajax_result.then(function(response) {
        var keys = ['task_id', 'task_code', 'actual_times', 'estimate_times'];
        keys.forEach(value => {
          $('#error_' + value + '_edit').text('').css('display', 'none');
          $('#' + value + '_div_edit').removeClass('has-error');
          $('.error_' + value + '_edit').text('').css('display', 'none');
          $('.' + value + '_div_edit').removeClass('has-error');
        });
        if (response.status == true) {
          $('#editModal').modal('hide');
          $('#confirmation_modal').modal('show');
          $('.modal-message').text("<?= __('Are you sure you want to update this task?'); ?>");
          $('.submitForm').addClass('submit_edit_task');

        } else {
          jQuery.each(response.message, function(index, item) {
            $('#error_' + index + '_edit').text(item).css('display', 'inline-block');
            $('#' + index + '_div_edit').addClass('has-error');
            $('.error_' + index + '_edit').text(item).css('display', 'inline-block');
            $('.' + index + '_div_edit').addClass('has-error');
          });
        }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Saving updated daily_work_results
  $(document).delegate(".submit_edit_task", "click", function(){
    var taskId               = $("#edittaskId").val();
    var editEstimateVal      = $(".editEstimate").val();
    var editProjectVal       = $(".editProject").val();
    var editMilestoneVal     = $(".editMilestone").val();
    var editProject          = $(".editProject option[value='"+ editProjectVal +"']").text();
    var editMilestone        = $(".editMilestone option[value='"+ editMilestoneVal +"']").text();
    var editTaskCode         = $('.editTaskCode').val();
    var originalTaskCode     = $('.original_task_code').val();
    var editOldActualtime    = parseFloat($('.oldActtime').val());
    var editNewActualtime    = parseFloat($('.editActtime').val());
    var editOldTotalActTime  = parseFloat($('#total_actual_times').val());
    var editNewTotalActTime  = editOldTotalActTime;

    if (editOldActualtime !== editNewActualtime) {
      var editNewTotalActTime  = editOldTotalActTime - ( editOldActualtime - editNewActualtime );
      $('.oldActtime').val(editNewActualtime);
    }

    var ajax_result = ajaxHandler('/users/productivity_edit',{
                  work_date          : $('#work_date').val(),
                  id                 : taskId,
                  milestone_id       : editMilestoneVal,
                  actual_times       : editNewActualtime,
                  task_code          : editTaskCode,
                  project_id         : editProjectVal,
                  estimate_times     : editEstimateVal,
                  original_task_code : originalTaskCode
               });

    ajax_result.then(function(response) {
        if (response.status == true) {
          $("#confirmation_modal").modal('hide');
          $("#successModal").modal('show');
          $("#successMsg").html(response.message);
          $('.submitForm').removeClass('submit_edit_task');

         $('table tr[data-id="'+ table_row +'"]').html("<td>"+ response.task_name +"</td><td>"+ response.proj_name +"</td><td>"+ response.mile_name +"</td><td>"+ response.task_code +"</td><td>"+ response.act_times +"</td><td>"+ response.est_times +"</td><td><i class='icon fa fa-pencil-square-o edit' data-toggle='modal' data-target='#editModal' id='edit' data-id='"+ response.task_id +"' data-estimate_times='"+ response.est_times +"' data-dwr_id='"+ response.dwr_id +"' data-proj_id='"+ response.proj_id +"' data-milestone_id='"+ response.mile_id.milestone_id +"' ></i></td><td class='deleterow' data-id='"+ response.dwr_id +"' data-actual_time='"+ response.act_times +"'><i class='icon fa fa-trash-o'></i></td>");

          $('#total_actual_time').text(editNewTotalActTime.toFixed(2) + ' hrs');
          $('#total_actual_times').val(editNewTotalActTime.toFixed(2));
        } else {
          $("#errorModal").modal('show');
          $("#errorMsg").html(response.message);
        }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Validate Daily Report
  $(document).delegate(".save-btn", "click", function(){
    $("[id$=_span]").text('').css('display', 'none');
    $("[id$=_div]").removeClass('has-error');

    var ajax_result = ajaxHandler('/users/validate_add_daily_work',$('#formDailyWork').serialize());

    ajax_result.then(function(response) {
      if (response.status == false) {
        jQuery.each(response.message, function(k,v){
          $('#error_'+k+'_span').text(v).css('display', 'inline-block');
          $('#error_'+k+'_div').addClass('has-error');
        });

        var total_work_time = $('#total_work_time').val();
        $('#total_work_time_span').text(total_work_time + ' hrs').css('display', 'inline-block');
      } else {
        $('#confirmation_modal').modal('show');
        $('.modal-message').text("<?= __('Are you sure you want to save your work?'); ?>");
        $('.submitForm').addClass('submit_daily_report');
        $('.submitForm').removeClass('submit_edit_task');
        $('.submitForm').removeClass('submit_delete_task');
        $('.submitForm').removeClass('submit_add_task');
      }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });

    return false;
  });

  // Saving daily_report
  $(document).delegate(".submit_daily_report", "click", function(){
    $('#confirmation_modal').modal('hide');
    $('.submitForm').removeClass('submit_daily_report');

    var ajax_result = ajaxHandler('/users/add_daily_work',$('#formDailyWork').serialize());

    ajax_result.then(function(response) {
        if (response.status === true) {
          $("#successModal").modal({
            backdrop: 'static',
            keyboard: false
          });
          $("#successMsg").html(response.message);
          $("#successModal").addClass('dailyReport');
        } else {
          $("#errorModal").modal('show');
          $("#errorMsg").html(response.message);
        }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
    return false;
  });
  $(document).delegate('#successBtn, .close','click', function() {
    if ($('.dailyReport').length != 0) {
      location.reload();
    }
  });
</script>
