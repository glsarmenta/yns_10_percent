<div class="container">
  <div class="col-md-6 col-md-offset-3 tms__wrapper-margin">
    <div class="box box-danger">
      <div class="box-header">
        <h3 class="box-title"><?= __('Email Confirmation') ?></h3>
      </div>
        <?= $this->Form->create($forgotPasswordForm, ['class' => 'form-horizontal', 'id' => 'forgot_passwordForm']); ?>
            <div class="box-body">
              <div class="form-group">
                <label for="email" class="col-sm-2 control-label"><?= __('Email') ?></label>
                <div class="col-sm-10">
                    <?= $this->Form->control('email', [
                            'type'        => 'text',
                            'id'          => 'email',
                            'class'       => 'form-control',
                            'required'    => false,
                            'placeholder' => __('Email'),
                            'label'       => false
                        ]);
                    ?>
                    <?= $this->Flash->render() ?>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <a role="button" class="btn btn-default pull-left" id="return" href="/users/login"><?php echo __d('admin', 'Back'); ?></a>
              <?= $this->Form->submit(__('Submit'), ['class'=>'btn btn-info pull-right']); ?>
            </div>
        <?= $this->Form->end(); ?>
    </div>
  </div>
</div>