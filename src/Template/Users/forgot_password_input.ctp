<div class="container">
  <div class="col-md-6 col-md-offset-3 tms__wrapper-margin">
    <div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title"><?= __('Forgot Password') ?></h3>
        </div>

        <?= $this->Form->create($editForm, ['class'=>'form-horizontal','id' => 'editForm']); ?>
            <div class="box-body">
              <div class="form-group">
                <label for="new_password" class="col-sm-4 control-label"><?= __('New Password') ?></label>
                <div class="col-sm-8" id="password_div">
                    <?= $this->Form->control('password', [
                        'type'        => 'password',
                        'class'       =>'form-control',
                        'required'    => false,
                        'placeholder' => __('Password'),
                        'label'       => false,
                        'id'          => 'new_password'
                        ]);
                    ?>
                    <span id="error_password" class="help-block text-red"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="confirm_password" class="col-sm-4 control-label"><?= __('Confirm Password') ?></label>
                <div class="col-sm-8" id="confirm_password_div">
                    <?= $this->Form->control('confirm_password', [
                        'type'        => 'password',
                        'class'       =>'form-control',
                        'required'    => false,
                        'id'          => 'inputPassword',
                        'placeholder' => __('Confirm Password'),
                        'label'       => false,
                        'id'          => 'confirm_password'
                        ]); ?>
                    <?= $this->Form->control('password_updated', [
                        'type' => 'hidden',
                        'value'=> 1
                        ]); ?>
                    <span id="error_confirm_password" class="help-block text-red"></span>
                </div>
              </div>
            </div>

            <div class="box-footer">
              <?= $this->Flash->render() ?>
              <input type="button" class="btn btn-default pull-left" id='clearBtn' name="clear" value="<?= __('Clear'); ?>">
              <input type="button" class="btn btn-info pull-right" id='submitBtn' name="submit" value="<?= __('Submit'); ?>">
            </div>

        <?= $this->Form->end(); ?>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    reset_error();
  });

  // Reset form
  $('#clearBtn').click(function() {
    $('#editForm').trigger("reset");
    reset_error();
  });

  // For hiding error messages
  function reset_error() {
    $('#error_password, #error_confirm_password').css('display', 'none');
    $('#password_div, #confirm_password_div').removeClass('has-error');
  }

  // Validating inputs
  $('#submitBtn').click(function() {
    var password  = $('#new_password').val();
    var confirm_password = $('#confirm_password').val();

    var ajax_result = ajaxHandler('/users/forgot_password_input', { password : password, confirm_password : confirm_password });
    
    ajax_result.then(function(response) {
      reset_error();

      if (response.status === true) {
        $('#confirmation_modal').modal("show");
        $('.modal-message').text("<?= __('Are you sure you want to change your password?'); ?>");

        $('.submitForm').attr('data-id', response.user_id);
        $('.submitForm').attr('data-password', response.password);
      } else {
        jQuery.each(response.message, function(index) {
          jQuery.each(response.message[index], function(key, value) {
            $('#error_' + index).text(value).css('display', 'inline-block');
            $('#' + index + '_div').addClass('has-error');
          }); 
        }); 
      }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Updating password
  $(document).delegate(".submitForm","click", function(){
    var id = $('.submitForm').data('id');
    var password = $('.submitForm').data('password');
    var password_updated = 1; 

    var ajax_result = ajaxHandler('forgot_password_change_password', { 
      id       : id,
      password : password,
      password_updated: password_updated
    });

    ajax_result.then(function(response) {
      if (response.status === true) {
        $('#confirmation_modal').modal("hide");
        window.location.href = 'productivity';
      }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });

  });
</script>