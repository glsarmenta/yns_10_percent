<div class="container">
  <div class="col-md-6 col-md-offset-3 tms__wrapper-margin">
    <div class="box box-danger">
      <div class="box-header">
        <h3 class="box-title"><?= __('Code verification') ?></h3>
      </div>
        <?= $this->Form->create($forgot_password_codeForm, ['class' => 'form-horizontal', 'id' => 'forgot_password_codeForm']); ?>
            <div class="box-body">
              <div class="form-group">
                <label for="code" class="col-sm-3 control-label"><?= __('Enter code') ?></label>
                <div class="col-sm-9">
                    <?= $this->Form->control('code', [
                            'type'        => 'text',
                            'id'          => 'code',
                            'class'       => 'form-control',
                            'required'    => false,
                            'label'       => false
                        ]);
                    ?>

                <?= $this->Flash->render() ?>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <?= $this->Form->submit(__('Submit'), ['class'=>'btn btn-info pull-right']); ?>
            </div>
        <?= $this->Form->end(); ?>
    </div>
  </div>
</div>