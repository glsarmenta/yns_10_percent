<div class="container">
  <div class="col-md-6 col-md-offset-3 tms__wrapper-margin">
  <!-- Horizontal Form -->
    <div class="box box-danger">
      <div class="box-header">
        <h3 class="box-title"><?= __('Edit Password') ?></h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
        <?= $this->Form->create($editForm, ['class'=>'form-horizontal','id' => 'editForm']); ?>
            <div class="box-body">
              <div class="form-group">
                <label for="employeeID" class="col-sm-2 control-label"><?= __('Password') ?></label>
                <div class="col-sm-10">
                    <?= $this->Form->control('password', [
                        'type' => 'password',
                        'class'=>'form-control',
                        'required' => false,
                        'placeholder' => __('Password'),
                        'label' => false
                        ]);
                    ?>
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-sm-2 control-label"><?= __('Confirm Password') ?></label>
                <div class="col-sm-10">
                    <?= $this->Form->control('confirm_password', [
                        'type' => 'password',
                        'class'=>'form-control',
                        'required' => false,
                        'id' => 'inputPassword',
                        'placeholder' => __('Confirm Password'),
                        'label' => false
                        ]); ?>
                    <?= $this->Form->control('password_updated', [
                        'type' => 'hidden',
                        'value'=>1
                        ]); ?>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <?= $this->Flash->render() ?>
              <input type="button" class="btn btn-info" id='clearBtn' name="clear" value="clear">
              <?= $this->Form->submit(__('Submit'), ['class'=>'btn btn-info pull-right']); ?>
            </div>
            <!-- /.box-footer -->
        <?= $this->Form->end(); ?>
    </div>
    <!-- /.box -->
  </div>
</div>
<script type="text/javascript">
  $('#clearBtn').click(function() {
      $('#editForm').trigger("reset");
  })
</script>