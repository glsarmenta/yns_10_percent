<div class="container">
  <div class="col-md-6 col-md-offset-3 tms__wrapper-margin">
  <!-- Horizontal Form -->
    <div class="box box-danger">
      <div class="box-header">
        <h3 class="box-title"><?= __('Login') ?></h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
        <?= $this->Form->create($loginForm, ['class'=>'form-horizontal']); ?>
            <div class="box-body">
              <div class="form-group">
                <label for="employeeID" class="col-sm-2 control-label"><?= __('Username') ?></label>
                <div class="col-sm-10">
                    <?= $this->Form->control('name', [
                        'class'=>'form-control',
                        'id' => 'inputemployeeID',
                        'required' => false,
                        'placeholder' => __('Username'),
                        'label' => false
                        ]);
                    ?>
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-sm-2 control-label"><?= __('Password') ?></label>
                <div class="col-sm-10">
                    <?= $this->Form->control('password', [
                        'type' => 'password',
                        'class'=>'form-control',
                        'required' => false,
                        'id' => 'inputPassword',
                        'placeholder' => __('Password'),
                        'label' => false
                        ]); ?>
                </div>
              </div>
              <a href="/users/forgot_password"><?= __('Forgot Password?'); ?></a>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
            <?= $this->Flash->render() ?>
              <?= $this->Form->submit(__('Login'), ['class'=>'btn btn-info pull-right']); ?>
            </div>
            <!-- /.box-footer -->
        <?= $this->Form->end(); ?>
    </div>
    <!-- /.box -->
  </div>
</div>