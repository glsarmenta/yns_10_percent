<div class="content-wrapper">
  <section class="content">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo __('Edit Tasks'); ?></h3>
              <a role="button" class="btn btn-default pull-right" id="return" href="/admin/tasks"><?php echo __d('admin', '< Back to List'); ?></a>
              <br/>
            </div>

            <div class="box-body">
              <h3 class="profile-username text-center"><?= h($tasks['task_code']); ?></h3>
              <p class="text-muted text-center"><?= h($tasks['name']); ?></p>

              <br/>
              <form>
                <div class="container col-md-8 col-md-offset-2">               
                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Project') ?></label>
                        <div class="col-sm-9" id="project_id_div">
                            <?= $this->Form->input('project_id', [
                                  'class'   => 'form-control project',
                                  'div'     => false,
                                  'label'   => false,
                                  'empty'   => '---',
                                  'id'      => 'project_id',
                                  'options' => h($projects),
                                  'value'   => $tasks['project_id']
                                ]);
                            ?>
                          <span id="error_project_id" class="help-block text-red"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Task Code') ?></label>
                        <div class="col-sm-9" id="task_code_div">
                            <?= $this->Form->control('task_code', [
                                  'class'       =>'form-control',
                                  'id'          => 'task_code',
                                  'required'    => false,
                                  'placeholder' => __('Task Code'),
                                  'label'       => false,
                                  'value'       => h($tasks['task_code'])
                                ]);
                            ?>
                            <?= $this->Form->hidden('old_task_code', [
                                    'required'    => false,
                                    'id'          => 'old_task_code',
                                    'value'       => h($tasks['task_code'])
                                ]); 
                            ?>
                          <span id="error_task_code" class="help-block text-red"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Task Name') ?></label>
                        <div class="col-sm-9" id="name_div">
                            <?= $this->Form->control('name', [
                                  'class'       =>'form-control',
                                  'id'          => 'name',
                                  'required'    => false,
                                  'placeholder' => __('Task Name'),
                                  'label'       => false,
                                  'value'       => h($tasks['name'])
                                ]);
                            ?>
                          <span id="error_name" class="help-block text-red"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Estimate Time') ?></label>
                        <div class="col-sm-9" id="estimate_times_div">
                            <?= $this->Form->control('estimate_times', [
                                  'class'       => 'form-control estimate_times',
                                  'id'          => 'estimate_times',
                                  'required'    => false,
                                  'label'       => false,
                                  'type'        => 'number',
                                  'min'         => 0,
                                  'placeholder' => __('Estimate Time'),
                                  'value'       => $tasks['estimate_times']
                                ]);
                            ?>
                          <span id="error_estimate_times" class="help-block text-red"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Grade') ?></label>
                        <div class="col-sm-9" id="grade_div">
                            <?= $this->Form->input('grade', [
                                  'class'   => 'form-control grade',
                                  'div'     => false,
                                  'label'   => false,
                                  'empty'   => '---',
                                  'id'      => 'grade',
                                  'value'   => $tasks['grade'],
                                  'options' => [
                                          1 => __('Dame'),
                                          2 => __('Soso'),
                                          3 => __('Normal'),
                                          4 => __('Good'),
                                          5 => __('Very Good'),
                                      ]
                                ]);
                            ?>
                          <span id="error_grade" class="help-block text-red"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Status') ?></label>
                        <div class="col-sm-9" id="is_close_div">
                            <?= $this->Form->input('is_close', [
                                  'class'   => 'form-control status',
                                  'div'     => false,
                                  'label'   => false,
                                  'empty'   => '---',
                                  'id'      => 'is_close',
                                  'value'   => $tasks['is_close'],
                                  'options' => [
                                          0 => __('Open'),
                                          1 => __('Closed'),
                                      ]
                                ]);
                            ?>
                          <span id="error_is_close" class="help-block text-red"></span>
                        </div>
                    </div>
                </div>

                <div class="container col-md-8 col-md-offset-2">
                    <br/>
                    <input type="reset" class="btn btn-default pull-left" id='resetBtn' name="reset" value="<?= __('Reset'); ?>">
                    <input type="button" class="btn btn-primary pull-right" id='confirmBtn' name="submit" value="<?= __('Submit'); ?>">
                </div>
              </form>
            </div>
        </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('.active-tasks').addClass('active');
  });

  // Validation. If status = true, display and set data to confirmation modal
  $(document).delegate("#confirmBtn", "click", function(){
    var ajax_result = ajaxHandler('/admin/tasks/validate_add_edit', {
          project_id     : $('#project_id').val(),
          task_code      : $('#task_code').val(),
          old_task_code  : $('#old_task_code').val(),
          name           : $('#name').val(),
          estimate_times : $('#estimate_times').val(),
          grade          : $('#grade').val(),
          is_close       : $('#is_close').val(),
          action         : 'edit'
        });

    ajax_result.then(function(response) {
        // Reset error message div and span
        $('.help-block').css('display', 'none');
        var keys = ['project_id', 'task_code', 'name', 'estimate_times', 'grade', 'is_close'];
        keys.forEach(value => {
          $('#error_' + value).text('').css('display', 'none');
          $('#' + value + '_div').removeClass('has-error');
        });

        if (response.status === true) {
          $('#confirmation_modal').modal('show');
          $('.modal-message').text("<?= __('Are you sure you want to change this task?'); ?>");

        } else {
          jQuery.each(response.message, function(index, item) {
            $('#error_' + index).text(item).css('display', 'inline-block');
            $('#' + index + '_div').addClass('has-error');
          });
        }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  }); 

  // Save if status = true
  $(document).delegate(".submitForm", "click", function(){
    $('#confirmation_modal').modal('hide');

    var ajax_result = ajaxHandler('/admin/tasks/edit/<?= $tasks['id']; ?>', {
          project_id     : $('#project_id').val(),
          task_code      : $('#task_code').val(),
          old_task_code  : $('#old_task_code').val(),
          name           : $('#name').val(),
          estimate_times : $('#estimate_times').val(),
          grade          : ($('#grade').val() == '') ? 0 : $('#grade').val(),
          is_close       : $('#is_close').val()
        });

    ajax_result.then(function(response) {
        if (response.status === true) {
          $('#old_task_code').val($('#task_code').val());
          $("#successModal").modal('show');
          $("#successMsg").html(response.message);

        } else {
          $("#errorModal").modal('show');
          $("#errorMsg").html(response.message);
        }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Accept only decimal and non-negative inputs
  $('#estimate_times').keypress(function(event) {
    if ( (event.which != 46 || $(this).val().indexOf('.') != -1) 
          && (event.which < 48 || event.which > 57) ) {
        event.preventDefault();
    }
    if ( ($(this).val().indexOf('.') != -1) 
          && ($(this).val().substring($(this).val().indexOf('.'), $(this).val().indexOf('.').length).length > 2)) {
        event.preventDefault();
    }
  });

  // Reset form
  $("html").delegate("#resetBtn", "click", function(){
    $('#error_project_id, #error_task_code, #error_name, #error_estimate_times, #error_grade, #error_is_close').text('').css('display', 'none');
    $('#project_id_div, #task_code_div, #name_div, #estimate_times_div, #grade_div, #is_close_div').removeClass('has-error');
  });
</script>