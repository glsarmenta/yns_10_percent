<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <?= $this->Flash->render() ?>
            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo __('Task List'); ?></h3>
              <br/><br/>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="tasks_table" class="display table table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= __('Project Name'); ?></th>
                            <th><?= __('Task Code'); ?></th>
                            <th><?= __('Members'); ?></th>
                            <th><?= __('Grade'); ?></th>
                            <th><?= __('Actual Hours'); ?></th>
                            <th><?= __('Estimated Hours'); ?></th>
                            <th><?= __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($daily_work_results as $key => $value) {
                                echo "<tr>";
                                    echo "<td><a href='/admin/projects/detail/{$value['project_id']}'>". h($value['project']['name']) ."</a></td>";
                                    echo "<td><a href='/admin/tasks/detail/{$value['id']}'>". h($value['task_code']) ."</a></td>";
                                    $members = [];
                                    foreach ($value['daily_work_results'] as $k => $v) {
                                        $members[$v['user']['id']] = $v['user']['name'];
                                    }
                                    echo "<td>". h(implode(', ', $members)) ."</td>";

                                    echo "<td>";
                                        $grade_default = ($value['grade'] == 0) ? '' : $value['grade'];
                                        echo $this->Form->input('grade', [
                                                'class'        => 'form-control grade_option',
                                                'options'      => $all_grades,
                                                'div'          => false,
                                                'label'        => false,
                                                'id'           => 'grade',
                                                'data-id'      => $value['id'],
                                                'default'      => $grade_default,
                                            ]);
                                    echo "</td>";

                                    $actual_times = [];
                                    foreach ($value['daily_work_results'] as $k => $v) {
                                        $actual_times[] = $v['actual_times'];
                                    }
                                    echo "<td> ". implode(' hr/s, ', $actual_times) ." hrs</td>";

                                    echo "<td>". $value['estimate_times'] ." hrs</td>";

                                    echo "<td>";
                                    if ($value['project']['in_backlog'] == 0) {
                                        echo "<a href='/admin/tasks/edit/{$value['id']}'><button class='btn btn-success'>". __('Edit') ."</button></a>";
                                    }
                                    echo "</td>";

                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
                <br/>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
            </div>
            <!-- /.box-footer -->
          </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#tasks_table').DataTable();
      $('.active-tasks, .active-tasks-list').addClass('active');
    });
    // Display Change Grade modal, then set values on hidden inputs
    $('.grade_option').on('change', function() {
      var grade = $(this).val();
      var id    = $(this).data('id');

      if (grade !== '') {
        $('#confirmation_modal').modal('show');
        $('.modal-message').text("<?= __('Are you sure you want to change the grade of this task?'); ?>");
        $('.submitForm').attr('data-id', id);
        $('.submitForm').attr('data-grade', grade);
      }
    });

    // Add/edit grade of task
    $("html").delegate(".submitForm","click", function(){
        var editGradeId = $('.submitForm').data('id');
        var editGrade   = $('.submitForm').data('grade');

        var ajax_result = ajaxHandler('/admin/tasks/grade_task', {
                            id    : editGradeId,
                            grade : editGrade
                        });

        ajax_result.then(function(response) {
            if (response.status == true) {
               $('#confirmation_modal').modal('hide');
               $("#successMsg").html(response.message);
               $("#successModal").modal('show');
            } else {
               $('#confirmation_modal').modal('hide');
               $("#errorMsg").html(response.message);
               $("#errorModal").modal('show');
            }
        }).catch(function(fromReject) {
          $("#errorMsg").html("<?= 'Something went wrong'; ?>");
          $("#errorModal").modal('show');
        });

    });
</script>
