<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo __('Add Issues'); ?></h3><br/>
        </div>
        <div class="box-body">
          <?= $this->Form->create('', ['type' => 'GET']); ?>
          <div class="container col-md-12">
            <div class="form-group col-md-5">
              <label for="keyword" class="col-sm-4 control-label">
                <?= __('Keyword') ?>
              </label>
              <div class="col-sm-8" id="keyword_div">
                <?=
                  $this->Form->control('keyword', [
                    'class'       =>'form-control',
                    'required'    => false,
                    'id'          => 'keyword',
                    'placeholder' => __('Keyword'),
                    'label'       => false,
                    'value'       => isset($_GET['keyword']) ? $_GET['keyword'] : ''
                  ]);
                ?>
              </div>
            </div>
            <div class="form-group col-md-5">
              <label for="project_name" class="col-sm-5 control-label">
                <?= __('Project Name') ?>
              </label>
              <div class="col-sm-7" id="name_div">
                <?=
                  $this->Form->control('project_id', [
                    'class'       =>'form-control',
                    'id'          => 'project_id_add',
                    'options'     => $projects,
                    'empty'       => ['0' => '---'],
                    'required'    => false,
                    'label'       => false,
                    'value'       => isset($_GET['project_id']) ? $_GET['project_id'] : ''
                  ]);
                ?>
              </div>
            </div>
            <div class="form-group col-md-1">
              <input type="submit" class="btn btn-primary pull-right" id="confirmBtn" value="Submit">
            </div>
          </div>
          <?= $this->Form->end(); ?>
          <!-- List of Users -->
          <div class="container col-md-12 data-table">
            <br/><br/>
            <table id="tasks" class="table table-responsive table-striped table-bordered display" cellspacing="0" width="100%">
              <thead>
                <tr>
                 <th><?= __('ID'); ?></th>
                 <th><?= __('Project Name'); ?></th>
                 <th><?= __('Task Code'); ?></th>
                 <th><?= __('Task Name'); ?></th>
                 <th><?= __('Estimated Hour'); ?></th>
                 <th><?= __('Status'); ?></th>
                 <th><?= __('Action'); ?></th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($issues)): ?>
                <?php foreach($issues as $key => $value): ?>
                <tr>
                  <td><?= $value['id'] ?></td>
                  <td><?= $projects[$value['projectId']]; ?></td>
                  <td><?= $value['issueKey'] ?></td>
                  <td><?= $value['summary'] ?></td>
                  <td><?= $value['estimatedHours'] ?></td>
                  <td><?= $status[$value['status']['id']] ?></td>
                  <td><a class="add" href="javascript:void(0);" id="<?= $value['id'] ?>">Add</a></td>
                  <?= $this->Form->create('', ['type'=>'POST', 'id'=>'add-'.$value['id']])?>
                  <?= $this->Form->control('id', ['type'=>'hidden','value'=>$value['id']]); ?>
                  <?= $this->Form->control('project_id', ['type'=>'hidden','value'=>$value['projectId']]); ?>
                  <?= $this->Form->control('milestone_id', ['type'=>'hidden','value'=>!empty($value['milestone'][0]['id']) ? $value['milestone'][0]['id'] : null ]); ?>
                  <?= $this->Form->control('task_code', ['type'=>'hidden','value'=>$value['issueKey']]); ?>
                  <?= $this->Form->control('name', ['type'=>'hidden','value'=>$value['summary']]); ?>
                  <?= $this->Form->control('estimate_times', ['type'=>'hidden','value'=> $value['estimatedHours'] == '' ? 0 : $value['estimatedHours']]); ?>
                  <?= $this->Form->control('is_close', ['type'=>'hidden','value'=>0]); ?>
                  <?= $this->Form->end(); ?>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('.add').on('click', function(){
      var id   = $(this).attr('id');
      var data = $('#add-'+id).serialize();
      var ajax_result = ajaxHandler('/admin/tasks/add_issue', {
            data : data
          });

      ajax_result.then(function(response) {
        if (response.status === true) {
          $("#successModal").modal('show');
          $("#successMsg").html(response.message);

        } else {
          $("#errorModal").modal('show');
          $("#errorMsg").html(response.message);
        }
      }).catch(function(fromReject) {
        $("#errorMsg").html("<?= 'Something went wrong'; ?>");
        $("#errorModal").modal('show');
      });
    });
    $('#tasks').DataTable({});
  });
</script>
