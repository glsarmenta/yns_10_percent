<div class="content-wrapper">
  <section class="content">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo __('Add Task'); ?></h3>
              <br/>
            </div>

            <div class="box-body">
              <form>
                <div class="container col-md-8 col-md-offset-2">               
                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Project') ?></label>
                        <div class="col-sm-9" id="project_id_div">
                            <?= $this->Form->input('project_id', [
                                  'class'   => 'form-control project',
                                  'div'     => false,
                                  'label'   => false,
                                  'empty'   => '---',
                                  'id'      => 'project_id',
                                  'options' => h($projects)
                                ]);
                            ?>
                          <span id="error_project_id" class="help-block text-red"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Task Code') ?></label>
                        <div class="col-sm-9" id="task_code_div">
                            <?= $this->Form->control('task_code', [
                                  'class'       =>'form-control',
                                  'id'          => 'task_code',
                                  'required'    => false,
                                  'placeholder' => __('Task Code'),
                                  'label'       => false
                                ]);
                            ?>
                          <span id="error_task_code" class="help-block text-red"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Task Name') ?></label>
                        <div class="col-sm-9" id="name_div">
                            <?= $this->Form->control('name', [
                                  'class'       =>'form-control',
                                  'id'          => 'name',
                                  'required'    => false,
                                  'placeholder' => __('Task Name'),
                                  'label'       => false
                                ]);
                            ?>
                          <span id="error_name" class="help-block text-red"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Estimate Time') ?></label>
                        <div class="col-sm-9" id="estimate_times_div">
                            <?= $this->Form->control('estimate_times', [
                                  'class'       => 'form-control estimate_times',
                                  'id'          => 'estimate_times',
                                  'required'    => false,
                                  'label'       => false,
                                  'type'        => 'number',
                                  'min'         => 0,
                                  'placeholder' => __('Estimate Time')
                                ]);
                            ?>
                          <span id="error_estimate_times" class="help-block text-red"></span>
                        </div>
                    </div>
                </div>

                <div class="container col-md-8 col-md-offset-2">
                    <br/>
                    <input type="reset" class="btn btn-default pull-left" id='clearBtn' name="clear" value="<?= __('Clear'); ?>">
                    <input type="button" class="btn btn-primary pull-right" id='confirmBtn' name="submit" value="<?= __('Submit'); ?>">
                </div>
              </form>
            </div>
        </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('.active-tasks, .active-tasks-add').addClass('active');
  });

  // Validation. If status = true, display and set data to confirmation modal
  $(document).delegate("#confirmBtn", "click", function(){
    var ajax_result = ajaxHandler('/admin/tasks/validate_add_edit', {
          project_id     : $('#project_id').val(),
          task_code      : $('#task_code').val(),
          name           : $('#name').val(),
          estimate_times : $('#estimate_times').val(),
          action         : 'add'
        });

    ajax_result.then(function(response) {
        // Reset error message div and span    
        $('.help-block').css('display', 'none');
        var keys = ['project_id', 'task_code', 'name', 'estimate_times'];
        keys.forEach(value => {
          $('#error_' + value).text('').css('display', 'none');
          $('#' + value + '_div').removeClass('has-error');
        });

        if (response.status === true) {
          $('#confirmation_modal').modal('show');
          $('.modal-message').text("<?= __('Are you sure you want to add this task?'); ?>");

        } else {
          jQuery.each(response.message, function(index, item) {
            $('#error_' + index).text(item).css('display', 'inline-block');
            $('#' + index + '_div').addClass('has-error');
          });
        }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Save if status = true
  $(document).delegate(".submitForm", "click", function(){
    $('#confirmation_modal').modal('hide');

    var ajax_result = ajaxHandler('/admin/tasks/add', {
          project_id     : $('#project_id').val(),
          task_code      : $('#task_code').val(),
          name           : $('#name').val(),
          estimate_times : $('#estimate_times').val()
        });

    ajax_result.then(function(response) {
        if (response.status === true) {
          $("#successModal").modal('show');
          $("#successMsg").html(response.message);
        } else {
          $("#errorModal").modal('show');
          $("#errorMsg").html(response.message);
        }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  }); 

  // Accept only decimal and non-negative inputs
  $('#estimate_times').keypress(function(event) {
    if ( (event.which != 46 || $(this).val().indexOf('.') != -1) 
          && (event.which < 48 || event.which > 57) ) {
        event.preventDefault();
    }
    if ( ($(this).val().indexOf('.') != -1) 
          && ($(this).val().substring($(this).val().indexOf('.'), $(this).val().indexOf('.').length).length > 2)) {
        event.preventDefault();
    }
  });

  // Reset form
  $("html").delegate("#clearBtn", "click", function(){
    $('#error_project_id, #error_task_code, #error_name, #error_estimate_times').text('').css('display', 'none');
    $('#project_id_div, #task_code_div, #name_div, #estimate_times_div').removeClass('has-error');
  });
</script>