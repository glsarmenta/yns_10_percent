<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="row">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo __('Task Detail'); ?></h3>
              <!-- /.box-header -->
              <div class="box-body">
                <h3 class="profile-username text-center"><?= h($task['task_code']); ?></h3>

                <p class="text-muted text-center"><?= h($task['name']); ?></p>

                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b><?= __('Grade'); ?></b>
                      <span class="pull-right">
                      <?php
                        if (isset($all_grades[$task['grade']])):
                          echo h($task['grade']).' ['.$all_grades[$task['grade']].']';
                        else:
                          echo __('0 [No Grade]');
                        endif;
                      ?>
                      </span>
                  </li>
                  <li class="list-group-item">
                    <b><?= __('Members'); ?></b> <span class="pull-right"><?php
                                    $members = [];
                                    foreach ($task['daily_work_results'] as $k => $v) {
                                        $members[$v['user']['id']] = $v['user']['name'];
                                    }
                                    echo  h(implode(',', $members)); ?></span>
                  </li>
                  <li class="list-group-item">
                    <b><?= __('Actual Time'); ?></b> <span class="pull-right"><?= h($task['total_actual_times']) . __(' hrs'); ?></span>
                  </li>
                  <li class="list-group-item">
                    <b><?= __('Estimated Time'); ?></b> <span class="pull-right"><?= h($task['estimate_times']) . __(' hrs'); ?></span>
                  </li>
                  <li class="list-group-item">
                    <b><?= __('Status'); ?></b> <span class="pull-right"><?= $task['is_close'] == 1 ? 'Close' : 'Open'; ?></span>
                  </li>
                  <li class="list-group-item">
                    <b><?= __('Project'); ?></b> <a class="pull-right" href="/admin/projects/detail/<?= h($task['project']['id']); ?>"><?= h($task['project']['name']); ?></a>
                  </li>
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div id='calendar'></div>
      </div>
    </div>
  </section>
</div>

<!-- Change Grade Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="gradeModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content tms__modal">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= __('Change grade confirmation'); ?></h4>
      </div>

      <div class="modal-body">
        <h5><?= __('Are you sure you want to change the grade of this task?'); ?></h5>

        <?= $this->Form->hidden('id', [
          'id' => 'hidden_id'
        ]);
        ?>
        <?= $this->Form->hidden('grade', [
          'id' => 'hidden_grade'
        ]);
        ?>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= __('Close'); ?></button>
        <button type="button" class="btn btn-success updateGrade"><?= __('Yes'); ?></button>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $('.active-tasks').addClass('active');
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      disableDragging: true,
      navLinks: true, // can click day/week names to navigate views
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      events: [
        <?php if (!empty($daily_works)): ?>
        <?php foreach ($daily_works as $key => $value): ?>
            {
              id   :'<?= $key; ?>',
              title:'<?= h($value['title']); ?>',
              start:'<?= h($value['start']); ?>',
            },
        <?php endforeach; ?>
        <?php endif; ?>
      ]
    });
  });

</script>
