<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
<?= $this->Html->script(['/js/admin.js']); ?>
<?= $this->Html->script(['/js/common_ajax.js']); ?>
<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo __('Summary of work hour per month and milestone'); ?></h3><br/>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <label>DURATION: </label>
                <select class="form-control select2" id="year-month">
                    <?php for($i = 0; $i < count($years); $i++): ?>
                        <?php for($y = 1; $y < 13; $y++): ?>
                            <?php if($today == $years[$i] . ' / ' . $y): ?>
                                <option selected value="<?= $years[$i] . $y ?>"><?= $years[$i] . ' / ' . $y ?></option>
                            <?php else : ?>
                                <option value="<?= $years[$i] . $y ?>"><?= $years[$i] . ' / ' . $y ?></option>
                            <?php endif; ?>
                        <?php endfor; ?>
                    <?php endfor; ?>
                </select>
            </div>
        </div>
        <br>
        <br>
        <div class="data-table">
            <table id="data-table" class="table table-responsive table-striped table-bordered display projects_table" cellspacing="0" width="100%">
              <thead>
                <tr>
                 <th><?= __('PROJECT NAME'); ?></th>
                 <th><?= __('MILESTONE'); ?></th>
                 <th><?= __('MEMBER'); ?></th>
                 <th><?= __('MONTHLY ACTUAL HOURS'); ?></th>
                </tr>
              </thead>
              <tbody id="data-result">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<style type="text/css">
  .font-weight-bold {
    font-weight: bold;
  }
  .checkbox-label {
    background-color: #eee;
  }
  .li-border {
    display: inline-block;
    margin-right: 10px;
    border-left: 1px solid #000000;
    position: relative;
  }
  .li-border:first-child {
    border-left: none;
  }
  .remove {
    position: absolute;
    top: -10px;
    right: -5px;
    color: red;
  }
  .calendar {
    padding-left: 0;
  }
  .calendar label {
    padding-top: 6px;
  }
  td, th {
    text-align: center;
  }
  th {
    background: #b3b3ff;
  }
  .member-search-list {
    display: none;
  }
  .data-table {
    display: none;
  }
  tr.group {
    background-color: cyan !important;
  }
</style>

