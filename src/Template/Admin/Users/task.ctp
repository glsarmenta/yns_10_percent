<style type="text/css">
    .no-padding {
        padding-right: 0 !important;
        padding-left: 0 !important;
    }
    .font12Arial{
        font-size: 12px;
        font-family: Arial;
    }
    label {
        font-weight: normal !important;
    }
</style>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
              <?= $this->Flash->render() ?>
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><?= __('Tasks') ?></h3>
                  <a role="button" class="btn btn-default pull-right" id="return" href="/admin/users"><?php echo __d('admin', '< Back to List'); ?></a>
                </div>
                <!-- /.box-header -->

                <div class="box-body table-responsive no-padding">
                    <h3 class="profile-username text-center"><?= h($user['name']); ?></h3>
                    <br/>
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group">
                            <label class="col-sm-3 control-label sm-input--label no-padding font12Arial"><?= __('Date: ') ?></label>
                            <div class="form-layout__container col-sm-9">
                                <div class="input-group date">
                                    <?= $this->Form->control('date_task_code', [
                                        'class'    =>'form-control datepicker font12Arial',
                                        'id'       => 'date_task_code',
                                        'required' => false,
                                        'label'    => false,
                                        'type'     => 'text'
                                        ]);
                                    ?>
                                    <div class="input-group-addon calendar_icon">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label sm-input--label no-padding font12Arial"><?= __('Start work time: ') ?></label>
                            <div class="form-layout__container col-sm-9">
                                <label class="font12Arial"><?= h($start_time); ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label sm-input--label no-padding font12Arial"><?= __('End work time: ') ?></label>
                            <div class="form-layout__container col-sm-9">
                                <label class="font12Arial"><?= h($end_time); ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label sm-input--label no-padding font12Arial"><?= __('Total work time: ') ?></label>
                            <div class="form-layout__container col-sm-9">
                                <label class="font12Arial"><?= $total_work_time . __(' hrs'); ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label sm-input--label no-padding font12Arial"><?= __('Total actual time: ') ?></label>
                            <div class="form-layout__container col-sm-9">
                                <label class="font12Arial"><?= $total_actual_time . __(' hrs'); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <table id="tasks_table" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><?= __('Project Name'); ?></th>
                                    <th><?= __('Issue Name'); ?></th>
                                    <th><?= __('Task Code'); ?></th>
                                    <th><?= __('Actual Hours'); ?></th>
                                    <th><?= __('Estimated Hours'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($daily_work_results as $key => $value) {
                                        echo "<tr>";
                                            echo "<td>". $value['task']['project']['name'] ."</td>";
                                            echo "<td>". h($value['task']['name']) ."</td>";
                                            echo "<td class='task_code'>". h($value['task']['task_code']) ."</td>";
                                            echo "<td>". $value['actual_times'] ." hrs</td>";
                                            echo "<td>". $value['task']['estimate_times'] ." hrs</td>";
                                        echo "</tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                        <br/>
                    </div>

                    <div class="col-md-12">
                        <article class="col-md-9">
                            <label class="font12Arial"><b><?= __('Comment'); ?></b></label>
                            <?= $this->Form->textarea('comment', [
                                    'class'    =>'form-control',
                                    'id'       => 'comment',
                                    'required' => false,
                                    'value'    => stripcslashes($daily_report['comment']),
                                    'label'    => false,
                                    'rows'     => 5,
                                    'readonly' => true,
                                ]);
                            ?>
                        </article>
                    </div>

                    <div class="col-md-12">
                        <article class="col-md-9">
                        <br/>
                        <label class="font12Arial"><b><?= __('Plan for Tomorrow'); ?></b></label>
                            <?= $this->Form->textarea('plan_for_tomorrow', [
                                    'class'    =>'form-control',
                                    'id'       => 'plan_for_tomorrow',
                                    'required' => false,
                                    'value'    => stripcslashes($daily_report['plan_for_tomorrow']),
                                    'label'    => false,
                                    'rows'     => 5,
                                    'readonly' => true,
                                ]);
                            ?>
                        </article>
                    </div>
                </div>
              </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#tasks_table').DataTable();
      $("#date_task_code").val("<?= h($date_selected); ?>");
    });

    $('.datepicker').datepicker({
      format    : 'yyyy-mm-dd',
      weekStart : 0
    }).datepicker("update", "<?= h($date_selected); ?>");

    $("#date_task_code").on('changeDate change paste', function() {
      var user_id   = <?= $id; ?>;
      var new_date  = $(this).val();

      window.location = '/admin/users/task/' + user_id + '/' + new_date;
    });
</script>
