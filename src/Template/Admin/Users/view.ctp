<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Daily Reports'), ['controller' => 'DailyReports', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Daily Report'), ['controller' => 'DailyReports', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Daily Work Results'), ['controller' => 'DailyWorkResults', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Daily Work Result'), ['controller' => 'DailyWorkResults', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $this->Number->format($user->role) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password Updated') ?></th>
            <td><?= $this->Number->format($user->password_updated) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Enable') ?></th>
            <td><?= $this->Number->format($user->is_enable) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted') ?></th>
            <td><?= $this->Number->format($user->deleted) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted Date') ?></th>
            <td><?= h($user->deleted_date) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Daily Reports') ?></h4>
        <?php if (!empty($user->daily_reports)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Start Time') ?></th>
                <th scope="col"><?= __('End Time') ?></th>
                <th scope="col"><?= __('Business Date') ?></th>
                <th scope="col"><?= __('Comment') ?></th>
                <th scope="col"><?= __('Plan For Tomorrow') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Deleted') ?></th>
                <th scope="col"><?= __('Deleted Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->daily_reports as $dailyReports): ?>
            <tr>
                <td><?= h($dailyReports->id) ?></td>
                <td><?= h($dailyReports->user_id) ?></td>
                <td><?= h($dailyReports->start_time) ?></td>
                <td><?= h($dailyReports->end_time) ?></td>
                <td><?= h($dailyReports->business_date) ?></td>
                <td><?= h($dailyReports->comment) ?></td>
                <td><?= h($dailyReports->plan_for_tomorrow) ?></td>
                <td><?= h($dailyReports->created) ?></td>
                <td><?= h($dailyReports->modified) ?></td>
                <td><?= h($dailyReports->deleted) ?></td>
                <td><?= h($dailyReports->deleted_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DailyReports', 'action' => 'view', $dailyReports->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DailyReports', 'action' => 'edit', $dailyReports->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DailyReports', 'action' => 'delete', $dailyReports->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dailyReports->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Daily Work Results') ?></h4>
        <?php if (!empty($user->daily_work_results)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Task Id') ?></th>
                <th scope="col"><?= __('Actual Times') ?></th>
                <th scope="col"><?= __('Work Date') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Deleted') ?></th>
                <th scope="col"><?= __('Deleted Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->daily_work_results as $dailyWorkResults): ?>
            <tr>
                <td><?= h($dailyWorkResults->id) ?></td>
                <td><?= h($dailyWorkResults->user_id) ?></td>
                <td><?= h($dailyWorkResults->task_id) ?></td>
                <td><?= h($dailyWorkResults->actual_times) ?></td>
                <td><?= h($dailyWorkResults->work_date) ?></td>
                <td><?= h($dailyWorkResults->created) ?></td>
                <td><?= h($dailyWorkResults->modified) ?></td>
                <td><?= h($dailyWorkResults->deleted) ?></td>
                <td><?= h($dailyWorkResults->deleted_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DailyWorkResults', 'action' => 'view', $dailyWorkResults->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DailyWorkResults', 'action' => 'edit', $dailyWorkResults->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DailyWorkResults', 'action' => 'delete', $dailyWorkResults->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dailyWorkResults->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
