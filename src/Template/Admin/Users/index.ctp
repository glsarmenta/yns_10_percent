<div class="content-wrapper">
<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><?= __('Users') ?></h3>
            </div>
            <!-- /.box-header -->
            <?= $this->Flash->render() ?>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-striped" id="table_users">
                    <thead>
                      <tr>
                          <th scope="col"><?= __('id'); ?></th>
                          <th scope="col"><?= __('name'); ?></th>
                          <th scope="col"><?= __('role'); ?></th>
                          <th scope="col"><?= __('email'); ?></th>
                          <th scope="col"><?= __('is_enable'); ?></th>
                          <th scope="col" class="actions"><?= __('Actions') ?></th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user): ?>
                        <tr data-id="tms_<?= ($user->id);?>" >
                            <td><?= $this->Number->format($user->id) ?></td>
                            <td><?= h($user->name) ?></td>
                            <td><?= $user->role == 2 ? "Member" : "Admin"; ?></td>
                            <td><?= h($user->email) ?></td>
                            <td class="status"><?= $user->is_enable == 1 ? "Enabled" : "Disabled"; ?></td>

                            <?php
                                // Actions
                                if ($user->id != $user_id) {
                                    $status = $user->is_enable == 1 ? "Disabled" : "Enabled";

                                    echo "<td class='actions'>";
                                    echo $this->Html->link(__('Task'), ['action' => 'task', $user->id]);
                                    echo " / ";
                                    echo "<a href='#' data-id='". $user->id ."' id='change_status' class='change_status' data-toggle='modal' data-target='#confirmation_modal'>". $status ."</a>";
                                    echo "</td>";
                                } else {
                                    echo "<td class='actions'>";
                                    echo $this->Html->link(__('Task'), ['action' => 'task', $user->id]);
                                    echo "</td>";
                                }
                            ?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    </div>
</section>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    var miss_registration_tasks_id = '';

    $('#successModal').on('hidden.bs.modal', function () {
      location.reload();
    })

    $('.close').click(function(){
      location.reload();
    });

    $('#successBtn').click(function(){
      location.reload();
    });

    $('#table_users').DataTable({
      "aoColumnDefs": [ {
        "aTargets"  : [5],
        "bSortable" : false,
        "bSearchable" : false
      }]
    });
    $('.active-users, .active-users-list').addClass('active');
  });

  var user_id;

  $(document).delegate(".change_status", "click", function(){
    user_id = $(this).data('id');

    $('.submitForm').attr("data-user_id", user_id);
    $('.modal-message').text("<?= __('Are you sure you want to change this status?'); ?>");
  });

  $(document).delegate(".submitForm", "click", function(){
    $('#confirmation_modal').modal('hide');
    $('.loading_modal').show();

    var ajax_result = ajaxHandler('/admin/users/change_status',{user_id : user_id});

    ajax_result.then(function(response) {
      if(response.status === true) {
        $('.loading_modal').hide();

        $("#successMsg").html(response.message);
        $("#successModal").modal('show');

        $('table tr[data-id="tms_'+ user_id +'"]').html("<td>"+ Number(user_id).toLocaleString('en') +"</td><td>"+ response.name +"</td><td>"+ response.role +"</td><td>"+ response.email +"</td><td>"+ response.new_status +"</td><td><a href='/admin/users/task/"+ user_id +"'>Task</a> / <a href='#' data-id='"+ user_id +"' id='change_status' class='change_status' data-toggle='modal' data-target='#confirmation_modal'>"+ response.old_status +"</a></td>");

        // reset datatable
        $('#table_users').dataTable().fnDestroy();
        $('#table_users').DataTable({
          "aoColumnDefs": [ {
            "aTargets"  : [5],
            "bSortable" : false,
            "bSearchable" : false
          }]
        });
      } else {
        $("#errorMsg").html(response.message);
        $("#errorModal").modal('show');
      }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });

  });
</script>
