<div class="content-wrapper">
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title"><?= __('Users') ?></h3>
        </div>
        <hr>
        <ul class="tab">
          <li class="tab-list <?= !isset($date) ? 'tab-active' : '' ; ?>">
            <a href="/admin/users/miss_registration" class="tab-link"> All </a>
          </li>
          <li class="tab-list <?= (isset($date) && $date == date("Y-m-d",strtotime("-1 days"))) ? 'tab-active' : '' ; ?>">
            <a href="/admin/users/miss_registration/<?= date("Y-m-d",strtotime("-1 days")) ?>" class="tab-link"> Yesterday </a>
          </li>
          <li class="tab-list <?= (isset($date) && $date == date("Y-m-d")) ? 'tab-active' : '' ; ?>">
            <a href="/admin/users/miss_registration/<?= date("Y-m-d") ?>" class="tab-link"> This Month </a>
          </li>
          <li class="tab-list <?= (isset($date) && $date == date("Y-m-d", strtotime("first day of previous month"))) ? 'tab-active' : '' ; ?>">
            <a href="/admin/users/miss_registration/<?= date("Y-m-d", strtotime("first day of previous month")) ?>" class="tab-link"> Last Month </a>
          </li>
        </ul>
        <hr>
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover table-striped" id="table_users">
            <thead>
              <tr>
                <th scope="col"><?= __('id'); ?></th>
                <th scope="col"><?= __('name'); ?></th>
                <th scope="col"><?= __('miss registration count'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($users as $user): ?>
                <?php if(!empty($user->miss_registration_tasks)): ?>
                  <tr data-id="tms_<?= ($user->id);?>" >
                    <td><?= $this->Number->format($user->id) ?></td>
                    <td><?= h($user->name) ?></td>
                    <td>
                      <?php if(count($user->miss_registration_tasks)> 0): ?>
                        <span><a href id = "showMissDetail" data-id = '<?= $user->id ?>'><?= count($user->miss_registration_tasks); ?></a></span>
                      <?php else: ?>
                        <span data-id = '<?= $user->id ?>'><?= count($user->miss_registration_tasks); ?></span>
                      <?php endif; ?>
                    </td>
                  </tr>
                <?php endif; ?>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!--modal for show miss details-->
    <div class="modal fade" tabindex="-1" role="dialog" id="showMissDetails">
      <div class="modal-dialog" role="document">
        <div class="modal-content tms__modal">
          <div class="modal-header">
            <h4 class="modal-title"><?= __('Task Miss Registration Details'); ?></h4>
          </div>
          <ul class="modal-tab">
            <li class="modal-tab-list modal-tab-active">
              <a href="javascript:void(0);" class="modal-tab-link" id="all"> All </a>
            </li>
            <li class="modal-tab-list">
              <a href="javascript:void(0);" class="modal-tab-link" id="<?= date('Y-m-d',strtotime('-1 days')); ?>"> Yesterday </a>
            </li>
            <li class="modal-tab-list">
              <a href="javascript:void(0);" class="modal-tab-link" id="<?= date("Y-m-d") ?>"> This Month </a>
            </li>
            <li class="modal-tab-list">
              <a href="javascript:void(0);" class="modal-tab-link" id="<?= date("Y-m-d", strtotime("first day of previous month")) ?>"> Last Month </a>
            </li>
          </ul>
          <hr>
          <div class="modal-body pre-scrollable">
            <table class="table table-hover table-striped" id="table_users">
              <thead>
                 <tr>
                  <th scope="col"><?= __('Count'); ?></th>
                  <th scope="col"><?= __('Date'); ?></th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody id = "missList"></tbody>
            </table>
            <div id="missError"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><?= __('Close'); ?></button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="ignoreCountModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content tms__modal">
          <div class="modal-header">
            <h4 class="modal-title"><?= __('Confimation'); ?></h4>
          </div>
          <div class="modal-body">
            <h5> <?= 'Are you sure you want to ignore?' ?> </h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><?= __('Close'); ?></button>
            <button type="button" id = "ignoreMissCount" class="btn btn-success" ><?= __('Yes'); ?></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
<input type="hidden" id="sortDate" value="<?= isset($date) ? $date : 'all' ?>">
<style type="text/css">
  .modal-tab, .tab{
    padding: 0;
    margin: 15px 0;
    list-style: none;
    font-size: 0;
  }
  .modal-tab-list, .tab-list {
    display: inline-block;
    width: 25%;
    text-align: center;
    font-size: 14px;
    list-style: none;
    border-right: 1px solid #CCCCCC;
  }
  .modal-tab-link, .tab-link {
    color: #000000;
  }
  .modal-tab-active .modal-tab-link, .tab-active .tab-link {
    border-bottom: 1px solid blue;
    color: blue;
  }
  .modal-tab-list:last-child, .tab-list:last-child {
    border-right: none;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
    var missDetailId = '';
    var miss_registration_tasks_id = '';

    $('#table_users').DataTable({
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ]
    });
    $('.active-users, .active-users-miss-register').addClass('active');

    $(document).delegate('#showMissDetail', 'click', function(e) {
      e.preventDefault();
      missDetailId = $(this).data('id');

      var date = $('#sortDate').val();
      var html = '';
      var ajax_result = ajaxHandler('/admin/users/get_miss_count', {
        id: $(this).data('id'),
        date: date
      });

      ajax_result.then(function(response){
        $('#missList').html('');
        $('#missError').html('');
        var ctr = 0;
        $.each(response, function(i,v){
          ctr++;
          html +='<tr>'
                + '<td>'+ctr+'</td>'
                + '<td>'+v.miss_date+'</td>'
                + '<td><a href id= "ignoreCount" data-id = "'+v.id+'" > Ignore </a></td>'
              + '</tr>';
        });
        $('#missList').append(html);
        $('#showMissDetails').modal('show');
      });
      $('.modal-tab').find('li').removeClass('modal-tab-active');
      console.log('all');
      $('#'+date).parent().addClass('modal-tab-active');
      // $('.modal-tab li:first').addClass('modal-tab-active');
    });

    $('.modal-tab-list').on('click', function() {
      var date = $(this).find('a').attr('id');
      $('.modal-tab').find('li').removeClass('modal-tab-active');
      $(this).addClass('modal-tab-active');
      var ajax_result = ajaxHandler('/admin/users/get_miss_count', {
        id  : missDetailId,
        date: date
      });

      ajax_result.then(function(response){
        $('#missList').html('');
        $('#missError').html('');
        var ctr  = 0;
        var html = '';
        if (response != 0) {
            $.each(response, function(i,v){
              ctr++;
              html +='<tr>'
                    + '<td>'+ctr+'</td>'
                    + '<td>'+v.miss_date+'</td>'
                    + '<td><a href id= "ignoreCount" data-id = "'+v.id+'" > Ignore </a></td>'
                  + '</tr>';
            });
            $('#missList').append(html);
        } else {
            html = '<span>No result found.</span>';
            $('#missError').append(html);
            $('#missError').css('text-align','center');
            html = '';
        }
      });
    });

    $('.close').click(function(){
      $('#showMissDetails').modal('hide');
      $('#ignoreCountModal').modal('hide');
      location.reload();
    });

    $('#successBtn').click(function(){
      $('#showMissDetails').modal('hide');
      $('#ignoreCountModal').modal('hide');
      location.reload();
    });

    $('#ignoreMissCount').click(function() {
      var ajax_result = ajaxHandler('/admin/users/ignore_miss_count', {
        id:miss_registration_tasks_id
      });
      ajax_result.then(function(response){
        $("#successMsg").html(response.message);
        $('#showMissDetails').modal('hide');
        $('#ignoreCountModal').modal('hide');
        $("#successModal").modal('show');
      });
    });

    $('#cancelIgnoreCountModal').click(function(){
      $('#ignoreCountModal').modal('hide');
    });

    $(document).delegate('#ignoreCount','click', function(e){
      e.preventDefault();
      $('#ignoreCountModal').modal('show');
      miss_registration_tasks_id = $(this).data('id');
    });

    $('#cancelModal').click(function(){
      $('#showMissDetails').modal('hide');
    });
  });
</script>
