<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Daily Reports'), ['controller' => 'DailyReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Daily Report'), ['controller' => 'DailyReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Daily Work Results'), ['controller' => 'DailyWorkResults', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Daily Work Result'), ['controller' => 'DailyWorkResults', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('role');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('password_updated');
            echo $this->Form->control('is_enable');
            echo $this->Form->control('deleted');
            echo $this->Form->control('deleted_date', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
