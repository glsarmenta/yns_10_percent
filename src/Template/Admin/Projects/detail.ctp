<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="row">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo __('Project Detail'); ?></h3>
            </div>
            <div class="box-body">
              <h3 class="profile-username text-center"><?= h($project[0]['name']); ?></h3>
              <p class="text-muted text-center"><?= h($project[0]['project_code']); ?></p>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item" style="display: inline-block; width: 100%;">
                  <b><?= __('Members'); ?></b>
                  <a class="text-primary fa fa-edit" title="<?= __('Edit members'); ?>" href="/admin/projects/edit/<?= $id; ?>"></a>
                  <span class="pull-right">
                      <?php
                          foreach ($project as $key => $value) {
                              foreach ($value['project_users'] as $k => $v) {
                                  echo h($v['user']['name']) .'<br/>';
                              }
                          }
                      ?>
                  </span>
                </li>
                <li class="list-group-item">
                  <b><?= __('Total Actual Time'); ?></b>
                  <span class="pull-right"><?= $project[0]['total_actual_times'] .__(' hrs'); ?></span>
                </li>
                <li class="list-group-item">
                  <b><?= __('Total Estimate Time'); ?></b>
                  <span class="pull-right"><?= $project[0]['total_estimate_times'] .__(' hrs'); ?></span>
                </li>
              </ul>
              <br/>
              <!-- DataTable -->
              <table id="tasks_table" class="table table-responsive display" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th><?= __('Task Code'); ?></th>
                          <th><?= __('Members'); ?></th>
                          <th><?= __('Grade'); ?></th>
                          <th><?= __('Actual Hours'); ?></th>
                          <th><?= __('Estimated Hours'); ?></th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                          foreach ($daily_work_results as $key => $value) {
                              echo "<tr>";
                                  echo "<td>". h($value['task_code']) ."</td>";

                                  $members = [];
                                  foreach ($value['daily_work_results'] as $k => $v) {
                                      $members[$v['user']['id']] = $v['user']['name'];
                                  }
                                  echo "<td>". h(implode(', ', $members)) ."</td>";
                                  if (isset($all_grades[$value['grade']])):
                                  echo "<td>". h($value['grade']) ." [". $all_grades[$value['grade']] ."]</td>";
                                  else:
                                  echo "<td>". __('0 [No Grade]') ."</td>";
                                  endif;
                                  $actual_times = 0;
                                  foreach ($value['daily_work_results'] as $k => $v) {
                                      $actual_times = $actual_times + $v['actual_times'];
                                  }
                                  echo "<td> ". $actual_times . __(' hrs') ."</td>";

                                  echo "<td>". $value['estimate_times'] . __(' hrs') ."</td>";

                              echo "</tr>";
                          }
                      ?>
                  </tbody>
              </table>
            </div>
          <!-- /.box-body -->
            <div class="box-footer clearfix">
            </div>
          </div>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo __('Milestones'); ?></h3>
            </div>
            <div class="box-body">
              <table class="table table-responsive display" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th><?= __('Name'); ?></th>
                    <th><?= __('Estimated Hours'); ?></th>
                    <th><?= __('Actual Hours'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($milestone as $key => $value): ?>
                    <tr>
                      <td><?php echo $value->name ; ?></td>
                      <td><?php echo $value->total_estimate_time ; ?></td>
                      <td><?php echo $value->actual_time; ?></td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>

          <br/>

          <!-- Bar Chart -->
          <div class="chartWrapper row-fluid">
              <div class="chartAreaWrapper graph">
                  <div class="chartAreaWrapper2">
                      <canvas id="bar-chart" height="250" width="1200"></canvas>
                  </div>
              </div>
          </div>

          <br/><br/>
        </div>
      </div>
    </div>
  </section>
</div>

<script>
  $(document).ready(function() {
    $('.active-projects').addClass('active');
    $('#tasks_table').DataTable({searching: false});

    var ctx = document.getElementById('bar-chart').getContext('2d');
    var data = {
      labels: <?= $task_codes; ?>,
      datasets: [
        {
          label: "<?= __('Actual Time'); ?>",
          borderColor: "#3e95cd",
          backgroundColor: "#3e95cd",
          fill: true,
          data: <?= $actual_time; ?>
        },
        {
          label: "<?= __('Estimate Time'); ?>",
          borderColor: "#8e5ea2",
          backgroundColor: "#8e5ea2",
          fill: true,
          data: <?= $estimate_time; ?>
        }
      ]
    };

    var options = {
      title: {
        display: true,
        text: "<?= __('Actual Time and Estimate Time per Task'); ?>"
      },
      scales: {
        xAxes: [{
          ticks: {
            beginAtZero:true
          },
          scaleLabel: {
            display: true,
            labelString: 'Task Code'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero:true,
            max: <?= $max; ?>
          },
          scaleLabel: {
            display: true,
            labelString: 'Hours'
          },
          gridLines: {
            lineWidth: 2,
          }
        }]
      }
    };

    $(function() {
      var chartFuelSpend = new Chart(ctx, {
        type: 'bar',
        data: data,
        maintainAspectRatio: true,
        responsive: true,
        options: options
      });

      var newwidth = $('.chartAreaWrapper2').width() + ($('.chartAreaWrapper2').width() / .5);
      $('.chartAreaWrapper2').width(newwidth);
    });
  });
</script>
