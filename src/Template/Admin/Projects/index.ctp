<div class="content-wrapper">
  <section class="content">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo __('Project List'); ?></h3>
              <br/><br/>
            </div>

            <div class="box-body">
                <table id="projects_table" class="display table table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= __('Project Name'); ?></th>
                            <th><?= __('Total Actual Time'); ?></th>
                            <th><?= __('Total Estimate Time'); ?></th>
                            <th><?= __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($projects as $key => $value) {
                                echo "<tr>";
                                    echo "<td><a href='/admin/projects/detail/{$value['id']}'>". h($value['name']) ."</a></td>";
                                    echo "<td>". $value['total_actual_times'] . __(' hrs') ."</td>";
                                    echo "<td>". $value['total_estimate_times'] . __(' hrs') ."</td>";
                                    echo "<td><a href='/admin/projects/edit/{$value['id']}'><button class='btn btn-success'>". __('Edit') ."</button></a></td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>

            <div class="box-footer clearfix">
            </div>
        </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('.active-projects, .active-projects-list').addClass('active');
    $('#projects_table').DataTable({});
  });
</script>
