<div class="content-wrapper">
  <section class="content">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo __('Add Project'); ?></h3>
              <br/>
            </div>

            <div class="box-body">
                <div class="container col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Project Name') ?></label>
                        <div class="col-sm-9" id="name_div">
                          <?= $this->Form->control('name', [
                                  'class'       =>'form-control',
                                  'id'          => 'name',
                                  'required'    => false,
                                  'placeholder' => __('Project Name'),
                                  'label'       => false
                              ]);
                          ?>
                          <span id="error_name" class="help-block text-red"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_code" class="col-sm-3 control-label"><?= __('Project Code') ?></label>
                        <div class="col-sm-9" id="project_code_div">
                          <?= $this->Form->control('project_code', [
                                  'class'       =>'form-control',
                                  'required'    => false,
                                  'id'          => 'project_code',
                                  'placeholder' => __('Project Code'),
                                  'label'       => false
                              ]);
                          ?>
                          <span id="error_project_code" class="help-block text-red"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="is_common" class="col-sm-3 control-label"><?= __('Common Project') ?></label>
                        <div class="col-sm-9" id="is_common_div">
                          <?=  $this->Form->checkbox('is_common', [
                                'value'         => 1,
                                'hiddenField'   => 0,
                                'id'            => 'is_common',
                                'label'         => false
                            ]);
                          ?>
                          <span id="error_is_common" class="help-block text-red"></span>
                        </div>
                    </div>
                </div>

                <div class="container col-md-8 col-md-offset-2">
                    <br/>
                    <input type="button" class="btn btn-default pull-left" id='clearBtn' name="clear" value="<?= __('Clear'); ?>">
                    <input type="button" class="btn btn-primary pull-right" id='confirmBtn' name="submit" value="<?= __('Submit'); ?>">
                </div>

                <!-- List of Users -->
                <div class="col-md-10 col-md-offset-1">
                    <br/><br/>
                    <table id="projects_table" class="table table-responsive table-striped table-bordered display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th id="th_select">
                                    <input type='checkbox' class='is_allmembers' name='is_allmembers' id="is_allmembers" title="<?= __('Select All'); ?>">
                                    <?= __('Select Member'); ?>
                                </th>
                                <th id="th_member_name"><?= __('Member Name'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($users as $key => $value) {
                                    echo "<tr>";
                                        echo "<td id='td_select'><center><input type='checkbox' class='checkbox_user' name='user_id' value='". $key ."'></center></td>";
                                        echo "<td id='td_member_name'>". h($value) ."</td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('.active-projects, .active-projects-add').addClass('active');
    $('#projects_table').DataTable( {
        "scrollY":        "350px",
        "scrollCollapse": true,
        "paging":         false,
        "aaSorting":      [[0]],
        "aoColumnDefs":   [{ "bSortable": false, "aTargets": [0] }]
    });
  });

  // Select ALL members
  $('#is_allmembers').click(function(){
    $('[class=checkbox_user]').prop('checked', $(this).is(':checked'));
  });

  // Select is_allmember to 'YES' if all members' checkboxes are checked
  $(".checkbox_user").change(function(){
    if ($('.checkbox_user:checked').length == $('.checkbox_user').length) {
        $('#is_allmembers').prop('checked', true);
    } else {
        $('#is_allmembers').prop('checked', false);
    }
  });

  // Validation. If status = true, display and set data to confirmation modal
  $(document).delegate("#confirmBtn", "click", function(){
    var is_allmembers = ($('#is_allmembers').is(':checked') === true) ? 1 : 0;
    var user_ids = $('input[name=user_id]:checked').map(function() {
        return $(this).val();
    }).get();

    var ajax_result = ajaxHandler('/admin/projects/validate_add_edit', {
          name           : $('#name').val(),
          project_code   : $('#project_code').val(),
          is_allmembers  : is_allmembers,
          user_id        : user_ids,
          action         : 'add'
        });

    ajax_result.then(function(response) {
      // Reset error message div and span
      $('.help-block').css('display', 'none');
      var keys = ['name', 'project_code', 'is_allmembers'];
      keys.forEach(value => {
        $('#error_' + value).text('').css('display', 'none');
        $('#' + value + '_div').removeClass('has-error');
      });

      if (response.status === true) {
        $('#confirmation_modal').modal('show');
        $('.modal-message').text("<?= __('Are you sure you want to add this project?'); ?>");

      } else {
        jQuery.each(response.message, function(index, item) {
          if (response.message[index] === "<?= 'Please select a member'; ?>") {
              $("#errorMsg").html(response.message[index]);
              $("#errorModal").modal('show');
          } else {
              $('#error_' + index).text(response.message[index]).css('display', 'inline-block');
              $('#' + index + '_div').addClass('has-error');
          }
        });
      }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Save if status = true
  $(document).delegate(".submitForm", "click", function(){
    $('#confirmation_modal').modal('hide');

    var user_ids = $('input[name=user_id]:checked').map(function() {
        return $(this).val();
    }).get();

    var is_allmembers = ($('#is_allmembers').is(':checked') === true) ? 1 : 0;

    var ajax_result = ajaxHandler('/admin/projects/add', {
          name          : $('#name').val(),
          project_code  : $('#project_code').val(),
          is_allmembers : is_allmembers,
          user_id       : user_ids,
          is_common     : $('#is_common').is(':checked'),
          action        : 'add'
        });

    ajax_result.then(function(response) {
      if (response.status === true) {
        $("#successModal").modal('show');
        $("#successMsg").html(response.message);

      } else {
        $("#errorModal").modal('show');
        $("#errorMsg").html(response.message);
      }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Reset form
  $("html").delegate("#clearBtn", "click", function(){
    $('#error_name, #error_project_code, #error_is_allmembers').text('').css('display', 'none');
    $('#name_div, #project_code_div, #is_allmembers_div').removeClass('has-error');
    $('#is_allmembers').prop('checked', false);
    $('[class=checkbox_user]').prop('checked', false);
  });
</script>
