<div class="content-wrapper">
  <section class="content">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo __('Edit Project'); ?></h3>
              <a role="button" class="btn btn-default pull-right" id="return" href="/admin/projects"><?php echo __d('admin', '< Back to List'); ?></a>
              <br/>
            </div>

            <div class="box-body">
              <h3 class="profile-username text-center"><?= h($projects['name']); ?></h3>
              <p class="text-muted text-center"><?= h($projects['project_code']); ?></p>

              <form>
                <?php if($projects['in_backlog'] == 0): ?>
                <br/>
                <div class="container col-md-8 col-md-offset-2">               
                    <div class="form-group">
                        <label for="project_name" class="col-sm-3 control-label"><?= __('Project Name') ?></label>
                        <div class="col-sm-9" id="name_div">
                          <?= $this->Form->control('name', [
                                  'class'       =>'form-control',
                                  'id'          => 'name',
                                  'required'    => false,
                                  'placeholder' => __('Project Name'),
                                  'label'       => false,
                                  'value'       => h($projects['name'])
                              ]);
                          ?>
                          <span id="error_name" class="help-block text-red"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_code" class="col-sm-3 control-label"><?= __('Project Code') ?></label>
                        <div class="col-sm-9" id="project_code_div">
                          <?= $this->Form->control('project_code', [
                                  'class'       =>'form-control',
                                  'required'    => false,
                                  'id'          => 'project_code',
                                  'placeholder' => __('Project Code'),
                                  'label'       => false,
                                  'value'       => h($projects['project_code'])
                              ]); 
                          ?>
                          <?= $this->Form->hidden('old_project_code', [
                                  'required'    => false,
                                  'id'          => 'old_project_code',
                                  'value'       => h($projects['project_code'])
                              ]); 
                          ?>
                          <span id="error_project_code" class="help-block text-red"></span>
                        </div>
                    </div>
                </div>
                <br/>
                <?php endif; ?>

                <div class="col-md-10 col-md-offset-1">
                    <br/>
                    <table id="projects_table" class="table table-responsive table-striped table-bordered display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th id="th_select">
                                    <input type='checkbox' class='is_allmembers' name='is_allmembers' id="is_allmembers" title="<?= __('Select All'); ?>">
                                    <?= __('Select Member'); ?>
                                </th>
                                <th id="th_member_name"><?= __('Member Name'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($users as $key => $value) {
                                    $is_checked = '';
                                    if (in_array($key, $user_ids)) {
                                        $is_checked = 'checked';
                                    }
                                    echo "<tr>";
                                        echo "<td id='td_select'><center><input type='checkbox' class='checkbox_user' name='user_id' value='". $key ."'". $is_checked ."></center></td>";
                                        echo "<td id='td_member_name'>". h($value) ."</td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                      <span id="error_is_allmembers" class="help-block text-red"></span>
                </div>

                <div class="container col-md-8 col-md-offset-2">
                    <br/>
                    <input type="reset" class="btn btn-default pull-left" id='resetBtn' name="reset" value="<?= __('Reset'); ?>">
                    <input type="button" class="btn btn-primary pull-right" id='confirmBtn' name="submit" value="<?= __('Submit'); ?>">
                </div>
              </form>
            </div>
        </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  var is_allmembers = 0;

  $(document).ready(function() {
    $('.active-projects').addClass('active');
    $('#projects_table').DataTable( {
        "scrollY":        "350px",
        "scrollCollapse": true,
        "paging":         false,
        "aaSorting":      [[0]],
        "aoColumnDefs":   [{ "bSortable": false, "aTargets": [0] }]
    });
    toggleCheckboxUser();
  });

  // Select ALL members
  $('#is_allmembers').click(function(){    
    $('[class=checkbox_user]').prop('checked', $(this).is(':checked'));    
  });

  // Select is_allmember to 'YES' if all members' checkboxes are checked
  $(".checkbox_user").change(function(){
    toggleCheckboxUser();
  });

  // Select is_allmember to 'YES' if all members' checkboxes are checked
  $(document).on('keyup', "input[type='search']", function(){
    toggleCheckboxUser();
  });

  function toggleCheckboxUser() {
    if ($('.checkbox_user:checked').length == $('.checkbox_user').length) { 
        $('#is_allmembers').prop('checked', true);
        is_allmembers = 1;
    } else {
        $('#is_allmembers').prop('checked', false);
        is_allmembers = 0;
    }
  }

  // Validation. If status = true, display and set data to confirmation modal
  $(document).delegate("#confirmBtn", "click", function(){
    // Clear search before submitting form
    $('#projects_table').DataTable().search('').draw(false);

    toggleCheckboxUser();

    var user_ids = $('input[name=user_id]:checked').map(function() {
        return $(this).val();
    }).get();

    var ajax_result = ajaxHandler('/admin/projects/validate_add_edit', {
          name               : $('#name').val(),
          old_project_code   : $('#old_project_code').val(),
          project_code       : $('#project_code').val(),
          is_allmembers      : is_allmembers,
          user_id            : user_ids,
          action             : 'edit'
        });

    ajax_result.then(function(response) {
      // Reset error message div and span    
      $('.help-block').css('display', 'none');
      var keys = ['name', 'project_code', 'is_allmembers'];
      keys.forEach(value => {
        $('#error_' + value).text('').css('display', 'none');
        $('#' + value + '_div').removeClass('has-error');
      });

      if (response.status === true) {
        $('#confirmation_modal').modal('show');
        $('.modal-message').text("<?= __('Are you sure you want to change this project?'); ?>");

      } else {
        jQuery.each(response.message, function(index, item) {
          if (response.message[index] === "<?= 'Please select a member'; ?>") {
              $("#errorMsg").html(response.message[index]);
              $("#errorModal").modal('show');
          } else {
              $('#error_' + index).text(response.message[index]).css('display', 'inline-block');
              $('#' + index + '_div').addClass('has-error');
          }
        });
      }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });    
  });

  // Save if status = true
  $(document).delegate(".submitForm", "click", function(){
    $('#confirmation_modal').modal('hide');

    toggleCheckboxUser();

    var user_ids = $('input[name=user_id]:checked').map(function() {
        return $(this).val();
    }).get();

    var ajax_result = ajaxHandler('/admin/projects/edit/<?= $projects['id']; ?>', {
          name              : $('#name').val(),
          project_code      : $('#project_code').val(),
          old_project_code  : $('#old_project_code').val(),
          is_allmembers     : is_allmembers,
          user_id           : user_ids,
          action            : 'edit'
        });    

    ajax_result.then(function(response) {
      if (response.status === true) {
        $('#old_project_code').val($('#project_code').val());
        $("#successModal").modal('show');
        $("#successMsg").html(response.message);

      } else {
        $("#errorModal").modal('show');
        $("#errorMsg").html(response.message);
      }
    }).catch(function(fromReject) {
      $("#errorMsg").html("<?= 'Something went wrong'; ?>");
      $("#errorModal").modal('show');
    });
  });

  // Reset form
  $("html").delegate("#resetBtn", "click", function(){
    $('#error_name, #error_project_code, #error_is_allmembers').text('').css('display', 'none');
    $('#name_div, #project_code_div, #is_allmembers_div').removeClass('has-error');
  });
</script>