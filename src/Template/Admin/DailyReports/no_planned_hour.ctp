<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo __('No Planned Hour'); ?></h3><br/>
        </div>
        <div class="box-body">

          <div class="container col-md-12">
            <div class="form-group col-md-6">
              <label for="project_name" class="col-sm-4 control-label">
                <?= __('Project Name') ?>
              </label>
              <div class="col-sm-8" id="name_div">
                <?=
                  $this->Form->control('project_id', [
                    'class'       =>'form-control',
                    'id'          => 'project_id_add',
                    'options'     => $projects,
                    'empty'       => ['0' => '---'],
                    'required'    => false,
                    'label'       => false
                  ]);
                ?>
                <span id="error_name" class="help-block text-red"></span>
              </div>
            </div>
            <div class="form-group col-md-6">
              <label for="member_name" class="col-sm-4 control-label">
                <?= __('Member Name') ?>
              </label>
              <div class="col-sm-8" id="member_name_div">
                <?=
                  $this->Form->control('member_name', [
                    'class'       =>'form-control',
                    'required'    => false,
                    'id'          => 'member_name',
                    'placeholder' => __('Member name'),
                    'label'       => false
                  ]);
                ?>
                <span id="error_project_code" class="help-block text-red"></span>
              </div>
            </div>
          </div>

          <div class="container col-md-12"><br/>
            <div>
              <h4 class="font-weight-bold"><?= __('Member'); ?></h4>
            </div>
            <ul class="list-inline clearfix" id="member_id">
              <?php foreach($users as $key => $value): ?>
              <li class="col-md-6">
                <div>
                  <div class="col-xs-1">
                    <input type="checkbox" class="minimal" value="<?= $value['id']; ?>">
                  </div>
                  <label class="col-xs-11 checkbox-label"><?= $value['name']; ?></label>
                </div>
              </li>
            <?php endforeach; ?>
            </ul>
            <div class="container col-md-12 calendar">
              <label class="col-xs-3">Selected Member:</label>
              <ul class="list-inline clearfix col-xs-9" id="selected-member">
              </ul>
            </div>
            <div class="container col-md-12 calendar">
            <br/>
              <div class="form-group col-md-5 calendar">
                <label for="project_name" class="col-sm-3 control-label">
                  <?= __('Milestone') ?>
                </label>
                <div class="col-sm-9" id="name_div">
                  <?=
                    $this->Form->control('milestone_id', [
                      'class'       =>'form-control',
                      'empty'       => ['0' => '----'],
                      'id'          => 'milestone_id_add',
                      'required'    => false,
                      'label'       => false
                    ]);
                  ?>
                  <span id="error_name" class="help-block text-red"></span>
                </div>
              </div>
            </div>
          </div>
          <!-- List of Users -->
          <div class="container col-md-12 data-table">
            <br/><br/>
            <table id="projects_table" class="table table-responsive table-striped table-bordered display projects_table" cellspacing="0" width="100%">
              <thead>
                <tr>
                 <th><?= __('Member'); ?></th>
                 <th><?= __('Project Name'); ?></th>
                 <th><?= __('Milestone'); ?></th>
                 <th><?= __('Task No.'); ?></th>
                 <th><?= __('Status'); ?></th>
                 <th><?= __('Total Actual Hours'); ?></th>
                 <th><?= __('Actions'); ?></th>
                </tr>
              </thead>
              <tbody id="data-result">
                <?php
                    foreach ($tasks as $task_key => $task):
                        if (!empty($task->daily_work_results)):
                            foreach ($task->daily_work_results as $key => $val): ?>
                                <tr>
                                    <td><?= (isset($val->user->name)) ? h($val->user->name) : ''; ?></td>
                                    <td><?= h($task->project->name); ?></td>
                                    <td><?= (!empty($task->milestone)) ? h($task->milestone->name) : ''; ?></td>
                                    <td><?= h($task->task_code); ?></td>
                                    <td><?= ($task->is_close == 1) ? 'Close' : 'Open'; ?></td>
                                    <td><?= h($val->duration) . ' h'; ?></td>
                                    <td><button type="button" onclick="ignoreTask(<?= $task->id ?>)" class="btn btn-danger btn-flat ignore-btn" data-toggle="modal" data-target="#ignore-modal">
                                        Ignore
                                    </button></td>
                                </tr>
                            <?php endforeach;
                        endif;
                    endforeach;
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="ignore-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">IGNORE TASK</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="hidden-task-id">
                <input type="hidden" id="hidden-row-index">
                <p>DO YOU WANT TO IGNORE THIS TASK?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="ignoreCompletely()">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<input type="hidden" value="1" id="estimated_hour">
<style type="text/css">
  .font-weight-bold {
    font-weight: bold;
  }
  .checkbox-label {
    background-color: #eee;
  }
  .li-border {
    display: inline-block;
    margin-right: 10px;
    border-left: 1px solid #000000;
    position: relative;
  }
  .li-border:first-child {
    border-left: none;
  }
  .remove {
    position: absolute;
    top: -10px;
    right: -5px;
    color: red;
  }
  .calendar {
    padding-left: 0;
  }
  .calendar label {
    padding-top: 6px;
  }
  td, th {
    text-align: center;
  }
  th {
    background: #b3b3ff;
  }
  .member-search-list {
    display: none;
  }
  .data-table {
    display: none;
  }
</style>
<script type="text/javascript" src="/js/common_ajax"></script>
<script type="text/javascript">

var oTable = $('#projects_table').DataTable({
        "paging":   false,
        "order": [[ 0, "desc" ]]
    });

$(function(){
    oTable;
    $('.data-table').css('display', 'block');
    var project_id   = '';
  var milestone_id = '';
  var start        = '' ;
  var end          = '';
  var member_id    = new Array();

  //dropdown searching
  $('#project_id_add').on('change', function(){
    var id          = $(this).attr("id");
    project_id      = $('#' + id).val();
    var url         = '/admin/daily-reports/getMemberAndMilestoneWithoutDuration';
    var mile_id     = (id === 'project_id_add') ? 'milestone_id_add' : 'milestone_id';
    var date        = new Date();
    var today       = date.getFullYear()+'-'+ (date.getMonth()+1) +'-'+date.getDate();
    date.setDate(date.getDate() - 6);
    var defaultDate = date.getFullYear()+'-'+ (date.getMonth()+1) +'-'+date.getDate();

    if (project_id === '') {
      $('#' + mile_id).empty().prepend("<option value='' selected='selected'>---</option>");
    } else {
      member_id = [];
      $('#member_id').empty();
      $('#member_name').val('');
      $('#date_to').val(today);
      $('#date_from').val(defaultDate);
      start = defaultDate;
      end   = today;
      filterSearch(url, project_id, member_id, milestone_id, start, end, mile_id);
    }
  });
  //for selectbox
  $('.select2').select2()

  //for textbox searching with autocomplete
  $('#member_name').on('focus', function() {
    $(this).autocomplete({
      autoFocus: true,
      source: function(request, response) {
        var url  = '/admin/daily-reports/suggestMember';
        var data = { name : request.term, project_id : project_id };

        suggestFields(url, data, response);
      },
      select: function(event, ui) {
        event.preventDefault();
        var name    = ui.item.value;
        var value   = ui.item.data
        var checked = "";

        if ($('#selected-'+value).length != 0) {
          checked = "checked";
        }

        $('#member_id').empty();
        $('#member_id')
        .append(
          "<li class='col-md-6'><div><div class='col-xs-1'><input type='checkbox' value="+value+" "+checked+"></div><label class='col-xs-11 checkbox-label'>"+name+"</label></div></li>"
        );
        $(this).val(name);
      }
    });
  });

  //check member search if null
  $('#member_name').keypress(function(e) {
    if (e.keyCode == 13) {
      if ($(this).val() == '') {
        $('ul#member_id li').remove();
        filterSearch('/admin/daily-reports/getMemberAndMilestoneWithoutDuration', project_id, member_id, milestone_id, start, end,"", true);
      }
    }
  });

  //for checkbox searching
  $('body').delegate('input[type=checkbox]','click', function() {
    var url  = '/admin/daily-reports/getMemberAndMilestoneWithoutDuration';
    var data = $(this).val();
    var i    = member_id.indexOf(data);
    var name = $(this).parent().next().text();

    if ($(this).prop('checked') == true) {
      member_id.push(data);
    } else {
      if(i != -1) {
        member_id.splice(i, 1);
      }

      $('#selected-'+data).parent().remove();
    }
    filterSearch(url, project_id, member_id, milestone_id, start, end);
  });

  //milestone dropdown search
  $('#milestone_id_add').on('change', function(){
    var url      = '/admin/daily-reports/getMemberAndMilestoneWithoutDuration';
    milestone_id = $(this).val();

    filterSearch(url, project_id, member_id, milestone_id, start, end);
  });
});



/**
 * This function is responsible for filtering search
 * @param  {[type]}  url          [function url]
 * @param  {String}  project_id   [project_idd]
 * @param  {String}  member_id    [member_id]
 * @param  {String}  milestone_id [milestone_id]
 * @param  {String}  date_from    [date_from]
 * @param  {String}  date_to      [date_to]
 * @param  {String}  mile_id      [mile_id]
 * @param  {Boolean} blank        [blank means if the search in textbox member is blank]
 */
function filterSearch (url, project_id='', member_id='', milestone_id='',
    date_from='', date_to='', mile_id='', blank=false) {
    var estimate_times = $('#estimated_hour').val();
    var status         = {0:'Open', 1:'Closed'};

    $('.data-table').css('display', 'block');
    //remove error message
    $.fn.dataTable.ext.errMode = 'none';
    //check if dataTable is exist and destroy
    if ( $.fn.DataTable.isDataTable( '#projects_table' )) {
      $('#projects_table').DataTable({
        "processing" : true,
        "dom"        : 'B<"clear">lfrtip',
        'order'      : [[ 0, 'desc' ]],
        'searching'  : false,
        "buttons"    : [{
          extend: 'collection',
          text: 'Export',
          buttons: ['copy', 'excel', 'csv', 'pdf', 'print']
        }],
        "stateSave": false,
        "stateDuration": 60*60*24*365,
        "displayLength": 100,
        "drawCallback": function ( settings ) {
          var api          = this.api();
          var rows         = api.rows( {page:'current'} ).nodes();
          var last         = null;
          var colonne      = "";
          if (typeof api.row(0).data() !== "undefined") {
            colonne      = api.row(0).data().length;
          }
          var totale       = new Array();
          totale['Totale'] = new Array();
          var groupid      = -1;
          var subtotale    = new Array();
          var a            = 1;

          api.column(0, {page:'current'} ).data().each( function ( group, i ) {
            if ( last !== group ) {
              groupid++;
              $(rows).eq( i ).before(
                '<tr class="group"><td>Total Hour:</td></tr>'
              );
              last = group;
            }
            val = api.row(api.row($(rows).eq( i )).index()).data();      //current order index
            $.each(val,function(index2,val2) {
              if (index2 === 7) {
                if (typeof subtotale[groupid] =='undefined'){
                  subtotale[groupid] = new Array();
                }
                if (typeof subtotale[groupid][index2] =='undefined'){
                  subtotale[groupid][index2] = 0;
                }
                if (typeof totale['Totale'][index2] =='undefined') {
                  totale['Totale'][index2] = 0;
                }
                valore = Number(val2.replace('h',""));
                subtotale[groupid][index2] += valore;
                totale['Totale'][index2] += valore;
              }
            });
          });
          $('tbody').find('.group').each(function (i,v) {
            var rowCount = $(this).nextUntil('.group').length;
            var subtd = '';
            for (a = 1; a <= colonne; a++) {
                if (a < 8) {
                    if (totale['Totale'][a] == null) {
                        subtd += '<td></td>'
                    } else {
                        subtd += '<td>'+subtotale[i][a]+'hrs</td>';
                    }
                }
            }
            $(this).append(subtd);
          });
        }
      }).destroy();
    }

    var ajax_result = ajaxHandler(url, {
      project_id     : project_id,
      member_id      : (blank) ? '' : member_id,
      milestone_id   : milestone_id,
      date_from      : date_from,
      date_to        : date_to,
      estimate_times : (estimate_times) ? estimate_times : ''
    });
    ajax_result.then(function(response) {
      if (mile_id != '') {
        $('#' + mile_id).empty().prepend("<option value='' selected='selected'>---</option>");
        $.each(response['Milestones'], function(key, value) {
          $('#' + mile_id)
            .append($("<option></option>")
            .attr("value",key)
            .text(value));
        });
      }
      if ($('ul#member_id li').length >= 1) {
        if (member_id == '') {
          $('#member_id').empty();
          $.each(response['Members'], function(key, value) {
            $('#member_id')
              .append(
                "<li class='col-md-6'><div><div class='col-xs-1'><input type='checkbox' value="+key+" ></div><label class='col-xs-11 checkbox-label'>"+value+"</label></div></li>"
              );
          });
        }
      } else {
        $('#member_id').empty();
          $.each(response['Members'], function(key, value) {
            $('#member_id')
              .append(
                "<li class='col-md-6'><div><div class='col-xs-1'><input type='checkbox' value="+key+" "+idExist(key, member_id)+"></div><label class='col-xs-11 checkbox-label'>"+value+"</label></div></li>"
              );
          });
      }
      $('#data-result').empty();
      $.each(response['Summary'].result, function(key, value) {
        var milestone = (value.task.milestone_id) ? response['Milestones'][value.task.milestone_id] : '';
        $('#data-result')
        .append('<tr><td>'+value.user.name+'</td><td>'+value.task.project.name+'</td><td>'+milestone+'</td><td>'+value.task.task_code+'</td><td>'+status[value.task.is_close]+'</td><td>'+ value.duration +' h</td><td>'+'<button type="button" onclick="ignoreTask('+value.task.id+')" class="btn btn-danger btn-flat ignore-btn" data-toggle="modal" data-target="#ignore-modal">Ignore</button>'+'</td></tr>');
      });

      $('#projects_table').DataTable({
        "processing" : true,
        "paging"     : false,
        "dom"        : 'B<"clear">lfrtip',
        'order'      : [[ 0, 'desc' ]],
        "buttons"    : [{
          extend: 'collection',
          text: 'Export',
          buttons: [
            'copy',
            'excel',
            'csv',
            'pdf',
            'print'
          ]
        }],
        "stateSave": false,
        "stateDuration": 60*60*24*365,
        drawCallback: function ( settings ) {
          var api          = this.api();
          var rows         = api.rows( {page:'current'} ).nodes();
          var last         = null;
          var colonne      = "";
          if (typeof api.row(0).data() !== "undefined") {
            colonne      = api.row(0).data().length;
          }
          var totale       = new Array();
          totale['Totale'] = new Array();
          var groupid      = -1;
          var subtotale    = new Array();
          var a            = 1;

          api.column(0, {page:'current'} ).data().each( function ( group, i ) {
            val = api.row(api.row($(rows).eq( i )).index()).data();      //current order index
            $.each(val,function(index2,val2) {
              if (index2 === 7) {
                if (typeof subtotale[groupid] =='undefined'){
                  subtotale[groupid] = new Array();
                }
                if (typeof subtotale[groupid][index2] =='undefined'){
                  subtotale[groupid][index2] = 0;
                }
                if (typeof totale['Totale'][index2] =='undefined') {
                  totale['Totale'][index2] = 0;
                }
                valore = Number(val2.replace('h',""));
                subtotale[groupid][index2] += valore;
                totale['Totale'][index2] += valore;
              }
            });
          });
          $('tbody').find('.group').each(function (i,v) {
            var rowCount = $(this).nextUntil('.group').length;
            var subtd = '';
            for (a = 1; a <= colonne; a++) {
                if (a < 8) {
                    if (totale['Totale'][a] == null) {
                        subtd += '<td></td>'
                    } else {
                        subtd += '<td>'+subtotale[i][a]+'hrs</td>';
                    }
                }
            }
            $(this).append(subtd);
          });
        }
      });
    });
  }
/**
 * Check if the id is exist in a array
 */
function idExist(id, array) {
  var checked = "";
  $.each(array, function(k, v) {
    if(id == v) {
      checked = "checked";
    }
  });
  return checked;
}

$('body').delegate('.ignore-btn', 'click', function() {
    let row_index = $('#hidden-row-index').val($(this).closest('tr').index() + 1);
});

function ignoreTask(id) {
    let task_id = $('#hidden-task-id').val(id);
}

function ignoreCompletely() {
    let task_id = $("#hidden-task-id").val()
    $("tr").eq($('#hidden-row-index').val()).children('td, th')
    .animate({ padding: 0 })
    .wrapInner('<div />')
    .children()
    .slideUp(function() { $(this).closest('tr').remove(); });;
    console.log(task_id);
    $.ajax({
        url: "/admin/daily-reports/ignore",
        type: "POST",
        data: {
            task_id: task_id
        }
    });
}

</script>
