<div class="content-wrapper">
<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><?= __('Daily Reports') ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-striped" id="daily_report_tbl">
                    <thead>
                      <tr>
                          <th scope="col"><?= __('Business Date'); ?></th>
                          <th scope="col"><?= __('Member'); ?></th>
                          <th scope="col"><?= __('Created'); ?></th>
                          <th scope="col"><?= __('Modified'); ?></th>
                          <th scope="col" class="actions"><?= __('Action'); ?></th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dailyReports as $dailyReport): ?>
                        <tr>
                            <td><?= h($dailyReport->business_date); ?></td>
                            <td><?= h($dailyReport->user->name); ?></td>
                            <td><?= h($dailyReport->created->i18nFormat('yyyy-MM-dd HH:mm:ss')); ?></td>
                            <td><?= h($dailyReport->modified->i18nFormat('yyyy-MM-dd HH:mm:ss')); ?></td>
                            <td>
                              <a  href='/admin/users/task/<?= h($dailyReport->user_id).'/'.$dailyReport->business_date; ?>'>
                                <button class='btn-success'>
                                  <?= __('View'); ?>
                                </button>
                              </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    </div>
</section>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#daily_report_tbl').DataTable({'order': [[ 0, 'desc' ]]});
    $('.active-report, .active-report-list').addClass('active');
  });
</script>
