<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo __('Summary List'); ?></h3><br/>
        </div>
        <div class="box-body">

          <div class="container col-md-12">
            <div class="form-group col-md-6">
              <label for="project_name" class="col-sm-4 control-label">
                <?= __('Project Name') ?>
              </label>
              <div class="col-sm-8" id="name_div">
                <?=
                  $this->Form->control('project_id', [
                    'class'       =>'form-control',
                    'id'          => 'project_id_add',
                    'options'     => $projects,
                    'empty'       => ['0' => '---'],
                    'required'    => false,
                    'label'       => false
                  ]);
                ?>
                <span id="error_name" class="help-block text-red"></span>
              </div>
            </div>
            <div class="form-group col-md-6">
              <label for="member_name" class="col-sm-4 control-label">
                <?= __('Member Name') ?>
              </label>
              <div class="col-sm-8" id="member_name_div">
                <?=
                  $this->Form->control('member_name', [
                    'class'       =>'form-control',
                    'required'    => false,
                    'id'          => 'member_name',
                    'placeholder' => __('Member name'),
                    'label'       => false
                  ]);
                ?>
                <span id="error_project_code" class="help-block text-red"></span>
              </div>
            </div>
          </div>

          <div class="container col-md-12"><br/>
            <div>
              <h4 class="font-weight-bold"><?= __('Member'); ?></h4>
            </div>
            <ul class="list-inline clearfix" id="member_id">
              <?php foreach($users as $key => $value): ?>
              <li class="col-md-6">
                <div>
                  <div class="col-xs-1">
                    <input type="checkbox" value="<?= $value['id']; ?>">
                  </div>
                  <label class="col-xs-11 checkbox-label"><?= $value['name']; ?></label>
                </div>
              </li>
            <?php endforeach; ?>
            </ul>
            <div class="container col-md-12 calendar">
              <br/>
              <div class="form-group col-md-5 calendar">
                <label for="project_name" class="col-sm-3 control-label">
                  <?= __('Duration') ?>
                </label>
                <div class="col-sm-9" id="name_div">
                  <?=
                    $this->Form->control('date_from', [
                      'class'       =>'form-control',
                      'id'          => 'date_from',
                      'required'    => false,
                      'label'       => false,
                      'placeholder' => __('From')
                    ]);
                  ?>
                  <span id="error_name" class="help-block text-red"></span>
                </div>
              </div>
              <div class="form-group col-md-5 calendar">
                <div class="col-sm-9" id="name_div">
                  <?=
                    $this->Form->control('date_to', [
                      'class'       =>'form-control',
                      'id'          => 'date_to',
                      'required'    => false,
                      'label'       => false,
                      'placeholder' => __('To')
                    ]);
                  ?>
                  <span id="error_name" class="help-block text-red"></span>
                </div>
              </div>
              <div class="form-group col-md-5 calendar">
                <label for="project_name" class="col-sm-3 control-label">
                  <?= __('Milestone') ?>
                </label>
                <div class="col-sm-9" id="name_div">
                  <?=
                    $this->Form->control('milestone_id', [
                      'class'       =>'form-control',
                      'empty'       => ['0' => '----'],
                      'id'          => 'milestone_id_add',
                      'required'    => false,
                      'label'       => false
                    ]);
                  ?>
                  <span id="error_name" class="help-block text-red"></span>
                </div>
              </div>
            </div>
            <div id="no_planned_hour_title">
              <h4 class="font-weight-bold"><?= __("Miss Registration Dates"); ?></h4>
            </div>
            <div class="col-xs-12" id="no_planned_hour"></div>
          </div>

          <!-- List of Users -->
          <div class="container col-md-12 data-table">
            <br/><br/>
            <table id="projects_table" class="table table-responsive table-striped table-bordered display projects_table" cellspacing="0" width="100%">
              <thead>
                <tr>
                 <th><?= __('Member'); ?></th>
                 <th><?= __('Project Name'); ?></th>
                 <th><?= __('Milestone'); ?></th>
                 <th><?= __('Task No.'); ?></th>
                 <th><?= __('Planned Hours'); ?></th>
                 <th><?= __('Total Actual Hours'); ?></th>
                 <th><?= __('Status'); ?></th>
                 <th><?= __('Weekly Actual Hours'); ?></th>
                </tr>
              </thead>
              <tbody id="data-result">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<style type="text/css">
    #no_planned_hour_title {
        display: none;
    }
    .summary-list-title-container {
        padding-bottom: 20px !important;
    }
    .summary-list-title {
        display: block;
        font-weight: 700;
        padding: 10px 0;
    }
  .font-weight-bold {
    font-weight: bold;
  }
  .checkbox-label {
    background-color: #eee;
  }
  .li-border {
    display: inline-block;
    margin-right: 10px;
    border-left: 1px solid #000000;
    position: relative;
  }
  .li-border:first-child {
    border-left: none;
  }
  .remove {
    position: absolute;
    top: -10px;
    right: -5px;
    color: red;
  }
  .calendar {
    padding-left: 0;
  }
  .calendar label {
    padding-top: 6px;
  }
  td, th {
    text-align: center;
  }
  th {
    background: #b3b3ff;
  }
  .member-search-list {
    display: none;
  }
  .data-table {
    display: none;
  }
  tr.group {
    background-color: cyan !important;
  }
</style>
<script type="text/javascript" src="/js/common_ajax"></script>
<script type="text/javascript" src="/js/admin"></script>
