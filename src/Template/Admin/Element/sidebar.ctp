<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel header">
            <br/>
        </div>

        <ul class="sidebar-menu">
            <li class="active-users treeview">
              <a href="#">
                <i class="fa fa-user-circle text-aqua"></i> <span><?= __('Users'); ?></span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <!-- class="active" // for make it highlight -->
                <li class="active-users-list"><a href="/admin/users"><i class="fa fa-circle-o"></i><?= __('Users List'); ?></a></li>
                <li class="active-users-miss-register"><a href="/admin/users/miss_registration"><i class="fa fa-circle-o"></i><?= __('Miss Registration'); ?></a></li>
              </ul>
            </li>
            <li class="active-tasks treeview">
              <a href="#">
                <i class="fa fa-thumb-tack text-aqua"></i> <span><?= __('Tasks'); ?></span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <!-- class="active" // for make it highlight -->
                <li class="active-tasks-list"><a href="/admin/tasks"><i class="fa fa-circle-o"></i><?= __('Tasks List'); ?></a></li>
                <li class="active-tasks-add"><a href="/admin/tasks/add"><i class="fa fa-circle-o"></i><?= __('Add Task'); ?></a></li>
                <li class="active-report-list"><a href="/admin/daily-reports/no_planned_hour"><i class="fa fa-circle-o"></i><?= __('No Planned Hour'); ?></a></li>
                 <li class="active-tasks-add_backlog_issue"><a href="/admin/tasks/add_backlog_issue"><i class="fa fa-circle-o"></i><?= __('Add Issues'); ?></a></li>
              </ul>
            </li>
            <li class="active-projects treeview">
              <a href="#">
                <i class="fa fa-briefcase text-aqua"></i> <span><?= __('Projects'); ?></span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <!-- class="active" // for make it highlight -->
                <li class="active-projects-list"><a href="/admin/projects"><i class="fa fa-circle-o"></i><?= __('Projects List'); ?></a></li>
                <li class="active-projects-add"><a href="/admin/projects/add"><i class="fa fa-circle-o"></i><?= __('Add Project'); ?></a></li>
              </ul>
            </li>
            <li class="active-report treeview">
              <a href="#">
                <i class="fa fa-book text-aqua"></i> <span><?= __('Daily Reports'); ?></span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <!-- class="active" // for make it highlight -->
                <li class="active-report-list"><a href="/admin/daily-reports"><i class="fa fa-circle-o"></i><?= __('Report List'); ?></a></li>
              </ul>
            </li>
            <li class="active-report treeview">
              <a href="#">
                <i class="fa fa-book text-aqua"></i> <span><?= __('Summary Reports'); ?></span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <!-- class="active" // for make it highlight -->
                <li class="active-report-list"><a href="/admin/daily-reports/summary_list"><i class="fa fa-circle-o"></i><?= __('Summary List'); ?></a></li>
                <li class="active-report-list"><a href="/admin/monthly-reports/summary_list"><i class="fa fa-circle-o"></i><?= __('Summary per month'); ?></a></li>
              </ul>
            </li>
        </ul>
    </section>
</aside>
<!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
