  <header class="main-header">
    <a href="#" class="logo">
      <span class="logo-mini"><b><?= __('YNS'); ?></b></span>
      <span class="logo-lg"><b><?= __('YNS '); ?></b><?= __('Admin'); ?></span>
    </a>

    <nav class="navbar navbar-static-top">
      <?php if ($this->request->session()->read('Auth.User.id')): ?>
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only"><?= __('Toggle navigation'); ?></span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle">
                <span class="hidden-xs"><?= __('Hi ') . h($this->request->session()->read('Auth.User.name')); ?></span>
              </a>
            </li>

            <li>
              <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-gears"></i></a>
              <span class="dropdown-menu">
                  <a href="#" class="btn btn-default btn-flat pull-right" data-target="#logoutModal" data-toggle="modal"><?= __('Logout'); ?></a>
              </span>
            </li>
          </ul>
        </div>
      <?php endif; ?>
    </nav>
  </header>

  <!-- Logout Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id="logoutModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content tms__modal">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= __('Logout confirmation'); ?></h4>
        </div>

        <div class="modal-body">
          <h5><?= __('Are you sure you want to logout?'); ?></h5>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= __('Close'); ?></button>
          <a href="/admin/users/logout">
            <button type="button" class="btn btn-danger"><?= __('Yes'); ?></button>
          </a>
        </div>
      </div>
    </div>
  </div>