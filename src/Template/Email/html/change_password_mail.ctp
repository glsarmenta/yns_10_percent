<p><?= __('Dear ' . h($user_name) .","); ?> </p>

<?php 
	$link = $this->Html->link('here', [
		'controller' => 'Users', 
		'action'     => 'ForgotPasswordInput',
		$user_id
	], 
	[
		'fullBase'   => true
	]); ?>

<p><?= __('We received a request to access your YNS Management System Account through your email address. Your verification code is:'); ?></p>

<center>
	<h2><?= h($code); ?></h2>
</center>

<p>
	<?= __('If you did not request this code, it is possible that someone else is trying to access your account.'); ?> 
	<b><?= __('Do not forward or give this code to anyone.'); ?></b>
</p>

<p><?= __('Sincerely yours,'); ?></p>

<p><?= __('The YNS Team'); ?></p>