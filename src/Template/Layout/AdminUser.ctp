<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= __('Admin: Task Management System'); ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="/css/dist/img/tms-yns/yns-icon.png" />

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Select 2 -->
    <link rel="stylesheet" href="/css/bower_components/select2/dist/css/select2.min.css">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/css/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/css/dist/css/skins/_all-skins.min.css">
    <!-- jQuery 3 -->
    <script src="/js/jquery-3.2.1.js"></script>

    <!-- Datepicker -->
    <link rel="stylesheet" href="/js/datepicker/css/datepicker.css">
    <script src="/js/datepicker/js/bootstrap-datepicker.js"></script>

    <!-- DataTable -->
    <link rel="stylesheet" href="/DataTables/datatables.css">
    <script src="/DataTables/datatables.js"></script>

    <style type="text/css">
        .content, .content-wrapper {
            background-color: #ffffff !important;
        }
        .box-primary {
            -webkit-box-shadow: 0px 3px 5px 3px rgba(191,191,191,0.5);
            -moz-box-shadow: 0px 3px 5px 3px rgba(191,191,191,0.5);
            box-shadow: 0px 3px 5px 3px rgba(191,191,191,0.5);
            padding: 15px;
        }

        .chartWrapper {
          position: relative;
        }

        .chartWrapper > canvas {
          position: absolute;
          left: 0;
          top: 0;
          pointer-events: none;
        }

        .chartAreaWrapper {
          width: 100%;
          height: 100% !important;
          overflow-x: scroll;
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <?= $this->element('header'); ?>

    <?= $this->Flash->render() ?>

    <?php if ($this->request->session()->read('Auth.User.id')): ?>
        <?= $this->element('sidebar'); ?>
    <?php endif; ?>

    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>

    <!-- jQuery UI -->
    <script src="/js/jquery-ui.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/css/dist/js/app.min.js"></script>

    <!-- Full Calendar -->
    <link rel="stylesheet" href="/css/bower_components/fullcalendar/dist/fullcalendar.css">
    <script src="/css/bower_components/moment/moment.js"></script>
    <script src="/css/bower_components/fullcalendar/dist/fullcalendar.js"></script>
    <script src="/css/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!-- Chart JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

    <?= $this->Html->script(['/js/ajax_handler.js?'.time()]); ?>
    <?= $this->element('Common/success'); ?>
    <?= $this->element('Common/error'); ?>
    <?= $this->element('Common/loading_modal'); ?>
    <?= $this->element('Common/confirmation_modal'); ?>
</body>
</html>
