<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= __('Task Management System'); ?></title>
  <link rel="icon" href="/css/dist/img/tms-yns/yns-icon.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/css/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/css/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/css/dist/css/AdminLTE.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="/css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="/css/bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- jQuery 3 -->
  <script src="/css/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="/css/bower_components/jquery-ui/jquery-ui.min.js"></script>

  <!-- Bootstrap 3.3.7 -->
  <script src="/css/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <!-- Bootstrap WYSIHTML5 -->
  <script src="/css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

  <!-- AdminLTE App -->
  <script src="/css/dist/js/adminlte.min.js"></script>

  <script src="/css/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>

  <style type="text/css">
    .tms {
        padding-right: 0 !important;
    }
    .padding_right20 {
      padding-right:20px;
    }
  </style>
</head>

<body class="tms">
    <header>
      <div class="tms__nav">
        <h1>
          <a href="#">
            <img src ="/css/dist/img/tms-yns/yns-logo.png" alt=""/>
          </a>
        </h1>
        <?php if ($this->request->session()->read('Auth.User.id')): ?>
          <a href="#" class="pull-right padding_right20" data-target="#logoutModal" data-toggle="modal" >
            <b><?= __('Logout'); ?></b>
          </a>
          <span class="pull-right padding_right20">
            <?= __('Hi'); ?>
            <b><?= h($this->request->session()->read('Auth.User.name')); ?></b>
          </span>
        <?php endif; ?>
      </div>
    </header>

    <!-- Logout Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="logoutModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content tms__modal">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?= __('Logout confirmation'); ?></h4>
          </div>

          <div class="modal-body">
            <h5><?= __('Are you sure you want to logout?'); ?></h5>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= __('Close'); ?></button>
            <a href="/users/logout">
              <button type="button" class="btn btn-danger"><?= __('Yes'); ?></button>
            </a>
          </div>
        </div>
      </div>
    </div>

    <?= $this->fetch('content') ?>

    <?= $this->Html->script(['/js/jquery-ui.js']); ?>
    <?= $this->Html->script(['/js/ajax_handler.js']); ?>
    <?= $this->Html->script(['/js/common_ajax.js']); ?>

    <?= $this->element('Common/success'); ?>
    <?= $this->element('Common/error'); ?>
    <?= $this->element('Common/loading_modal'); ?>
    <?= $this->element('Common/confirmation_modal'); ?>
</body>
</html>
