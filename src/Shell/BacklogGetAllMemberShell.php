<?php
namespace App\Shell;

use App\Controller\Component\BacklogComponent;
use Cake\Console\Shell;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

class BacklogGetAllMemberShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
        $this->Backlog = new BacklogComponent(new ComponentRegistry(), []);
        $this->loadModel('Users');
        $this->out('Begin: Start Batch');
    }

    public function main()
    {
        $members = $this->Backlog->getAllCurrentUsers();
        $this->data_counter = 0;

        // Saving Users
        if (!empty($members)) {
            $this->data = [];

            $query = $this->Users->find('list');
            $query = $query->toArray();

            foreach ($members as $key => $value) {

                // If not exist save password same as name and updated_password = 0
                if (!isset($query[$value['id']])) {
                    $this->data[] = [
                        'id'                => $value['id'],
                        'name'              => $value['name'],
                        'role'              => $value['roleType'],
                        'email'             => $value['mailAddress'],
                        'password'          => $value['name'],
                        'updated_password'  => 0,
                        'is_enable'         => 1,
                    ];
                }
            }

            // Initiating Users Table
            $this->user = TableRegistry::get('Users');

            // Transaction process
            $this->user->connection()->transactional(function (&$data) {
                foreach ($this->data as $set) {
                    //validaton is being applied there
                    $entity = $this->user->newEntity($set, [
                        'validate' => false,
                        'accessibleFields' => ['id' => true]
                    ]);

                    // If not save transaction rollback
                    if (!$this->user->save($entity, ['checkRules' => false,])) {
                        $this->out('End: Error saving exiting');
                        return false;
                    }
                    $this->data_counter++;
                }

                return true;
            });
        }
        $this->out('End: Success inserted/updated '.$this->data_counter);
    }
}
