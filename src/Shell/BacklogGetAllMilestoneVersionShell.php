<?php
namespace App\Shell;

use App\Controller\Component\BacklogComponent;
use Cake\Console\Shell;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

class BacklogGetAllMilestoneVersionShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
        $this->Backlog = new BacklogComponent(new ComponentRegistry(), []);
        $this->out('Begin: Start Batch');
    }

    /**
     * Main function
     * Get all backlog milestone/version
     *
     * @param  $project_id int
     * @return void
     */
    public function main()
    {
        // Get all latest projects with limit 5
        $this->Projects = TableRegistry::get('Projects');
        $project_ids = $this->Projects->find(
            'list', [
                'valueField' => 'id',
                'order' => ['id' => 'DESC']
            ])
            ->where([
                'in_backlog' => 1
            ])
            ->toArray();

        // Get all milestone/version by project_id
        $milestone_version = [];
        foreach ($project_ids as $key => $value) {
            // Project 'FoodieFinder Marketing' do not have owner in backlog
            if ($value != 65997) {
                $data = $this->Backlog->getAllMilestoneVersion($value);
                if (!empty($data)) {
                    $milestone_version = array_merge($milestone_version, $data);
                }
            }
        }

        $this->data_counter = 0;
        if (!empty($milestone_version)) {
            $this->data = [];

            // filter milestone_version array for saving in milestones
            foreach ($milestone_version as $key => $value) {
                if ($value['releaseDueDate'] != '') {
                    $datetime1 = strtotime($value['startDate']);
                    $datetime2 = strtotime($value['releaseDueDate']);
                    $total_estimate_time = ($datetime2 - $datetime1) / 3600;
                } else {
                    $total_estimate_time = 0;
                }

                $this->data[] = [
                        'id'                  => $value['id'],
                        'project_id'          => $value['projectId'],
                        'name'                => $value['name'],
                        'total_estimate_time' => $total_estimate_time
                  ];
            }

            // Initiating Milestones Table
            $this->milestones = TableRegistry::get('Milestones');

            // Transaction process
            $this->milestones->connection()->transactional(
                function () {
                    foreach ($this->data as $set) {
                        //validaton is being applied there
                        $entity = $this->milestones->newEntity(
                            $set, [
                            'validate' => false,
                            'accessibleFields' => ['id' => true]
                            ]
                        );
                        // If not save transaction rollback
                        if (!$this->milestones->save($entity, ['checkRules' => false])) {
                            $this->data_counter = 0;
                            $this->out('End: Error saving exiting');
                            return false;
                        }
                        $this->data_counter++;
                    }
                    return true;
                }
            );
        }
        $this->out('End: Success inserted/updated '.$this->data_counter);
    }
}
