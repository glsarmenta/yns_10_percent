<?php
namespace App\Shell;

use App\Controller\Component\BacklogComponent;
use Cake\Console\Shell;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

class UpdateMissRegistrationTableShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
        $this->Backlog = new BacklogComponent(new ComponentRegistry(), []);
        $this->out('Begin: Start Batch');

    }

    public function main()
    {
    	$this->Users = TableRegistry::get('Users');
    	$this->MissRegistrationTasks = TableRegistry::get('MissRegistrationTasks');
        $this->MstHolidays = TableRegistry::get('MstHolidays');
    	$yesterday = date("Y-m-d", strtotime( '-1 days' ) );
        // is holiday
        $holiday = $this->MstHolidays->find('all')
            ->where([
                'date' => $yesterday,
            ])->toArray();
        
        if(count($holiday) > 0){
            $this->out('End: Yesterday is a holiday! 0 inserted data.');
        } else {
            $users = $this->Users->find('all')
                ->where([
                    'is_outsource'=> 0,
                    'role' => 2,
                    'is_enable' => 1,
                    "id NOT IN(SELECT user_id from daily_work_results b where b.work_date = '".$yesterday."')",
                ])
                ->toArray();
            $this->data_counter = 0;
            foreach ($users as $key => $value) {
                $data = $this->MissRegistrationTasks->newEntity();
                $data->user_id = $value->id;
                $data->miss_date = $yesterday;
                $this->MissRegistrationTasks->save($data);
                $this->data_counter++;
            }
            $this->out('End: Success inserted '.$this->data_counter);
        }
    }
}