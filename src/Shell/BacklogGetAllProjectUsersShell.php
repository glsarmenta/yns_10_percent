<?php
namespace App\Shell;

use App\Controller\Component\BacklogComponent;
use Cake\Console\Shell;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

class BacklogGetAllProjectUsersShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
        $this->Backlog = new BacklogComponent(new ComponentRegistry(), []);
        $this->out('Begin: Start Batch');
    }

    /**
     * Main function
     * Get all backlog users for project
     *
     * @param  $project_id int
     * @return void
     */
    public function main()
    {
        // Get all latest projects with limit 10
        $this->Projects = TableRegistry::get('Projects');
        $project_ids = $this->Projects->find(
            'list', [
                'valueField' => 'id',
                'order' => ['id' => 'DESC']
            ])
            ->where([
                'in_backlog' => 1
            ])
            ->toArray();
        $project_users = [];
        foreach ($project_ids as $key => $value) {
            // Project 'FoodieFinder Marketing' do not have owner in backlog
            if ($value != 65997) {
                $data = $this->Backlog->getAllProjectUsers($value);
                
                if (!empty($data)) {
                    foreach ($data as $k => $v) {
                        $data[$k]['projectId'] = $value;
                    }
                    $project_users = array_merge($project_users, $data);
                }
            }
        }

        $this->data_counter = 0;
        if (!empty($project_users)) {
            $this->data = [];

            foreach ($project_users as $key => $value) {
                $this->data[] = [
                        'id'         => $value['id']+$value['projectId'],
                        'project_id' => $value['projectId'],
                        'user_id'    => $value['id'],
                  ];
            }

            // Initiating ProjectUsers Table
            $this->ProjectUsers = TableRegistry::get('ProjectUsers');

            // Transaction process
            $this->ProjectUsers->connection()->transactional(
                function () {
                    foreach ($this->data as $set) {
                        //validaton is being applied there
                        $entity = $this->ProjectUsers->newEntity(
                            $set, [
                            'validate' => false,
                            'accessibleFields' => ['id' => true]
                            ]
                        );

                        // If not save transaction rollback
                        if (!$this->ProjectUsers->save($entity, ['checkRules' => false])) {
                            $this->data_counter = 0;
                            $this->out('End: Error saving exiting');
                            return false;
                        }

                        $this->data_counter++;
                    }
                    return true;
                }
            );
        }
        $this->out('End: Success inserted/updated '.$this->data_counter);
    }
}
