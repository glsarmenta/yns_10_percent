<?php
namespace App\Shell;

use App\Controller\Component\BacklogComponent;
use Cake\Console\Shell;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

class BacklogGetAllIssuesShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
        $this->Backlog = new BacklogComponent(new ComponentRegistry(), []);
        $this->out('Begin: Start Batch');
    }
    /**
    * Main function
    * Get all backlog issue
    * @param [int] $count limit of data
    * @param [date] $created_since date start created (yyyy-MM-dd)
    * @param [date] $created_until date end date (yyyy-MM-dd)
    * @param [int] $status [1 => Open, 2 => In Progress, 3 => Resolved, 4 => Closed]
    * @return void
    */

    public function main($count = null, $created_since = null, $created_until = null, $status = null)
    {
        $status_id = [1,2,3,4];
        $error     = false;
        //validation for count/number of items
        if (!empty($count) && $count > 100) {
            $this->out('Error: Count minimum is 1 up to 100');
            $error = true;
        }
        //validation for date format
        if (!empty($created_since) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$created_since)) {
            $this->out('Error: Created since must be yyyy-MM-dd');
            $error = true;
        }
        //validation for date format
        if (!empty($created_until) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$created_until)) {
            $this->out('Error: Created until must be yyyy-MM-dd');
            $error = true;
        }
        //validation for created since and created until
        if (!empty($created_since) && !empty($created_until) && ($created_since > $created_until)) {
            $this->out('Error: Created since must be less than Created until');
            $error = true;
        }
        //validation for status
        if (!empty($status) && !in_array($status, $status_id)) {
            $this->out('Error: Status must contain 1:Open, 2:In Progress, 3:Resolved, 4:Closed');
            $error = true;
        }
        if ($error) {
            return false;
        }
        // Get all issues by date if null $date will be current date in component
        $issues = $this->Backlog->getIssueByProjectAndUser(null, null, $count, $created_since, $created_until, $status);
        $this->data_counter = 0;
        if (!empty($issues)) {
            $this->data = [];
            // filter issues array for saving in task
            foreach ($issues as $key => $value) {
                $this->data[] = [
                                'id'                => $value['id'],
                                'project_id'        => $value['projectId'],
                                'milestone_id'      => !empty($value['milestone'])
                                                       ? $value['milestone'][0]['id']
                                                       : null,
                                'task_code'         => h($value['issueKey']),
                                'name'              => h($value['summary']),
                                'estimate_times'     => $value['estimatedHours'] == ''
                                                       ? 0
                                                       : $value['estimatedHours'],
                                'is_close'          => ( $value['status']['id'] == 1 ||
                                                         $value['status']['id'] == 2 ||
                                                         $value['status']['id'] == 3 )
                                                       ? 0
                                                       : 1
                          ];
            }
            // Initiating Tasks Table
            $this->task = TableRegistry::get('Tasks');
            // Transaction process
            $this->task->connection()->transactional(function () {
                foreach ($this->data as $set) {
                    //validaton is being applied there
                    $entity = $this->task->newEntity($set, [
                        'accessibleFields' => ['id' => true],
                        'validate'         => false
                    ]);
                    // If not save transaction rollback
                    if (!$this->task->save($entity)) {
                        $this->data_counter = 0;
                        $this->out('End: Error saving exiting');
                        return false;
                    }
                    $this->data_counter++;
                }
                return true;
            });
        }
        $this->out('End: Success inserted/updated '.$this->data_counter);
    }
}
