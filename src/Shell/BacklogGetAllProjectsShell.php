<?php
namespace App\Shell;

use App\Controller\Component\BacklogComponent;
use Cake\Console\Shell;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

class BacklogGetAllProjectsShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
        $this->Backlog = new BacklogComponent(new ComponentRegistry(), []);
        $this->out('Begin: Start Batch');
    }
    /**
    * Main function
    * Get all backlog projects
    * @return void
    */
    public function main()
    {
        $projects = $this->Backlog->getAllProjects();
        $this->data_counter = 0;
        if (!empty($projects)) {
            $this->data = [];
            // filter projects array for saving
            foreach ($projects as $key => $value) {
                $this->data[] = [
                                'id'                => $value['id'],
                                'name'              => $value['name'],
                                'project_code'      => $value['projectKey'],
                                'is_allmembers'     => 1,
                          ];
            }
            // Initiating Projects Table
            $this->project = TableRegistry::get('Projects');
            // Transaction process
            $this->project->connection()->transactional(function () {
                foreach ($this->data as $set) {
                    //validaton is being applied there
                    $entity = $this->project->newEntity($set, [
                        'accessibleFields' => ['id' => true]
                    ]);
                    // If not save transaction rollback
                    if (!$this->project->save($entity, ['checkRules' => false])) {
                        $this->data_counter = 0;
                        $this->out('End: Error saving exiting');
                        return false;
                    }
                    $this->data_counter++;
                }
                return true;
            });
        }
        $this->out('End: Success inserted/updated '.$this->data_counter);
    }
}
