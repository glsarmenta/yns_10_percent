<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class EditPasswordForm extends Form
{
    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('password', 'string')
            ->addField('confirm_password', 'string');
    }

    protected function _buildValidator(Validator $validator)
    {
        return $validator->notEmpty('password', __('Password is required'))
                         ->notEmpty('confirm_password', __('Password is required'))
                         ->add('password', [
                            'minLength' => [
                                'rule' => ['minLength', 8],
                                'last' => true,
                                'message' => 'Password must have minimum of 8 characters.'
                            ],
                         ])
                         ->add('confirm_password', [
                            'minLength' => [
                                'rule' => ['minLength', 8],
                                'last' => true,
                                'message' => 'Password must have minimum of 8 characters.'
                            ],
                         ])
                         // Inline Validation rule for same password
                         ->add('confirm_password', 'equal', [
                            'rule' => function ($value, $context) {
                                return $context['data']['password'] == $value;
                            },
                            'message' => 'Password and Confirm Password must be equal'
                        ]);
    }
    /**
    * _execute method
    * this is like last callback if the validations are true
    * we can send email or do something in this method
    */
    protected function _execute(array $data)
    {
        return true;
    }
}
