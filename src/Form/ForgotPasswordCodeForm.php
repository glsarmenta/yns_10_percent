<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class ForgotPasswordCodeForm extends Form
{
    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('code', 'string');
    }

    protected function _buildValidator(Validator $validator)
    {
        return $validator->notEmpty('code', __('Code is required'))
                         ->requirePresence('code');
    }
    /**
    * _execute method
    * this is like last callback if the validations are true
    * we can send email or do something in this method
    */
    protected function _execute(array $data)
    {
        return true;
    }
}
