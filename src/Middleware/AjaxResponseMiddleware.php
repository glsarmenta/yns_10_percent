<?php
namespace App\Middleware;

class AjaxResponseMiddleware
{
    public function __invoke($request, $response, $next)
    {
        // Calling $next() delegates control to the *next* middleware
        // In your application's queue.
        $response = $next($request, $response);
        if ($request->is('ajax') && $request->params['plugin'] != 'DebugKit') {
            // Make Response type equal to json
            $response->type('json');
        }

        return $response;
    }
}