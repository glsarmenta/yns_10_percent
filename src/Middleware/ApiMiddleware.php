<?php
// In src/Middleware/ApiMiddleware.php
namespace App\Middleware;
use Cake\Core\Configure;

class ApiMiddleware
{
    public function __invoke($request, $response, $next)
    {
        // this time Middle work first before the action of controller
        $response->type('json');
        if ($request->getHeaderLine('X-API-TOKEN') !== Configure::read('API_TOKEN')) {
            $response = $response->withHeader('Content-Type', 'application/json')
                ->withHeader('Pragma', 'no-cache')
                ->withStatus(401);

            // Write to the body
            $body = $response->getBody();
            $body->write(json_encode([
                'status' => '401',
                'message' => __('Authentication Failed')
            ]));
            return $response;
        }
        // Calling $next() delegates control to the *next* middleware
        // In your application's queue.
        $response = $next($request, $response);
        return $response;
    }
}