<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Task Entity
 *
 * @property int $id
 * @property int $project_id
 * @property int $milestone_id
 * @property string $task_code
 * @property string $name
 * @property float $estimate_times
 * @property float $total_actual_times
 * @property int $is_close
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $deleted
 * @property \Cake\I18n\FrozenTime $deleted_date
 *
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\Milestone $milestone
 * @property \App\Model\Entity\DailyWorkResult[] $daily_work_results
 */
class Task extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
