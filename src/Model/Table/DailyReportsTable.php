<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;
use Cake\Event\Event;
use ArrayObject;

/**
 * DailyReports Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\DailyReport get($primaryKey, $options = [])
 * @method \App\Model\Entity\DailyReport newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DailyReport[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DailyReport|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DailyReport patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DailyReport[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DailyReport findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DailyReportsTable extends Table
{
    use SoftDeleteTrait;
    protected $softDeleteField = 'deleted_date';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('daily_reports');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('comment')
            ->requirePresence('comment', 'create')
            ->notEmpty('comment');

        $validator
            ->scalar('start_time')
            ->requirePresence('start_time', 'create')
            ->notEmpty('start_time');

        $validator
            ->scalar('start_time')
            ->requirePresence('start_time', 'create')
            ->time('start_time');

        $validator
            ->scalar('end_time')
            ->requirePresence('end_time', 'create')
            ->notEmpty('end_time');

        $validator
            ->scalar('end_time')
            ->requirePresence('end_time', 'create')
            ->time('end_time');

        $validator
            ->add(
                'end_time',
                'notLess',
                [
                'message' => 'End time must be greater than start time',
                'rule' => function ($data, $provider) {
                    if (isset($provider['data']['start_time'])) {
                        if (strtotime($data) <= strtotime($provider['data']['start_time'])) {
                            return false;
                        }
                    }
                    return true;
                }]
            );

        $validator
            ->add(
                'total_actual_times',
                'validateTotalActualTime',
                [
                'message' => 'Total actual time is not acceptable',
                'rule' => function ($data, $provider) {
                    if (isset($provider['data']['total_work_time'])) {

                        // true if half-day or undertime; and total_actual_time is not greater than total_work_time
                        if ($provider['data']['total_work_time'] < 7.5
                            && $provider['data']['total_work_time'] >= $data) {
                            return true;

                            // Accepts 6hrs actual time if employee completed 7.5 hours or more
                        } elseif ($provider['data']['total_work_time'] >= 7.5) {
                            if ($data >= 6 && $provider['data']['total_work_time'] >= $data) {
                                return true;
                            }
                        }
                        return false;
                    }
                    return true;
                }]
            );

        $validator
            ->greaterThan('total_actual_times', 0, 'Total actual time must be greater than 0');

        $validator
            ->lessThanOrEqual('total_actual_times', 20, 'Total actual time should not exceed to 20hrs');

        $validator
            ->scalar('plan_for_tomorrow')
            ->requirePresence('plan_for_tomorrow', 'create')
            ->notEmpty('plan_for_tomorrow');

        $validator
            ->integer('deleted_date')
            ->allowEmpty('deleted_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        $data['comment']           = trim($data['comment']);
        $data['plan_for_tomorrow'] = trim($data['plan_for_tomorrow']);
    }
}
