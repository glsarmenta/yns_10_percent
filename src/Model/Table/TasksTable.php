<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Tasks Model
 *
 * @property \App\Model\Table\ProjectsTable|\Cake\ORM\Association\BelongsTo $Projects
 * @property \App\Model\Table\MilestonesTable|\Cake\ORM\Association\BelongsTo $Milestones
 * @property \App\Model\Table\DailyWorkResultsTable|\Cake\ORM\Association\HasMany $DailyWorkResults
 *
 * @method \App\Model\Entity\Task get($primaryKey, $options = [])
 * @method \App\Model\Entity\Task newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Task[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Task|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Task patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Task[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Task findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TasksTable extends Table
{
    use SoftDeleteTrait;
    protected $softDeleteField = 'deleted_date';

    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tasks');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
            ]
        );
        $this->belongsTo(
            'Milestones', [
            'foreignKey' => 'milestone_id',
            'joinType' => 'LEFT'
            ]
        );
        $this->hasMany(
            'DailyWorkResults', [
            'foreignKey' => 'task_id'
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param  \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('project_id')
            ->requirePresence('project_id', 'create')
            ->notEmpty('project_id');

        $validator
            ->scalar('task_code')
            ->requirePresence('task_code', 'create')
            ->notEmpty('task_code')
            ->add(
                'task_code', 
                [
                    'unique' => 
                    [
                        'rule'     => 'validateUnique', 
                        'provider' => 'table', 
                        'message'  => __('This value is already in used'),
                        'on'       => 'create'
                    ]
                ]
            );

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->numeric('estimate_times')
            ->requirePresence('estimate_times', 'create')
            ->notEmpty('estimate_times');

        $validator
            ->numeric('total_actual_times')
            ->requirePresence('total_actual_times', 'create')
            ->notEmpty('total_actual_times');

        $validator
            ->integer('is_close')
            ->requirePresence('is_close', 'create')
            ->notEmpty('is_close');

        $validator
            ->dateTime('deleted_date')
            ->allowEmpty('deleted_date');

        return $validator;
    }


    /**
     * Custom validation rules.
     * Responsible for removing isUnique validation if task_code is not changed
     *
     * @param  \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationCustom(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('project_id')
            ->requirePresence('project_id', 'create')
            ->notEmpty('project_id');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->numeric('estimate_times')
            ->requirePresence('estimate_times', 'create')
            ->notEmpty('estimate_times');

        $validator
            ->numeric('total_actual_times')
            ->requirePresence('total_actual_times', 'create')
            ->notEmpty('total_actual_times');

        $validator
            ->integer('is_close')
            ->requirePresence('is_close', 'create')
            ->notEmpty('is_close');

        $validator
            ->dateTime('deleted_date')
            ->allowEmpty('deleted_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {

        return $rules;
    }
}
