<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * DailyWorkResults Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TasksTable|\Cake\ORM\Association\BelongsTo $Tasks
 *
 * @method \App\Model\Entity\DailyWorkResult get($primaryKey, $options = [])
 * @method \App\Model\Entity\DailyWorkResult newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DailyWorkResult[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DailyWorkResult|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DailyWorkResult patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DailyWorkResult[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DailyWorkResult findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DailyWorkResultsTable extends Table
{
    use SoftDeleteTrait;
    protected $softDeleteField = 'deleted_date';
    
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('daily_work_results');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tasks', [
            'foreignKey' => 'task_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('actual_times')
            ->requirePresence('actual_times', 'create')
            ->notEmpty('actual_times');
            
        $validator
            ->add(
                'actual_times',
                'notLess',
                [
                'message' => 'Actual time must be greater than 0',
                'rule' => function ($data, $provider) {
                    if ($data <= 0) {
                        return false;
                    }
                    return true;
                }]
            );
            
        $validator
            ->lessThanOrEqual('actual_times', 20, 'Actual time should not exceed to 20hrs');

        $validator
            ->numeric('estimate_times')
            ->allowEmpty('estimate_times');

        $validator
            ->date('work_date')
            ->requirePresence('work_date', 'create')
            ->notEmpty('work_date');

        $validator
            ->dateTime('deleted_date')
            ->allowEmpty('deleted_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['task_id'], 'Tasks'));

        return $rules;
    }
}
