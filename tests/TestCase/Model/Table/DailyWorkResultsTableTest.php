<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DailyWorkResultsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DailyWorkResultsTable Test Case
 */
class DailyWorkResultsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DailyWorkResultsTable
     */
    public $DailyWorkResults;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.daily_work_results',
        'app.users',
        'app.daily_reports',
        'app.tasks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DailyWorkResults') ? [] : ['className' => DailyWorkResultsTable::class];
        $this->DailyWorkResults = TableRegistry::get('DailyWorkResults', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DailyWorkResults);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
