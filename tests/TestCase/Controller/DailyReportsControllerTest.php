<?php
namespace App\Test\TestCase\Controller;

use App\Controller\Admin\DailyReportsController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;
use PHPUnit\Framework\Exception;

/**
 * App\Controller\DailyReportsController Test Case
 */
class DailyReportsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.daily_reports',
        'app.users',
        'app.projects',
        'app.milestones',
        'app.daily_work_results',
        'app.tasks',
    ];

    /**
     * noPlannedHour method
     *
     * @author Kevin
     * Method : GET
     * URI    : /admin/daily-reports/no_planned_hour
     * for    : To check that all tasks that is passed to view is all ignored and estimate time = 0
     * @return void
     */
    public function testAllTaskIsIgnored()
    {
        // set session, because no planned hour cannot be accessed if no session
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'name' => 'kevin',
                    'is_enable' => 1,
                    'role' => 1,
                    'password_updated' => 1
                ]
            ]
        ]);

        // Get Action for no_planned_hour page
        $this->get('/admin/daily-reports/no_planned_hour');

        // array of expected tasks.id, condition is estimate times is 0, and is_ignored is 0
        $expected_tasks = [1,2,3];

        // Return contains tasks variable from get request
        $tasks = $this->viewVariable('tasks');

        //check every user.id is existing in the expected_users array
        foreach ($tasks as $k => $v) {
            $this->assertEquals(true, in_array($v->id, $expected_tasks));
        }
    }


    /**
     * noPlannedHour method
     *
     * @author Kevin
     * Method : GET
     * URI    : /admin/daily-reports/no_planned_hour
     * for    : To check that the users variable that is passed to view is not empty, and get all users that is not deleted
     * @return void
     */
    public function testNoPlannedHour()
    {
        // set session, because no planned hour cannot be accessed if no session
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'name' => 'kevin',
                    'is_enable' => 1,
                    'role' => 1,
                    'password_updated' => 1
                ]
            ]
        ]);

        // Get Action for no_planned_hour page
        $this->get('/admin/daily-reports/no_planned_hour');

        // array of expected users.id,
        $expected_users = [2,3,4,5];

        // Return contains user variable from get request
        $users = $this->viewVariable('users');

        //check every user.id is existing in the expected_users array
        foreach ($users as $k => $v) {
            $this->assertEquals(true, in_array($v->id, $expected_users));
        }
    }

    /**
     * noPlannedHour method
     *
     * @author Kevin
     * Method : GET
     * URI    : /admin/daily-reports/no_planned_hour
     * for    : To check if session is existing
     * @return void
     */
    public function testSession()
    {
        // Mocking of Session for authentication
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'name' => 'kevin',
                    'is_enable' => 1,
                    'role' => 1,
                    'password_updated' => 1
                ]
            ]
        ]);

        // Get Action for no_planned_hour page
        $this->get('/admin/daily-reports/no_planned_hour');

        // Check for a 2xx response code
        $this->assertResponseOk();

        // Session user id is 1, this test will return true
        $this->assertSession(1, 'Auth.User.id');
    }

    /**
     * @author Kevin
     * URI    : /admin/daily-reports/no_planned_hour
     * for    : To check if it will redirect to login page if no session
     * @return void
     */
    public function testIfNoSession() {
        // No session is set
        $this->get('/admin/daily-reports/no_planned_hour');

        // expect null because no session is set
        $this->assertSession(null, 'Auth');

        // no session, it will redirect to /users/login
        $this->assertRedirectContains('/users/login');
    }

    /**
     * ignore method
     *
     * @author Kevin
     * Method : POST
     * URI    : /admin/daily-reports/ignore
     * for    : To check if ignore button is working properly
     * @return void
     */
    public function testIgnoreButton1() {
        //load tasks table
        $this->Tasks = TableRegistry::get('tasks');

        $tasks_before_ignore = $this->Tasks->find('all')->where(['is_ignored' => 0]);

        // get count of all tasks that is not ignored before post request
        $tasks_before_ignore = $tasks_before_ignore->count();

        // Mocking of Session for authentication
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'name' => 'kevin',
                    'is_enable' => 1,
                    'role' => 1,
                    'password_updated' => 1
                ]
            ]
        ]);

        // set task_id for passing it to post request
        $data = ['task_id' => 1];

        // post request, and task_id to be ignored
        $this->post('/admin/daily-reports/ignore', $data);

        // Check for a 2xx response code
        $this->assertResponseOk();

        $ignored_task = $this->Tasks->find('all')
                        ->where(['id' => $data['task_id'], 'is_ignored' => 1])
                        ->first();

        // check if task is really ignored
        $this->assertEquals($ignored_task->id, $data['task_id']);

        // should return only 4 data, because task.id 1 is ignored already
        $tasks_after_ignore = $this->Tasks->find()->where(['is_ignored' => 0]);

        // will be true, because expected is only 4 because task id 1 is ignored
        $this->assertEquals($tasks_before_ignore - 1, $tasks_after_ignore->count());

        // check if preview tasks count is not equal to new task count after ignore
        $this->assertNotEquals($tasks_before_ignore, $tasks_after_ignore->count());

    }

    /**
     * ignore method
     *
     * @author Kevin
     * Method : POST
     * URI    : /admin/daily-reports/ignore
     * for    : To check what will happen if not existing task id is ignored
     * @return void
     */
    public function testIgnoreButton2() {
        //load tasks table
        $this->Tasks = TableRegistry::get('tasks');

        $tasks_before_ignore = $this->Tasks->find('all')->where(['is_ignored' => 0]);

        // get count of all tasks that is not ignored before post request
        $tasks_before_ignore = $tasks_before_ignore->count();

        // Mocking of Session for authentication
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'name' => 'kevin',
                    'is_enable' => 1,
                    'role' => 1,
                    'password_updated' => 1
                ]
            ]
        ]);

        // set task_id, this time task id 6 is not existing in fixture
        $data = ['task_id' => 6];

        // post request, and task_id to be ignored
        $this->post('/admin/daily-reports/ignore', $data);

        // Check for a 2xx response code
        $this->assertResponseOk();

        // this will return 5 because task id 6 is not existing in fixture
        $tasks_after_ignore = $this->Tasks->find()->where(['is_ignored' => 0]);

        // expected is same count of tasks before ignore
        $this->assertEquals($tasks_before_ignore, $tasks_after_ignore->count());
    }
}
