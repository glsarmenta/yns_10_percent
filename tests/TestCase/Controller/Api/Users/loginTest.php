<?php
namespace App\Test\TestCase\Controller\Api;

use App\Controller\Api\usersController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\Api\usersController Test Case
 */
class loginTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
    ];

    /**
     * Test 01
     * Token : Exist
     * Authenticated : True
     * Response : 200
     * User : Exist
     * Valid Json : True
     * @return void
     */
    public function testlogin01()
    {

        $this->post('/api/users/login', [
            'username' => 'user_1',
            'password' => 'asai'
        ]);
        $this->assertResponseCode(302);
        $this->assertRedirect('/');
    }
}
