<?php
namespace App\Test\TestCase\Controller\Api;

use App\Controller\Api\usersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Api\usersController Test Case
 */
class getUserTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
    ];

    /**
     * Test 01
     * Token : Exist
     * Authenticated : True
     * Response : 200
     * User : Exist
     * Valid Json : True
     * @return void
     */
    public function testgetUser01()
    {
        // Config of request including api token
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'X-API-TOKEN' => '123456'
            ]
        ]);
        // Mocking of Session for authentication
        $this->session(['Auth.User.id' => 1]);
        // Get Action with id 1
        $result = $this->get('/api/users/get_user.json?id=1');
        // Get Response with status code 2xx
        $this->assertResponseOk();
        // This API Content is valid json
        $this->assertContentType('application/json');
    }

    /**
     * Test 02
     * Token : Not Exist
     * Authenticated : True
     * Response : 401
     * User : Exist
     * Valid Json : True
     * @return void
     */
    public function testgetUser02()
    {
        // Config of request including api token
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
            ]
        ]);
        // Mocking of Session for authentication
        $this->session(['Auth.User.id' => 1]);
        // Get Action with id 1
        $result = $this->get('/api/users/get_user.json?id=1');
        // Get Response with status code 4xx
        $this->assertResponseError();
        // Check for a specific response code, e.g. 401
        $this->assertResponseCode(401);
        // This API Content is valid json
        $this->assertContentType('application/json');
    }

    /**
     * Test 03
     * Token : Exist / Invalid
     * Response : 401
     * Valid Json : True
     * @return void
     */
    public function testgetUser03()
    {
        // Config of request including api token
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'X-API-TOKEN' => 'wrong token'
            ]
        ]);
        // Mocking of Session for authentication
        $this->session(['Auth.User.id' => 1]);
        // Get Action with id 1
        $result = $this->get('/api/users/get_user.json?id=1');
        // Get Response with status code 4xx
        $this->assertResponseError();
        // Check for a specific response code, e.g. 401
        $this->assertResponseCode(401);
        // This API Content is valid json
        $this->assertContentType('application/json');
    }
    /**
     * Test 04
     * Token : Exist
     * Authenticated : False
     * Response : 302
     * Valid Json : True
     * @return void
     */
    public function testgetUser04()
    {
        // Config of request including api token
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'X-API-TOKEN' => '123456'
            ]
        ]);
        // Get Action with id 1
        $result = $this->get('/api/users/get_user.json?id=1');
        // Get Response with status code 4xx
        $this->assertResponseSuccess();
        // Check for a specific response code, e.g. 302 // Redirect
        $this->assertResponseCode(302);
        // Redirected to users login
        $this->assertRedirectContains('/users/login');
        // This API Content is valid json
        $this->assertContentType('application/json');
    }

    /**
     * Test 05
     * Token : Exist
     * Authenticated : True
     * Invalid Parameter
     * Response : 400
     * Valid Json : True
     * @return void
     */
    public function testgetUser05()
    {
        // Config of request including api token
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'X-API-TOKEN' => '123456'
            ]
        ]);
        // Mocking of Session for authentication
        $this->session(['Auth.User.id' => 1]);
        // Get Action with id 1
        $result = $this->get('/api/users/get_user.json');
        $this->assertResponseFailure();
    }
}
