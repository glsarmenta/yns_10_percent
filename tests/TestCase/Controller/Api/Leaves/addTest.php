<?php
namespace App\Test\TestCase\Controller\Api;

use App\Controller\Api\leavesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Api\usersController Test Case
 */
class addTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.leaves',
    ];

    /**
     * Test 01
     * Token : Exist
     * Authenticated : True
     * Response : 200
     * User : Exist
     * Valid Json : True
     * @return void
     */
    public function testadd01()
    {
        // Config of request including api token
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'X-API-TOKEN' => '123456'
            ]
        ]);
        // Mocking of Session for authentication
        $this->session(['Auth.User.id' => 1]);
        // Get Action with id 1
        $result = $this->post('/api/leaves/add.json?id=1');
        // Get Response with status code 2xx
        $this->assertResponseOk();
        // Return contains user variable
        $user =  $this->viewVariable('user');
        // User object contain id = 1
        $this->assertEquals('1', $user->id);
        // This API Content is valid json
        $this->assertContentType('application/json');
    }
}
