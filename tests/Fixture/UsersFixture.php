<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 *
 */
class UsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $import = ['model' => 'users'];

    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'name' => 'Kevin',
            'role' => 1,
            'email' => 'kevin.fontanilla50@gmail.com',
            'password' => 'Lorem ipsum dolor sit amet',
            'is_enable' => 1,
            'password_updated' => 0,
            'created' => '2017-09-04 09:09:46',
            'modified' => '2017-09-04 09:09:46',
        ],
        [
            'name' => 'Oba',
            'role' => 2,
            'email' => 'Oba@gmail.com',
            'password' => 'Lorem ipsum dolor sit amet',
            'is_enable' => 0,
            'created' => '2017-09-04 09:09:46',
            'modified' => '2017-09-04 09:09:46',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'name' => 'Denmark',
            'role' => 1,
            'email' => 'denmark@gmail.com',
            'password' => 'Lorem ipsum dolor sit amet',
            'is_enable' => 0,
            'created' => '2017-09-04 09:09:46',
            'modified' => '2017-09-04 09:09:46',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'name' => 'lorem',
            'role' => 1,
            'email' => 'lorem@gmail.com',
            'password' => 'Lorem ipsum dolor sit amet',
            'is_enable' => 0,
            'created' => '2017-09-04 09:09:46',
            'modified' => '2017-09-04 09:09:46',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'name' => 'ipsum',
            'role' => 1,
            'email' => 'ipsum@gmail.com',
            'password' => 'Lorem ipsum dolor sit amet',
            'is_enable' => 0,
            'created' => '2017-09-04 09:09:46',
            'modified' => '2017-09-04 09:09:46',
            'deleted' => 0,
            'deleted_date' => NULL
        ]
    ];
}
