<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DailyWorkResultsFixture
 *
 */
class DailyWorkResultsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $import = ['model' => 'daily_work_results'];

    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'user_id' => 1,
            'task_id' => 1,
            'actual_times' => 5,
            'work_date' => '2017-09-04',
            'created' => '2017-09-04 09:10:17',
            'modified' => '2017-09-04 09:10:17',
            'deleted' => 1,
            'deleted_date' => '2017-09-04 09:10:17'
        ],
        [
            'user_id' => 2,
            'task_id' => 2,
            'actual_times' => 5,
            'work_date' => '2017-09-04',
            'created' => '2017-09-04 09:10:17',
            'modified' => '2017-09-04 09:10:17',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'user_id' => 3,
            'task_id' => 3,
            'actual_times' => 5,
            'work_date' => '2017-09-04',
            'created' => '2017-09-04 09:10:17',
            'modified' => '2017-09-04 09:10:17',
            'deleted' => 1,
            'deleted_date' => '2017-09-04 09:10:17'
        ],
        [
            'user_id' => 4,
            'task_id' => 4,
            'actual_times' => 5,
            'work_date' => '2017-09-04',
            'created' => '2017-09-04 09:10:17',
            'modified' => '2017-09-04 09:10:17',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'user_id' => 5,
            'task_id' => 5,
            'actual_times' => 5,
            'work_date' => '2017-09-04',
            'created' => '2017-09-04 09:10:17',
            'modified' => '2017-09-04 09:10:17',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
    ];
}
