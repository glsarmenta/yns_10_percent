<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProjectUsersFixture
 *
 */
class ProjectUsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $import = ['model' => 'project_users'];

    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'project_id' => 1,
            'user_id' => 1,
            'created' => '2017-09-25 07:54:53',
            'modified' => '2017-09-25 07:54:53',
            'deleted' => 1,
            'deleted_date' => '2017-09-25 07:54:53'
        ],
        [
            'project_id' => 2,
            'user_id' => 2,
            'created' => '2017-09-25 07:54:53',
            'modified' => '2017-09-25 07:54:53',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'project_id' => 3,
            'user_id' => 3,
            'created' => '2017-09-25 07:54:53',
            'modified' => '2017-09-25 07:54:53',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'project_id' => 4,
            'user_id' => 4,
            'created' => '2017-09-25 07:54:53',
            'modified' => '2017-09-25 07:54:53',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'project_id' => 5,
            'user_id' => 5,
            'created' => '2017-09-25 07:54:53',
            'modified' => '2017-09-25 07:54:53',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
    ];
}
