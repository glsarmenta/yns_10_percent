<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TasksFixture
 *
 */
class TasksFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $import = ['model' => 'tasks'];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'project_id' => 1,
            'milestone_id' => 1,
            'task_code' => 'TASK-1',
            'name' => 'Payments',
            'estimate_times' => 0,
            'total_actual_times' => 5,
            'is_ignored' => 0,
            'is_close' => 1,
            'created' => '2017-09-04 09:10:45',
            'modified' => '2017-09-04 09:10:45',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'project_id' => 2,
            'milestone_id' => 2,
            'task_code' => 'TASK-2',
            'name' => 'Payments',
            'estimate_times' => 0,
            'total_actual_times' => 5,
            'is_ignored' => 0,
            'is_close' => 1,
            'created' => '2017-09-04 09:10:45',
            'modified' => '2017-09-04 09:10:45',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'project_id' => 3,
            'milestone_id' => 3,
            'task_code' => 'TASK-3',
            'name' => 'Payments',
            'estimate_times' => 0,
            'total_actual_times' => 5,
            'is_ignored' => 0,
            'is_close' => 1,
            'created' => '2017-09-04 09:10:45',
            'modified' => '2017-09-04 09:10:45',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'project_id' => 4,
            'milestone_id' => 4,
            'task_code' => 'TASK-4',
            'name' => 'Payments',
            'estimate_times' => 100,
            'total_actual_times' => 5,
            'is_ignored' => 0,
            'is_close' => 1,
            'created' => '2017-09-04 09:10:45',
            'modified' => '2017-09-04 09:10:45',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
        [
            'project_id' => 5,
            'milestone_id' => 5,
            'task_code' => 'TASK-5',
            'name' => 'Payments',
            'estimate_times' => 100,
            'total_actual_times' => 5,
            'is_ignored' => 0,
            'is_close' => 1,
            'created' => '2017-09-04 09:10:45',
            'modified' => '2017-09-04 09:10:45',
            'deleted' => 0,
            'deleted_date' => NULL
        ],
    ];
}
