<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProjectsFixture
 *
 */
class ProjectsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $import = ['model' => 'projects'];

    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'name' => 'Project A',
            'project_code' => 'PROJECT-A',
            'is_allmembers' => 1,
            'created' => '2017-09-04 09:10:38',
            'modified' => '2017-09-04 09:10:38',
            'deleted' => 0,
            'detete_date' => NULL
        ],
        [
            'name' => 'Project B',
            'project_code' => 'PROJECT-B',
            'is_allmembers' => 1,
            'created' => '2017-09-04 09:10:38',
            'modified' => '2017-09-04 09:10:38',
            'deleted' => 1,
            'detete_date' => '2017-09-04 09:10:38'
        ],
        [
            'name' => 'Project C',
            'project_code' => 'PROJECT-C',
            'is_allmembers' => 1,
            'created' => '2017-09-04 09:10:38',
            'modified' => '2017-09-04 09:10:38',
            'deleted' => 1,
            'detete_date' => '2017-09-04 09:10:38'
        ],
        [
            'name' => 'Project D',
            'project_code' => 'PROJECT-D',
            'is_allmembers' => 1,
            'created' => '2017-09-04 09:10:38',
            'modified' => '2017-09-04 09:10:38',
            'deleted' => 0,
            'detete_date' => NULL
        ],
        [
            'name' => 'Project-E',
            'project_code' => 'PROJECT-E',
            'is_allmembers' => 1,
            'created' => '2017-09-04 09:10:38',
            'modified' => '2017-09-04 09:10:38',
            'deleted' => 0,
            'detete_date' => NULL
        ],
    ];
}
