<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MilestonesFixture
 *
 */
class MilestonesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $import = ['model' => 'milestones'];

    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'project_id' => 1,
            'name' => 'Project A Milestone',
            'total_estimate_time' => 100,
            'created' => '2017-09-04 09:10:29',
            'modified' => '2017-09-04 09:10:29',
            'deleted' => 0,
            'detete_date' => NULL
        ],
        [
            'project_id' => 2,
            'name' => 'Project B Milestone',
            'total_estimate_time' => 100,
            'created' => '2017-09-04 09:10:29',
            'modified' => '2017-09-04 09:10:29',
            'deleted' => 1,
            'detete_date' => '2017-09-04 09:10:29'
        ],
        [
            'project_id' => 3,
            'name' => 'Project C Milestone',
            'total_estimate_time' => 100,
            'created' => '2017-09-04 09:10:29',
            'modified' => '2017-09-04 09:10:29',
            'deleted' => 0,
            'detete_date' => NULL
        ],
        [
            'project_id' => 4,
            'name' => 'Project D Milestone',
            'total_estimate_time' => 100,
            'created' => '2017-09-04 09:10:29',
            'modified' => '2017-09-04 09:10:29',
            'deleted' => 0,
            'detete_date' => NULL
        ],
        [
            'project_id' => 5,
            'name' => 'Project E Milestone',
            'total_estimate_time' => 100,
            'created' => '2017-09-04 09:10:29',
            'modified' => '2017-09-04 09:10:29',
            'deleted' => 0,
            'detete_date' => NULL
        ],
    ];
}
