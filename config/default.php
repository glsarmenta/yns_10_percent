<?php
$config = [
  'Grades' => [
      0 => __('No Grade'),
      1 => __('Dame'),
      2 => __('Soso'),
      3 => __('Normal'),
      4 => __('Good'),
      5 => __('Very Good')
  ]
];
