<?php
use Migrations\AbstractMigration;

class RemoveDeletedFromProjectUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('project_users');
        $table->removeColumn('deleted');
        $table->update();
    }
}
