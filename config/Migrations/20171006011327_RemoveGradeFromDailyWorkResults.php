<?php
use Migrations\AbstractMigration;

class RemoveGradeFromDailyWorkResults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('daily_work_results');
        $table->removeColumn('grade');
        $table->update();
    }
}
