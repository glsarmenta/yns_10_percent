<?php
use Migrations\AbstractMigration;

class RemoveDeletedFromDailyWorkResults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('daily_work_results');
        $table->removeColumn('deleted');
        $table->update();
    }
}
