<?php
use Migrations\AbstractMigration;

class CreateLeaves extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('leaves')
        ->addColumn('user_id', 'integer', [
            'comment' => '1: sick 2; vacation; ',
            'limit' => 11,
            'null' => false,
        ])
        ->addColumn('type', 'integer', [
            'default' => null,
            'comment' => '1: sick 2; vacation; ',
            'limit' => 11,
            'null' => false,
        ])
        ->addColumn('reason', 'text', [
            'default' => null,
            'null' => false,
        ])
        ->addColumn('leave_start', 'date', [
            'default' => null,
            'null' => false,
        ])
        ->addColumn('leave_end', 'date', [
            'default' => null,
            'null' => false,
        ])
        ->addColumn('team_leader_user_id', 'integer', [
            'default' => null,
            'comment' => 'team_leader_user_id = Users.id ',
            'limit' => 11,
            'null' => false,
        ])
        ->addColumn('created', 'datetime', [
            'default' => 'CURRENT_TIMESTAMP',
            'limit' => null,
            'null' => true,
        ])
        ->addColumn('modified', 'datetime', [
            'default' => 'CURRENT_TIMESTAMP',
            'limit' => null,
            'null' => true,
        ])
        ->addColumn('deleted', 'integer', [
            'default' => '0',
            'limit' => 11,
            'null' => true,
        ])
        ->addColumn('deleted_date', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => true,
        ])
        ->create();
    }
}
