<?php
use Migrations\AbstractMigration;

class ChangeGradeFromTasks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tasks');
        $table->changeColumn('grade', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '1=Dame 2=Soso 3=Normal 4=Good 5=Very Good',
        ]);
        $table->update();
    }
}
