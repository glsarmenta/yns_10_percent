<?php
use Migrations\AbstractMigration;

class AddGradeToDailyWorkResults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */

    public function change()
    {
        $table = $this->table('daily_work_results');
        $table->addColumn('grade', 'integer', [
            'comment' => '1=Dame 2=Soso 3=Normal 4=Good 5=Very Good',
            'default' => null,
            'limit'   => 11,
            'null'    => false,
            'after' => 'work_date'
        ]);
        $table->update();
    }
}