<?php
use Migrations\AbstractMigration;

class CreateMissRegistrationTasks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('miss_registration_tasks');
        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('miss_date', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('is_ignored', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0: not ignored 1: ignored',
        ]);
        $table->addColumn('comments', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => 'CURRENT_TIMESTAMP',
            'limit' => null,
            'null' => true,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => 'CURRENT_TIMESTAMP',
            'limit' => null,
            'null' => true,
        ]);
        $table->addColumn('deleted', 'integer', [
            'default' => '0',
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('deleted_date', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => true,
        ]);
        $table->create();
    }
}
