<?php
use Migrations\AbstractMigration;

class AddPasswordUpdatedToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('password_updated', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'after' => 'password'
        ]);
        $table->update();
    }
}
