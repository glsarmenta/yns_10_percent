<?php
use Migrations\AbstractMigration;

class AddIsCommonToProjects extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('projects');
        $table->addColumn('is_common', 'integer', [
            'default'   => 0,
            'limit'     => 11,
            'null'      => true,
            'comment'   => '0: not common 1: common',
            'after'     => 'in_backlog'
        ]);
        $table->update();
    }
}
