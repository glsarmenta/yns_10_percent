<?php
use Migrations\AbstractMigration;

class AddIgnoreToTasks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tasks');
        $table->addColumn('is_ignored', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => false,
            'after' => 'grade',
        ]);
        $table->update();
    }
}
