
/*
** Autocomplete
*/
function suggestFields(url, data, response) {
  $.ajax({
    url  : url,
    data : data,
      success: function(res) {
       response(
        $.map(eval(res), function (i, val) {
          return { label: i, value: i, data: val}
          })
        );
      }
  });
}

/*
** get estimate_time by task_code
*/
function setEstimateTime(url, data, estimate) {
  $.ajax({
      url  : url,
      data : data,
      type : 'POST',
      success : function(res) {
          $('#' + estimate).val(res.estimate_times);
      }
  });
}

/*
** Search all matching milestones on selected project
*/
function searchMatchingProject(url, data, mile_id) {
  $.ajax({
    url    : url,
    method : 'POST',
    async  : false,
    data   : data
    })
    .done(function(result) {
      $('#' + mile_id).empty().prepend("<option value='' selected='selected'>---</option>");
      $.each(result, function(key, value) {
        $('#' + mile_id)
          .append($("<option></option>")
          .attr("value",key)
          .text(value));
    });
  });
}
