$(document).ready(function() {
  var project_id   = '';
  var milestone_id = '';
  var start        = '' ;
  var end          = '';
  var member_id    = new Array();

  //dropdown searching
  $('#project_id_add').on('change', function(){
    var id          = $(this).attr("id");
    project_id      = $('#' + id).val();
    var url         = '/admin/daily-reports/getMemberAndMilestone';
    var mile_id     = (id === 'project_id_add') ? 'milestone_id_add' : 'milestone_id';
    var date        = new Date();
    var today       = date.getFullYear()+'-'+ (date.getMonth()+1) +'-'+date.getDate();
    date.setDate(date.getDate() - 6);
    var defaultDate = date.getFullYear()+'-'+ (date.getMonth()+1) +'-'+date.getDate();

    if (project_id === '') {
      $('#' + mile_id).empty().prepend("<option value='' selected='selected'>---</option>");
    } else {
      member_id = [];
      $('#member_id').empty();
      $('#member_name').val('');
      $('#date_to').val(today);
      $('#date_from').val(defaultDate);
      start = defaultDate;
      end   = today;
      filterSearch(url, project_id, member_id, milestone_id, start, end, mile_id);
    }
  });
  //for selectbox
  $('.select2').select2()

  //for textbox searching with autocomplete
  $('#member_name').on('focus', function() {
    $(this).autocomplete({
      autoFocus: true,
      source: function(request, response) {
        var url  = '/admin/daily-reports/suggestMember';
        var data = { name : request.term, project_id : project_id };

        suggestFields(url, data, response);
      },
      select: function(event, ui) {
        event.preventDefault();
        var name    = ui.item.value;
        var value   = ui.item.data
        var checked = "";

        if ($('#selected-'+value).length != 0) {
          checked = "checked";
        }

        $('#member_id').empty();
        $('#member_id')
        .append(
          "<li class='col-md-6'><div><div class='col-xs-1'><input type='checkbox' value="+value+" "+checked+"></div><label class='col-xs-11 checkbox-label'>"+name+"</label></div></li>"
        );
        $(this).val(name);
      }
    });
  });

  //check member search if null
  $('#member_name').keypress(function(e) {
    if (e.keyCode == 13) {
      if ($(this).val() == '') {
        $('ul#member_id li').remove();
        filterSearch('/admin/daily-reports/getMemberAndMilestone', project_id, member_id, milestone_id, start, end,"", true);
      }
    }
  });

  //for checkbox searching
  $('body').delegate('input[type=checkbox]','click', function() {
    var url  = '/admin/daily-reports/getMemberAndMilestone';
    var data = $(this).val();
    var i    = member_id.indexOf(data);
    var name = $(this).parent().next().text();

    if ($(this).prop('checked') == true) {
      member_id.push(data);
    } else {
      if(i != -1) {
        member_id.splice(i, 1);
      }

      $('#selected-'+data).parent().remove();
    }
    filterSearch(url, project_id, member_id, milestone_id, start, end);
  });

  //milestone dropdown search
  $('#milestone_id_add').on('change', function(){
    var url      = '/admin/daily-reports/getMemberAndMilestone';
    milestone_id = $(this).val();

    filterSearch(url, project_id, member_id, milestone_id, start, end);
  });

  //Duration datepicker
  var date_from = $('#date_from')
    .datepicker({
      dateFormat     : 'yy-mm-dd',
      changeMonth    : true,
      numberOfMonths : 1,
      showButtonPanel: true,
      autoclose      : true,
      onSelect: function(date) {
        start = date

        $('#date_from').val(date);
        date_to.datepicker( "option", "minDate", new Date(start));

        filterSearch('/admin/daily-reports/getMemberAndMilestone', project_id, member_id, milestone_id, start, end);
      },
      onClose: function() {
        $('#date_from').val(start);
        date_to.datepicker( "option", "minDate", new Date(start));
      }
    })
    .on( "change", function() {
      date_to.datepicker( "option", "minDate", getDate( this ) );
      $('input[name=date_from]').val($(this).val());
    })
    .on('focus', function(){
      $('#date_from').keyup(function(e){
        start = $(this).val();

        if(e.keyCode === 13) {
          $("#date_from").datepicker("hide");
        }
      });
    })

    date_to = $( "#date_to" ).datepicker({
      dateFormat     : 'yy-mm-dd',
      changeMonth    : true,
      numberOfMonths : 1,
      showButtonPanel: true,
      autoclose      : true,
      onSelect: function(date) {
        end = date

        $('#date_to').val(date);
        date_from.datepicker( "option", "maxDate", new Date(date));

        filterSearch('/admin/daily-reports/getMemberAndMilestone', project_id, member_id, milestone_id, start, end);
      },
      onClose: function() {
        $('#date_to').val(end);
        date_from.datepicker( "option", "maxDate", new Date(end));
      },
    })
    .on( "change", function() {
      date_from.datepicker( "option", "maxDate", getDate( this ) );
      $('input[name=date_to]').val($(this).val());
    })
    .on('focus', function(){
      $('#date_to').keyup(function(e){
        end = $(this).val();

        if(e.keyCode === 13) {
          $("#date_to").datepicker("hide");
        }
      });
    })
    date_to.datepicker( "option", "minDate", new Date($(this).val()));
    date_from.datepicker( "option", "maxDate", new Date($(this).val()));

    var project_id = $("#project-id").val();
    var year_month = $("#year-month").val();
    if (typeof year_month !== "undefined") {
      var year       = year_month.substr(0, 4);
      var month      = year_month.substr(4, 2);
      if (month.length == 1) {
        var start_date = year + "-0" + month + "-" + "00"
        var end_date   = year + "-0" + (parseInt(month) + 1) + "-" + "00"
      } else {
        var start_date = year + "-" + month + "-" + "00"
        var end_date   = year + "-" + (parseInt(month) + 1) + "-" + "00"
      }
      $.ajax({
        url: "/admin/monthly-reports/getMilestonePerMonth",
        type: "POST",
        data: {
          start_date: start_date,
          end_date: end_date,
          project_id: project_id
        },
        success: function( result ) {
          $("#data-result").empty();
          var last_row_milestone    = $('#data-table tr td:nth-child(2)').html();
          var last_row_project_name = $('#data-table tr td:first-child').html();
          var last_row_total_hours  = $('#data-table tr td:last-child').html();
          var sum                   = 0;

          $.each( result, function( key, value ) {
            if (value.project_name == last_row_project_name) {
              if (value.milestone == last_row_milestone) {
                $('#data-result').append('<tr id="c"><td></td><td></td><td>'+ value.user_name +'</td><td>'+ value.actual_time +' Hrs</td></tr>');
                last_row_project_name = value.project_name
                last_row_milestone = value.milestone
                last_row_total_hours = value.actual_time
                sum += parseFloat(value.actual_time)
                $("#data-result tr.group:last td:last-child").html(sum + ' Hrs');
              } else {
                $('#data-result').append('<tr class="group"><td>Total Actual Hours</td><td></td><td></td><td>'+ sum +' Hrs</td></tr>');
                $('#data-result').append('<tr id="b"><td></td><td>'+ value.milestone +'</td><td>'+ value.user_name +'</td><td>'+ value.actual_time +' Hrs</td></tr>');
                last_row_project_name = value.project_name
                last_row_milestone = value.milestone
                last_row_total_hours = value.actual_time
                sum = parseFloat(value.actual_time)
                $("#data-result tr.group:last td:last-child").html(sum + ' Hrs');
              }
              } else {
                $('#data-result').append('<tr class="group"><td>Total Actual Hours</td><td></td><td></td><td>'+ sum +' Hrs</td></tr>');
                $('#data-result').append('<tr id="a"><td>'+ value.project_name +'</td><td>'+ value.milestone +'</td><td>'+ value.user_name +'</td><td>'+ value.actual_time +' Hrs</td></tr>');
                last_row_project_name = value.project_name
                last_row_milestone = value.milestone
                last_row_total_hours = value.actual_time
                sum = parseFloat(value.actual_time)
                $("#data-result tr.group:last td:last-child").html(sum + ' Hrs');
              }
            });
          }
        });
      $('.data-table').css('display', 'block');
    }

    $("#year-month").change(function(){
        var project_id = $("#project-id").val();
        var year_month = $("#year-month").val();
        var year = year_month.substr(0, 4);
        var month = year_month.substr(4, 2);
        if (month.length == 1) {
            var start_date = year + "-0" + month + "-" + "00"
            var end_date = year + "-0" + (parseInt(month) + 1) + "-" + "00"
        } else {
            var start_date = year + "-" + month + "-" + "00"
            var end_date = year + "-" + (parseInt(month) + 1) + "-" + "00"
        }
        $.ajax({
            url: "/admin/monthly-reports/getMilestonePerMonth",
            type: "POST",
            data: {
                start_date: start_date,
                end_date: end_date,
                project_id: project_id
            },
            success: function( result ) {
                $("#data-result").empty();
                var last_row_milestone = $('#data-table tr td:nth-child(2)').html();
                var last_row_project_name = $('#data-table tr td:first-child').html();
                var last_row_total_hours = $('#data-table tr td:last-child').html();
                var sum = 0;

                $.each( result, function( key, value ) {
                    if (value.project_name == last_row_project_name) {
                        if (value.milestone == last_row_milestone) {
                            $('#data-result').append('<tr id="c"><td></td><td></td><td>'+ value.user_name +'</td><td>'+ value.actual_time +' Hrs</td></tr>');
                            last_row_project_name = value.project_name
                            last_row_milestone = value.milestone
                            last_row_total_hours = value.actual_time
                            sum += parseFloat(value.actual_time)
                            $("#data-result tr.group:last td:last-child").html(sum + ' Hrs');
                        } else {
                            $('#data-result').append('<tr class="group"><td>Total Actual Hours</td><td></td><td></td><td>'+ sum +' Hrs</td></tr>');
                            $('#data-result').append('<tr id="b"><td></td><td>'+ value.milestone +'</td><td>'+ value.user_name +'</td><td>'+ value.actual_time +' Hrs</td></tr>');
                            last_row_project_name = value.project_name
                            last_row_milestone = value.milestone
                            last_row_total_hours = value.actual_time
                            sum = parseFloat(value.actual_time)
                            $("#data-result tr.group:last td:last-child").html(sum + ' Hrs');
                        }
                    } else {
                        $('#data-result').append('<tr class="group"><td>Total Actual Hours</td><td></td><td></td><td>'+ sum +' Hrs</td></tr>');
                        $('#data-result').append('<tr id="a"><td>'+ value.project_name +'</td><td>'+ value.milestone +'</td><td>'+ value.user_name +'</td><td>'+ value.actual_time +' Hrs</td></tr>');
                        last_row_project_name = value.project_name
                        last_row_milestone = value.milestone
                        last_row_total_hours = value.actual_time
                        sum = parseFloat(value.actual_time)
                        $("#data-result tr.group:last td:last-child").html(sum + ' Hrs');
                    }
                });
            }
          });
        $('.data-table').css('display', 'block');
    });
});

/**
 * This function is responsible for filtering search
 * @param  {[type]}  url          [function url]
 * @param  {String}  project_id   [project_idd]
 * @param  {String}  member_id    [member_id]
 * @param  {String}  milestone_id [milestone_id]
 * @param  {String}  date_from    [date_from]
 * @param  {String}  date_to      [date_to]
 * @param  {String}  mile_id      [mile_id]
 * @param  {Boolean} blank        [blank means if the search in textbox member is blank]
 */
function filterSearch (url, project_id = '', member_id = '', milestone_id = '',
    date_from = '', date_to = '', mile_id = '', blank = false) {
    var estimate_times = $('#estimated_hour').val();
    var status         = {0:'Open', 1:'Closed'};

    $('.data-table').css('display', 'block');
    //remove error message
    $.fn.dataTable.ext.errMode = 'none';
    //check if dataTable is exist and destroy
    if ( $.fn.DataTable.isDataTable( '#projects_table' )) {
      $('#projects_table').DataTable({
        "processing" : true,
        "dom"        : 'B<"clear">lfrtip',
        'order'      : [[ 0, 'desc' ]],
        'searching'  : false,
        "buttons"    : [{
          extend: 'collection',
          text: 'Export',
          buttons: ['copy', 'excel', 'csv', 'pdf', 'print']
        }],
        "stateSave": false,
        "stateDuration": 60*60*24*365,
        "displayLength": 100,
        // "dom": 'lfTrtip',
        "drawCallback": function ( settings ) {
          var api          = this.api();
          var rows         = api.rows( {page:'current'} ).nodes();
          var last         = null;
          var colonne      = "";
          if (typeof api.row(0).data() !== "undefined") {
            colonne      = api.row(0).data().length;
          }
          var totale       = new Array();
          totale['Totale'] = new Array();
          var groupid      = -1;
          var subtotale    = new Array();
          var a            = 1;

          api.column(0, {page:'current'} ).data().each( function ( group, i ) {
            if ( last !== group ) {
              groupid++;
              $(rows).eq( i ).before(
                '<tr class="group"><td>Total Hour:</td></tr>'
              );
              last = group;
            }
            val = api.row(api.row($(rows).eq( i )).index()).data();      //current order index
            $.each(val,function(index2,val2) {
              if (index2 === 7) {
                if (typeof subtotale[groupid] =='undefined'){
                  subtotale[groupid] = new Array();
                }
                if (typeof subtotale[groupid][index2] =='undefined'){
                  subtotale[groupid][index2] = 0;
                }
                if (typeof totale['Totale'][index2] =='undefined') {
                  totale['Totale'][index2] = 0;
                }
                valore = Number(val2.replace('h',""));
                subtotale[groupid][index2] += valore;
                totale['Totale'][index2] += valore;
              }
            });
          });
          $('tbody').find('.group').each(function (i,v) {
            var rowCount = $(this).nextUntil('.group').length;
            var subtd = '';
            for (a = 1; a <= colonne; a++) {
                if (a < 8) {
                    if (totale['Totale'][a] == null) {
                        subtd += '<td></td>'
                    } else {
                        subtd += '<td>'+subtotale[i][a]+'hrs</td>';
                    }
                }
            }
            $(this).append(subtd);
          });
        }
      }).destroy();
    }

    var ajax_result = ajaxHandler(url, {
      project_id     : project_id,
      member_id      : (blank) ? '' : member_id,
      milestone_id   : milestone_id,
      date_from      : date_from,
      date_to        : date_to,
      estimate_times : (estimate_times) ? estimate_times : ''
    });
    ajax_result.then(function(response) {
      if (mile_id != '') {
        $('#' + mile_id).empty().prepend("<option value='' selected='selected'>---</option>");
        $.each(response['Milestones'], function(key, value) {
          $('#' + mile_id)
            .append($("<option></option>")
            .attr("value",key)
            .text(value));
        });
      }
      if ($('ul#member_id li').length >= 1) {
        if (member_id == '') {
          $('#member_id').empty();
          $.each(response['Members'], function(key, value) {
            $('#member_id')
              .append(
                "<li class='col-md-6'><div><div class='col-xs-1'><input type='checkbox' value="+key+" ></div><label class='col-xs-11 checkbox-label'>"+value+"</label></div></li>"
              );
          });
        }
      } else {
        $('#member_id').empty();
          $.each(response['Members'], function(key, value) {
            $('#member_id')
              .append(
                "<li class='col-md-6'><div><div class='col-xs-1'><input type='checkbox' value="+key+" "+idExist(key, member_id)+"></div><label class='col-xs-11 checkbox-label'>"+value+"</label></div></li>"
              );
          });
      }
      $('#data-result').empty();
      $.each(response['Summary'].result, function(key, value) {
        var milestone = (value.task.milestone_id) ? response['Milestones'][value.task.milestone_id] : '';
        $('#data-result')
        .append("<tr><td>"+value.user.name+"</td><td>"+value.task.project.name+"</td><td>"+milestone+"</td><td>"+value.task.task_code+"</td><td>"+value.task.estimate_times+"h</td><td>"+value.task.total_actual_times+"h</td><td>"+status[value.task.is_close]+"</td><td>"+value.actual_times+"h</td></tr>");
      });

      /**
       * Append no planned hour data
       */
       if (response['no_planned_hour'].length > 0) {
        var check_miss_registration = false;
        $('#no_planned_hour').empty();
        $.each(response['no_planned_hour'], function(key, value) {
          if (value['miss_registration_tasks'].length > 0) {
            var miss_date           = '';
            check_miss_registration = true;
            $.each(value['miss_registration_tasks'], function(k, v){
              if (formatDate(v['miss_date'])) {
                miss_date+="<li class=col-xs-4>" +formatDate(v['miss_date']) + "</li>"
              }
            });
            $('#no_planned_hour')
            .append(
              "<div class=col-xs-12 no-padding summary-list-title-container>"+
              "<span class=summary-list-title>"+value['name']+"</span>"+
              "<ul>"+miss_date+
              "</ul></div>"
            );
          }
        });
        if (check_miss_registration) $('#no_planned_hour_title').show();
        else $('#no_planned_hour_title').hide();
       }

      $('#projects_table').DataTable({
        "processing" : true,
        "dom"        : 'B<"clear">lfrtip',
        'order'      : [[ 0, 'desc' ]],
        'searching'  : false,
        "buttons"    : [{
          extend: 'collection',
          text: 'Export',
          buttons: [
            'copy',
            'excel',
            'csv',
            'pdf',
            'print'
          ]
        }],
        "stateSave": false,
        "stateDuration": 60*60*24*365,
        "displayLength": 100,
        drawCallback: function ( settings ) {
          var api          = this.api();
          var rows         = api.rows( {page:'current'} ).nodes();
          var last         = null;
          var colonne      = "";
          if (typeof api.row(0).data() !== "undefined") {
            colonne      = api.row(0).data().length;
          }
          var totale       = new Array();
          totale['Totale'] = new Array();
          var groupid      = -1;
          var subtotale    = new Array();
          var a            = 1;

          api.column(0, {page:'current'} ).data().each( function ( group, i ) {
            if ( last !== group ) {
              groupid++;
              $(rows).eq( i ).before(
                '<tr class="group"><td>Total Hour:</td></tr>'
              );
              last = group;
            }
            val = api.row(api.row($(rows).eq( i )).index()).data();      //current order index
            $.each(val,function(index2,val2) {
              if (index2 === 7) {
                if (typeof subtotale[groupid] =='undefined'){
                  subtotale[groupid] = new Array();
                }
                if (typeof subtotale[groupid][index2] =='undefined'){
                  subtotale[groupid][index2] = 0;
                }
                if (typeof totale['Totale'][index2] =='undefined') {
                  totale['Totale'][index2] = 0;
                }
                valore = Number(val2.replace('h',""));
                subtotale[groupid][index2] += valore;
                totale['Totale'][index2] += valore;
              }
            });
          });
          $('tbody').find('.group').each(function (i,v) {
            var rowCount = $(this).nextUntil('.group').length;
            var subtd = '';
            for (a = 1; a <= colonne; a++) {
                if (a < 8) {
                    if (totale['Totale'][a] == null) {
                        subtd += '<td></td>'
                    } else {
                        subtd += '<td>'+subtotale[i][a]+'hrs</td>';
                    }
                }
            }
            $(this).append(subtd);
          });
        }
      });
    });
  }
/**
 * Check if the id is exist in a array
 */
function idExist(id, array) {
  var checked = "";
  $.each(array, function(k, v) {
    if(id == v) {
      checked = "checked";
    }
  });
  return checked;
}

/**
 * Format date
 */
 function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;
  if (!(d.getDay() == 6 || d.getDay() == 0)) {
    return [year, month, day].join('-');
  }

  return false;

}
