function ajaxHandler(url,data) {
  return new Promise(function(resolve, reject) {
    var return_data={};
    var jqXhr = $.ajax({
      url: url,
      cache: false,
      method: "POST",
      async: true,
      dataType: 'json',
      data: data,
      beforeSend: function(){
        $('.loading_modal').modal('show');
      },
      complete:function() {
        $('.loading_modal').modal('hide');
      }
    });

    jqXhr
    .done(function(res) {
      resolve(res);
    })
    .fail(function(xhr) {
      $("#errorMsg").html('Something went wrong');
      $("#errorModal").modal('show');
      reject(false);
    });
  });
}